package com.qigao.erp.web.vo.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author luiz
 * @Title: UserRegisterVo
 * @ProjectName tokyo
 * @Description: TODO
 * @date 2019-04-04 10:11
 */
@Data
public class UserPerfectVo {
    @ApiModelProperty(value = "用户号", required = true)
    private String userNo;
    @ApiModelProperty(value = "微信号")
    private String weichatNo;
    @ApiModelProperty(value = "FaceBook")
    private String facebookNo;
    @ApiModelProperty(value = "line")
    private String lineNo;
    @ApiModelProperty(value = "性别 0:男 1:女 ", required = false)
    private String sexType;
    @ApiModelProperty(value = "年龄", required = false)
    private String age;
    @ApiModelProperty(value = "常住城市", required = false)
    private String activeCity;
    @ApiModelProperty(value = "国籍", required = false)
    private String country;
    @ApiModelProperty(value = "语言 0:中文 1:日文 2:英文", required = false)
    private String languageType;
    @ApiModelProperty(value = "是否激活交友功能 0:否 1:是", required = false)
    private String friendActive;
    @ApiModelProperty(value = "身高", required = false)
    private String height;
    @ApiModelProperty(value = "体重", required = false)
    private String weight;
    @ApiModelProperty(value = "标签", required = false)
    private List<String> tagList;

}
