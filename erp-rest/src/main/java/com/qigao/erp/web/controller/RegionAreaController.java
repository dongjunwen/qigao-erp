package com.qigao.erp.web.controller;

import com.qigao.erp.commons.RegionInfoService;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.commons.region.Area;
import com.qigao.erp.commons.region.City;
import com.qigao.erp.commons.region.Province;
import com.qigao.erp.commons.region.RegionAreaProcess;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.controller.app.common
 * @ProjectName three-mall
 * @date 2019-11-2019/11/14 11:06
 * @Description:地域信息
 */
@RestController
@RequestMapping("/common/region")
@Api(tags = "Common地域信息API",value = "地域信息API")
public class RegionAreaController {
    private static final Logger logger= LoggerFactory.getLogger(RegionAreaController.class);

    @Resource
    private RegionInfoService regionInfoService;
    @Value("${qigao.pulishYm:202005}")
    private String publishYm;

    @ApiOperation("行政区域初始化")
    @ApiImplicitParam(value = "年月 类似202005",defaultValue = "202005")
    @GetMapping("/initData/{pulishYm}")
    public Result initData(@PathVariable("pulishYm") String publishYm){
        try{
            List<Province> provinces= RegionAreaProcess.getAreaList(publishYm);
            if(provinces==null||provinces.size()==0){
                return Result.newError(ResultCode.COMMON_DATA_NOT_EXISTS);
            }
            int i=0;
            for(Province province:provinces){
                i++;
                logger.info("开始存储第{}个省份{}",i,province.getName());
                regionInfoService.saveProvince(province);
                for(City city:province.getCityList()){
                    city.setProvinceCode(province.getCode());
                    city.setPublishYm(province.getPublishYm());
                    regionInfoService.saveCity(city);
                    for(Area area:city.getAreaList()){
                        area.setCityCode(city.getCode());
                        area.setPublishYm(province.getPublishYm());
                        regionInfoService.saveArea(area);
                    }
                }
            }
        }catch (Exception e){
            logger.error("{}行政区域数据初始化失败:{}",publishYm,e);
            return Result.newError(ResultCode.FAIL);
        }
        return Result.newSuccess();
    }


    @ApiOperation("获取省份列表")
    @GetMapping("/province")
    public Result findProvinceList(){
        return regionInfoService.findListByLevel(publishYm,1);
    }

    @ApiOperation("获取下级列表")
    @GetMapping("/findChildArea/{areaCode}")
    public Result findChildArea(@PathVariable("areaCode")String areaCode){
        return regionInfoService.findChildListByAreaCode(publishYm,areaCode);
    }



    @ApiOperation("生成省市json(vant格式) https://github.com/youzan/vant/blob/1.x/packages/area/demo/area.js")
    @GetMapping("/genVantAreaData/")
    public Result genVantAreaData(){
        return Result.newSuccess(regionInfoService.genVantAreaData(publishYm));
    }


/*
    @ApiOperation("获取省份列表")
    @GetMapping("/province/V1")
    public Result findProvinceListV1(){
        JSONArray jsonArray=PropertiesUtils.getJsonArrayResource("china");
        List<AreaResultVo> areaResultVos=new ArrayList<>();
        for (int i=0;i<=jsonArray.size()-1;i++) {
                AreaResultVo areaResultVo=new AreaResultVo();
                JSONObject tempJson=(JSONObject) jsonArray.get(i);
                areaResultVo.setAreaCode(tempJson.getString("code"));
                areaResultVo.setAreaName(tempJson.getString("name"));
                areaResultVos.add(areaResultVo);
        }
        return Result.newSuccess(areaResultVos);
    }
    @ApiOperation("按照代码获取下级列表")
    @GetMapping("/nextRegion/V1/{areaCode}")
    public Result nextRegion(@PathVariable("areaCode")String areaCode){
        JSONArray tempCityArray=findDataByProvince(areaCode);
        List<AreaResultVo> areaResultVos=convertJsonArray(tempCityArray);
        return Result.newSuccess(areaResultVos);
    }
    @ApiOperation("按照省份代码获取城市列表")
    @GetMapping("/city/V1/{provinceCode}")
    public Result findCityList(@PathVariable("provinceCode")String provinceCode){
        JSONArray tempCityArray=findDataByProvince(provinceCode);
        List<AreaResultVo> areaResultVos=convertJsonArray(tempCityArray);
        return Result.newSuccess(areaResultVos);
    }
    @ApiOperation("按照城市代码获取区县列表")
    @GetMapping("/area/V1/{cityCode}")
    public Result findAreayList(@PathVariable("cityCode")String cityCode){
        JSONArray tempCityArray=findDataByCityCode(cityCode);
        List<AreaResultVo> areaResultVos=convertJsonArray(tempCityArray);
        return Result.newSuccess(areaResultVos);
    }


    private JSONArray findDataByProvince(String provinceCode){
        JSONArray jsonArray=PropertiesUtils.getJsonArrayResource("china");
        for (int i=0;i<=jsonArray.size()-1;i++) {
            JSONObject tempJson=(JSONObject) jsonArray.get(i);
            String code=tempJson.getString("code");
            if(code.substring(0,2).equals(provinceCode.substring(0,2))){
                JSONArray tempJsonArray=tempJson.getJSONArray("cityList");
               return tempJsonArray;
            }
        }
        return null;
    }
    private JSONArray findDataByCityCode(String cityCode){
        JSONArray tempCityArray=findDataByProvince(cityCode);
        for (int i=0;i<=tempCityArray.size()-1;i++) {
            JSONObject tempCityJson=(JSONObject) tempCityArray.get(i);
            String code=tempCityJson.getString("code");
            if(code.equals(cityCode)){
                JSONArray tempJsonArray=tempCityJson.getJSONArray("areaList");
                return tempJsonArray;
            }
        }
        return null;
    }

    private List<AreaResultVo> convertJsonArray(JSONArray tempJsonArray){
        List<AreaResultVo> areaResultVos=new ArrayList<>();
        for (int i=0;i<=tempJsonArray.size()-1;i++) {
            JSONObject tempCityJson=(JSONObject) tempJsonArray.get(i);
            AreaResultVo areaResultVo=new AreaResultVo();
            areaResultVo.setAreaCode(tempCityJson.getString("code"));
            areaResultVo.setAreaName(tempCityJson.getString("name"));
            areaResultVos.add(areaResultVo);
        }
        return areaResultVos;
    }*/
}
