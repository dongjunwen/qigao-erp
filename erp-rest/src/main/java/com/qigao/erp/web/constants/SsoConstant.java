package com.qigao.erp.web.constants;


import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;

public class SsoConstant {
    /**
     * redirect url, for client
     */
    public static final String REDIRECT_URL = "redirect_url";
    /**
     * sso sessionid, between browser and sso-server
     */
    public static final String SSO_TOKENID = "Authorization";

    public static final String TOKEN_HEADER="TOKEN:";
    /**
     * 存储shiro登录信息
     */
    public static final String SHIRO_PASS_TOKEN = "cs_shiro_passtoken";

    public static final String SSO_USER = "cs_sso_user";
    /**
     * sso server address
     */
    public static final String SSO_SERVER = "sso_server";

    /**
     * app login url
     */
    public static final String SSO_APP_LOGIN = "/app/login";

    /**
     * login url
     */
    public static final String SSO_LOGIN = "/login";
    /**
     * logout url
     */
    public static final String SSO_LOGOUT = "/logout";

    /**
     * filter logout path
     */
    public static final String SSO_LOGOUT_PATH = "logoutPath";
    /**
     * oper Log redis Key
     */
    public static final String SSO_OPER_LOG_KEY = "SSO_OPER_LOG_KEY";

    public static final String SSO_OPER_LOG_TEMP_KEY = "SSO_OPER_LOG_TEMP_KEY";

    public static final Result<String> SSO_LOGIN_FAIL_RESULT = Result.newError(ResultCode.USER_NO_LOGGED_IN);

}
