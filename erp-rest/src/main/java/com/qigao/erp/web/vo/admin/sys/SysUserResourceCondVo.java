package com.qigao.erp.web.vo.admin.sys;

import com.qigao.erp.web.vo.PageVo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author:luiz
 * @Date: 2018/3/13 11:29
 * @Descripton:
 * @Modify :
 **/
@Getter
@Setter
@ToString
public class SysUserResourceCondVo extends PageVo {
    private String userNo;
    private String userName;
    private String sourceNo;
    private String sourceName;
    private String psourceNo;
}
