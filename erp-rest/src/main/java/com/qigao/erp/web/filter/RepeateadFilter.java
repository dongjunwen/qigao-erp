package com.qigao.erp.web.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author luiz
 * @Title: RepeateadFilter
 * @ProjectName csuser
 * @Description: TODO
 * @date 2018-12-27 17:27
 */
public class RepeateadFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            request = new RepeatedlyReadRequestWrapper((HttpServletRequest) request);
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
