package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/4 17:03
 * @Description:
 */
@Data
@ApiModel("添加购物车实体")
public class ShopCartCreateVo {
    @NotNull(message = "商品编号不能为空!")
    @Length(min = 1,max = 32)
    @ApiModelProperty("商品编号")
    private String itemNo;
    @NotNull(message = "商品编号不能为空!")
    @Length(min = 1,max = 32)
    @ApiModelProperty("库存编号")
    private String stockNo;
    @NotNull(message = "商品数量不能为空!")
    @Length(min = 1,max = 16)
    @DecimalMin(value = "0.01",message = "购买数量不合法")
    @ApiModelProperty("购买数量")
    private String buyNum;
}
