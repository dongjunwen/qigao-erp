package com.qigao.erp.web.vo.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @Date:2017/11/6 0006 16:31
 * @Author lu.dong
 * @Description：
 **/
@Data
@ApiModel(value = "用户操作实体 SysUserFogetPassVo")
public class UserModiVo {

    @NotNull(message = "邮箱或者昵称不能为空")
    @ApiModelProperty(value = "登录号(邮箱或者昵称)", required = true)
    private String loginNo;
    @NotNull(message = "老密码不能为空")
    @ApiModelProperty(value = "老密码", required = true)
    private String oldPass;
    @Length(min = 6, max = 16, message = "新密码不能为空")
    @ApiModelProperty(value = "新密码", required = true)
    private String passwordNew1;
    @Length(min = 6, max = 16, message = "确认新密码不能为空")
    @ApiModelProperty(value = "确认新密码", required = true)
    private String passwordNew2;


}
