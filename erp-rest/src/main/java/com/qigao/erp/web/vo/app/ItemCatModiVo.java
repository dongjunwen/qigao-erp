package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/11 10:35
 * @Description:商品分类查询参数
 */
@Data
public class ItemCatModiVo {
    @ApiModelProperty("上级分类编号 可以为空")
    private String pCatNo;
    @ApiModelProperty("分类编号 不能为空")
    @NotNull(message = "分类编号不能为空")
    private String itemCatNo;
    @ApiModelProperty("分类名称")
    @NotNull(message = "分类名称不能为空")
    @Length(min = 1,max = 64,message = "分类名称长度在1-64之间")
    private String catName;
    @ApiModelProperty("分类排序")
    private String itemSort="1";
}
