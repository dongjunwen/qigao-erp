package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: AddrCreateVo
 * Author:   luiz
 * Date:     2019/11/10 16:55
 * Description: 地址创建
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/10 16:55       版本号              描述
 */
@Data
public class AddrCreateVo {
    @ApiModelProperty("收件人")
    private String recvName;
    @ApiModelProperty("联系电话")
    private String recvPhone;
    @ApiModelProperty("联系手机")
    private String recvMobile;
    @ApiModelProperty("省份代码")
    private String provinceCode;
    @ApiModelProperty("省份名称")
    private String recvProvince;
    @ApiModelProperty("城市代码")
    private String cityCode;
    @ApiModelProperty("城市名称")
    private String recvCity;
    @ApiModelProperty("区县代码")
    private String districtCode;
    @ApiModelProperty("区县名称")
    private String recvDistrict;
    @ApiModelProperty("详细地址")
    private String recvAddress;
    @ApiModelProperty("邮编")
    private String recvZip;
    @ApiModelProperty("是否默认 Y：是 N:否")
    private String ifDefault;

}
