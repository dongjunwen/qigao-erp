package com.qigao.erp.web.vo.api;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "用户查询条件")
public class UserCondVo extends PageVo {
    @ApiModelProperty(value = "姓名", required = false)
    private String userName;
    @ApiModelProperty(value = "推荐人", required = false)
    private String inviteUserName;
}
