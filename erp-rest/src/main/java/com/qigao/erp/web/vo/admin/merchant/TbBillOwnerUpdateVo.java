package com.qigao.erp.web.vo.admin.merchant;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Date:2017/12/25 0025 14:46
 * @Author lu.dong
 * @Description：
 **/
@Data
@NoArgsConstructor
@ApiModel(value = "费用项角色更新操作实体")
public class TbBillOwnerUpdateVo {
    @ApiModelProperty("费用项编码")
    @NotNull(message = "费用项编码不能为空!")
    private String itemCode;
    @ApiModelProperty("费用项名称")
    private String itemName;
    @ApiModelProperty("费用项角色对应列表")
    private List<TbBillOwnerVo> tbBillOwnerVos;
}
