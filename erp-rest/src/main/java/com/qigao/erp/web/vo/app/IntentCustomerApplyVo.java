package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2020-04-2020/4/22 17:53
 * @Description:
 */
@Data
public class IntentCustomerApplyVo {
    @ApiModelProperty("客户名称")
    //@NotNull(message = "客户名称不能为空!")
    private String customerName;
    @ApiModelProperty("联系方式")
    @NotNull(message = "联系方式不能为空!")
    private String customerPhone;
    @ApiModelProperty("需求内容")
    private String noticeContent;
}
