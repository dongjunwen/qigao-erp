package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/20 9:29
 * @Description:
 */
@Data
@ApiModel("自定义设置")
public class SelfSettingVo {
    @ApiModelProperty("最小免费订单额度")
    private String minOrderAmt;
    @ApiModelProperty("运费")
    private String shippingAmt;
    @ApiModelProperty("工作开始时间")
    private String startWorkTime;
    @ApiModelProperty("工作结束时间")
    private String endWorkTime;
}
