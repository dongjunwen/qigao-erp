package com.qigao.erp.web.vo.admin.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * @Date:2017/11/6 0006 16:31
 * @Author lu.dong
 * @Description：
 **/
@ApiModel(value = "用户操作实体 SysUserFogetPassVo")
public class SysUserFogetPassVo {

    @NotNull(message = "登录号不能为空")
    @ApiModelProperty(value = "登录号", required = true)
    private String userNo;
    @NotNull(message = "老密码不能为空")
    @ApiModelProperty(value = "老密码", required = true)
    private String oldPass;

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

}
