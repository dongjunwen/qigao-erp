package com.qigao.erp.web.vo.app;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: OrderQueryVo
 * Author:   luiz
 * Date:     2019/11/18 21:40
 * Description: 订单查询vo
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/18 21:40       版本号              描述
 */
@Data
@ApiModel("订单查询条件")
public class OrderQueryVo  extends PageVo {
    @ApiModelProperty("店铺编号")
    private String shopNo;
    @ApiModelProperty("公司编号")
    private String compNo;
    @ApiModelProperty("订单号")
    private String orderNo;
    @ApiModelProperty("订单开始日期 YYYY-MM-DD")
    private String orderDateBegin;
    @ApiModelProperty("订单结束日期 YYYY-MM-DD")
    private String orderDateEnd;
    @ApiModelProperty("收款日期开始 YYYY-MM-DD")
    private String confirmDateBegin;
    @ApiModelProperty("收款日期结束 YYYY-MM-DD")
    private String confirmDateEnd;
    @ApiModelProperty("订单状态")
    private String orderStatus;
    @ApiModelProperty("收货用户号/客户用户号")
    private String recvUserNo;
    @ApiModelProperty("收货手机号/客户手机号")
    private String recvPhone;
    @ApiModelProperty("下单人/销售员")
    private String buylerName;
    @ApiModelProperty("用户号")
    private String userNo;
}
