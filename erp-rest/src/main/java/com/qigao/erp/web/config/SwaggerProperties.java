package com.qigao.erp.web.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author luiz
 * @Title: SwaggerProperties
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-04 12:19
 */
@ConfigurationProperties(prefix = "qigao.swagger")
@Data
public class SwaggerProperties {
    /**
     * 是否打开接口调试
     */
    private Boolean enableOpen = false;
    private String basePkg;
    private String baseAppPkg;
    private String baseAdminPkg;
    private Boolean enableAuth = false;
    private String title;
    private String termOfService;
    private String contact;
    private String description;
}
