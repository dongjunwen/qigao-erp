package com.qigao.erp.web.controller.admin.sys;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.qigao.erp.commons.SysResourceService;
import com.qigao.erp.commons.dto.SysResourceCondDto;
import com.qigao.erp.commons.dto.SysResourceCreateDto;
import com.qigao.erp.commons.dto.SysResourceModiDto;
import com.qigao.erp.commons.dto.SysResourceResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.jdbc.model.SysResource;
import com.qigao.erp.web.utils.RequestUtils;
import com.qigao.erp.web.utils.ValidatorUtil;
import com.qigao.erp.web.vo.admin.sys.SysResourceCondVo;
import com.qigao.erp.web.vo.admin.sys.SysResourceCreateVo;
import com.qigao.erp.web.vo.admin.sys.SysResourceModiVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Date:2017/10/20 0020 14:03
 * @Author lu.dong
 * @Description：
 **/
@RestController
@RequestMapping("/admin/sysResource")
@Api(tags = "Admin菜单资源", description = "菜单资源相关api")
public class SysResourceController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    SysResourceService sysResourceService;

    /**
     * restful api 增删改查
     */
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ApiOperation(value = "创建菜单资源", notes = "根据菜单资源对象创建菜单资源")
    @ApiParam(name = "sysResourceVo", value = "菜单资源信息实体 sysResourceVo", required = true)
    public Result<String> create(@RequestBody SysResourceCreateVo sysResourceCreateVo, HttpServletRequest request) {
        ValidatorUtil.validateEntity(sysResourceCreateVo);
        try {
            SysResourceCreateDto sysResourceCreateDto = new SysResourceCreateDto();
            BeanUtils.copyProperties(sysResourceCreateVo, sysResourceCreateDto);
            sysResourceCreateDto.setUserNo(RequestUtils.getCurrentUserNo(request));
            return sysResourceService.create(sysResourceCreateDto);
        } catch (Exception e) {
            logger.error("添加菜单资源异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @ApiOperation(value = "更新菜单资源详细信息", notes = "根据url的菜单资源编号来指定更新对象，并根据传过来的菜单资源信息来更新菜单资源详细信息")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public Result<String> update(@RequestBody SysResourceModiVo sysResourceModiVo, HttpServletRequest request) {
        ValidatorUtil.validateEntity(sysResourceModiVo);
        try {
            SysResourceModiDto sysResourceModiDto = new SysResourceModiDto();
            BeanUtils.copyProperties(sysResourceModiVo, sysResourceModiDto);
            sysResourceModiDto.setId(Integer.valueOf(sysResourceModiVo.getId()));
            sysResourceModiDto.setUserNo(RequestUtils.getCurrentUserNo(request));
            return sysResourceService.update(sysResourceModiDto);
        } catch (Exception e) {
            logger.error("修改菜单资源异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "删除菜单资源", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(name = "id", value = "菜单资源编号", required = true, dataType = "string", paramType = "path")
    public Result<String> delete(@PathVariable("id") String id) {
        try {
            return sysResourceService.delete(id);
        } catch (Exception e) {
            logger.error("删除菜单资源异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "获取菜单资源详细信息", notes = "根据url的id来获取菜单资源详细信息")
    @ApiImplicitParam(name = "id", value = "菜单资源编号", required = true, dataType = "string", paramType = "path")
    public Result<SysResource> getEntityByNo(@PathVariable("id") String id) {
        return sysResourceService.findById(id);
    }

    @RequestMapping(value = "/currentUser", method = RequestMethod.GET)
    @ApiOperation(value = "根据当前用户获取菜单列表", notes = "根据当前用户获取菜单列表")
    // @ApiImplicitParam(name = "sourceNo", value = "菜单资源编号", required = true, dataType = "string",paramType = "path")
    public Result<List<SysResourceResultDto>> getListByCurrentUser(HttpServletRequest request) {
        return sysResourceService.getListByCurrentUser(RequestUtils.getCurrentUserNo(request));
    }

    @RequestMapping(value = "/findResourceLike/{condStr}", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    @ApiOperation(value = "根据条件模糊查询资源列表", notes = "根据条件模糊查询资源列表")
    @ApiImplicitParam(name = "condStr", value = "查询条件", required = true, dataType = "string", paramType = "path")
    public Result<List<SysResourceResultDto>> findResourceLike(@PathVariable("condStr") String condStr) {
        return sysResourceService.findReSourceLike(condStr);
    }


    @RequestMapping(value = "listPage", method = RequestMethod.POST)
    @ApiOperation(value = "资源分页查询", notes = "资源分页查询")
    @ApiParam(name = "sysResourceCondVo", value = "资源分页查询 sysResourceCondVo", required = true)
    public Result listPage(@RequestBody SysResourceCondVo sysResourceCondVo) {
        ValidatorUtil.validateEntity(sysResourceCondVo);
        try {
            SysResourceCondDto sysResourceCondDto = new SysResourceCondDto();
            BeanUtils.copyProperties(sysResourceCondVo, sysResourceCondDto);
            Page<SysResource> sysResourcePage = sysResourceService.listPage(sysResourceCondDto);
            PageInfo pageInfo = new PageInfo(sysResourcePage);
            return Result.newSuccess(pageInfo);
        } catch (Exception e) {
            return Result.newError(ResultCode.FAIL);
        }

    }

    @RequestMapping(value = "listAll", method = RequestMethod.POST)
    @ApiOperation(value = "资源查询", notes = "资源查询")
    public Result listAll(HttpServletRequest request) {
        return sysResourceService.listAll();
    }


    @RequestMapping(value = "listTree", method = RequestMethod.POST)
    @ApiOperation(value = "资源查询", notes = "资源查询")
    public Result listTree(HttpServletRequest request) {
        return sysResourceService.listTree();
    }


}
