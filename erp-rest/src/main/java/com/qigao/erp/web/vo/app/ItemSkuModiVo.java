package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/4 15:15
 * @Description:
 */
@Data
public class ItemSkuModiVo extends ItemSkuCreateVo {
    @ApiModelProperty("库存编号")
    private String stockNo;
}
