package com.qigao.erp.web.vo.admin.merchant;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.admin
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-18 15:13
 * @Description:
 */
@ApiModel("费用项修改")
@Data
public class TbBillItemModiVo extends TbBillItemCreateVo {
    @NotEmpty(message = "主键ID不能为空")
    @ApiModelProperty(value = "主键ID", required = true)
    private String id;
}
