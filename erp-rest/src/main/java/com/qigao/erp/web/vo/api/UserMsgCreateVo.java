package com.qigao.erp.web.vo.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: UserMsgCreateVo
 * @ProjectName tokyo
 * @Description: TODO
 * @date 2019-04-08 10:48
 */
@Data
public class UserMsgCreateVo {
    @ApiModelProperty("上级消息编号")
    private String pMsgNo;
    @ApiModelProperty("接受者用户编号")
    private String recvUserNo;
    @ApiModelProperty("站内信标题")
    private String msgTitle;
    @ApiModelProperty("站内信内容")
    private String msgContent;
}
