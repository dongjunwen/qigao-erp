package com.qigao.erp.web.utils;

import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.commons.exceptions.BusinessException;
import org.hibernate.validator.HibernateValidator;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

/**
 * hibernate-validator校验工具类
 * <p>
 * 参考文档：http://docs.jboss.org/hibernate/validator/5.4/reference/en-US/html_single/
 *
 * @author lu.dong
 * @date 2017-03-15 10:50
 */
public class ValidatorUtil {
    private static Validator validator = Validation.byProvider(HibernateValidator.class).configure().failFast(true).buildValidatorFactory().getValidator();

    /**
     * 校验对象
     *
     * @param object 待校验对象
     * @param groups 待校验的组
     * @throws BusinessException 校验不通过，则报BusinessException异常
     */
    public static void validateEntity(Object object, Class<?>... groups) {
        if (object == null) {
            throw new BusinessException(ResultCode.COMMON_PARAM_NULL);
        }
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object);
        if (!constraintViolations.isEmpty()) {
            StringBuilder msg = new StringBuilder();
            for (ConstraintViolation<Object> constraint : constraintViolations) {
                if (!StringUtils.isEmpty(msg.toString())) {
                    msg.append("\n");
                }
                msg.append(constraint.getMessage());
            }
            throw new BusinessException(ResultCode.COMMON_PARAM_INVALID.getCode(), msg.toString());
        }
    }
}
