package com.qigao.erp.web.controller.admin.sys;

import com.google.common.collect.Lists;
import com.qigao.erp.commons.SysResourcePermitService;
import com.qigao.erp.commons.dto.SysResourcePermitDto;
import com.qigao.erp.commons.dto.SysResourcePermitResultDto;
import com.qigao.erp.commons.dto.SysResourcePermitUpdateDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.commons.utils.NumberValidationUtils;
import com.qigao.erp.web.utils.RequestUtils;
import com.qigao.erp.web.utils.ValidatorUtil;
import com.qigao.erp.web.vo.admin.sys.SysResourcePermitUpdateVo;
import com.qigao.erp.web.vo.admin.sys.SysResourcePermitVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * @Author:luiz
 * @Date: 2018/3/12 15:10
 * @Descripton:
 * @Modify :
 **/
@RestController
@RequestMapping("/admin/sysResourcePermit")
@Api(tags = "Admin资源授权", description = "资源授权相关api")
public class SysResourcePermitController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    SysResourcePermitService sysResourcePermitService;

    /**
     * restful api 增删改查
     */
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ApiOperation(value = "创建资源授权", notes = "根据资源授权对象创建资源授权")
    @ApiParam(name = "sysResourcePermitVo", value = "资源授权信息实体 sysResourcePermitVo", required = true)
    public Result<String> createRoleResource(@RequestBody SysResourcePermitVo sysResourcePermitVo, HttpServletRequest request) {
        try {
            ValidatorUtil.validateEntity(sysResourcePermitVo);//校验
            SysResourcePermitDto sysResourcePermitDto = new SysResourcePermitDto();
            BeanUtils.copyProperties(sysResourcePermitVo, sysResourcePermitDto);
            sysResourcePermitDto.setOperUserNo(RequestUtils.getCurrentUserNo(request));
            return sysResourcePermitService.create(sysResourcePermitDto);
        } catch (Exception e) {
            logger.error("添加资源授权异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }


    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ApiOperation(value = "保存资源授权", notes = "批量保存资源授权")
    @ApiParam(name = "sysResourcePermitUpdateVo", value = "资源授权信息实体 sysResourcePermitUpdateVo", required = true)
    public Result<String> save(@RequestBody SysResourcePermitUpdateVo sysResourcePermitUpdateVo, HttpServletRequest request) {
        ValidatorUtil.validateEntity(sysResourcePermitUpdateVo);
        try {
            SysResourcePermitUpdateDto sysResourcePermitUpdateDto=new SysResourcePermitUpdateDto();
            sysResourcePermitUpdateDto.setSourceCode(sysResourcePermitUpdateVo.getSourceCode());
            sysResourcePermitUpdateDto.setSourceTypeEnum(sysResourcePermitUpdateVo.getSourceTypeEnum());
            List<SysResourcePermitDto> sysResourcePermitDtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(sysResourcePermitUpdateVo.getResourceNoLists())){
                for (String resourceNo : sysResourcePermitUpdateVo.getResourceNoLists()) {
                    SysResourcePermitDto sysResourcePermitDto = new SysResourcePermitDto();
                    sysResourcePermitDto.setResourceNo(resourceNo);
                    sysResourcePermitDto.setSourceCode(sysResourcePermitUpdateVo.getSourceCode());
                    sysResourcePermitDto.setSourceTypeEnum(sysResourcePermitUpdateVo.getSourceTypeEnum());
                    sysResourcePermitDto.setOperUserNo(RequestUtils.getCurrentUserNo(request));
                    sysResourcePermitDtos.add(sysResourcePermitDto);
                }
            }
            sysResourcePermitUpdateDto.setSysResourcePermitDtos(sysResourcePermitDtos);
            return sysResourcePermitService.save(sysResourcePermitUpdateDto);
        } catch (Exception e) {
            logger.error("保存资源授权异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "删除资源授权", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(name = "id", value = "资源授权编号", required = true, dataType = "string", paramType = "path")
    public Result<String> delete(@PathVariable("id") String id) {
        try {
            if (!NumberValidationUtils.isRealNumber(id)) {
                return Result.newError(ResultCode.COMMON_PARAM_INVALID);
            }
            return sysResourcePermitService.delete(id);
        } catch (Exception e) {
            logger.error("删除资源授权异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/{ids}", method = RequestMethod.POST)
    @ApiOperation(value = "批量删除资源授权", notes = "根据url的ids来指定删除对象")
    @ApiParam(name = "ids", value = "资源授权编号", required = true)
    public Result<String> deleteByIds(@RequestBody List<String> ids) {
        try {
            return sysResourcePermitService.deleteByIds(ids);
        } catch (Exception e) {
            logger.error("批量删除资源授权异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }


    @RequestMapping(value = "/findBySourceCode/{sourceCode}", method = RequestMethod.GET)
    @ApiOperation(value = "获取资源授权详细信息", notes = "根据url的sourceCode来获取资源授权详细信息")
    @ApiImplicitParam(name = "sourceCode", value = "资源授权编号", required = true, dataType = "string", paramType = "path")
    public Result<List<SysResourcePermitResultDto>> findBySourceCode(@PathVariable("sourceCode") String sourceCode) {
        return sysResourcePermitService.findBySourceCode(sourceCode);
    }

    @RequestMapping(value = "/listTreeBySourceCode/{sourceCode}", method = RequestMethod.GET)
    @ApiOperation(value = "根绝角色编码或者用户号获取资源授权信息", notes = "根绝角色编码或者用户号获取资源授权信息")
    @ApiImplicitParam(name = "sourceCode", value = "资源授权编号", required = true, dataType = "string", paramType = "path")
    public Result<List<SysResourcePermitResultDto>>listTreeBySourceCode(@PathVariable("sourceCode") String sourceCode) {
        return sysResourcePermitService.listTreeBySourceCode(sourceCode);
    }
}
