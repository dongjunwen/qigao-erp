package com.qigao.erp.web.vo.admin.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("公司创建实体")
public class SysCompanyCreateVo {
    @ApiModelProperty(value = "上级公司代码", required = true)
    private String parentCompNo;
    @NotEmpty(message = "公司代码不能为空")
    @Length(min = 1, max = 32, message = "公司代码长度不能超过32")
    @ApiModelProperty(value = "公司代码", required = true)
    private String compNo;
    @Length(max = 64, message = "公司名称长度不能超过64")
    @ApiModelProperty(value = "公司名称", required = true)
    private String compName;
    @NotNull(message = "公司类型不能为空")
    @ApiModelProperty(value = "公司类型", required = false)
    private String compType;
    @NotNull(message = "公司级别不能为空")
    @ApiModelProperty(value = "公司级别", required = false)
    private String compLevel;
    @Length(max = 64, message = "联系人长度不能超过64")
    @ApiModelProperty(value = "联系人", required = false)
    private String contactName;
    @Length(max = 64, message = "联系电话长度不能超过64")
    @ApiModelProperty(value = "联系电话", required = false)
    private String tel;
    @Length(max = 32, message = "省份长度不能超过32")
    @ApiModelProperty(value = "省份", required = false)
    private String provinceCode;
    @ApiModelProperty(value = "省份名称", required = false)
    private String provinceName;
    @Length(max = 32, message = "城市代码长度不能超过32")
    @ApiModelProperty(value = "城市代码", required = false)
    private String cityCode;
    @ApiModelProperty(value = "城市名称", required = false)
    private String cityName;
    @Length(max = 32, message = "区县代码长度不能超过32")
    @ApiModelProperty(value = "区县代码", required = false)
    private String districtCode;
    @ApiModelProperty(value = "区县名称", required = false)
    private String districtName;
    @ApiModelProperty(value = "详细地址", required = false)
    private String contactAddr;
    @ApiModelProperty(value = "邮政编码", required = false)
    private String recvZip;
}
