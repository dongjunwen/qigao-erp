package com.qigao.erp.web.vo;


public class PageVo {
    //第N页
    private int pageNum = 1;
    //每页显示行数
    private int pageSize = 50;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
