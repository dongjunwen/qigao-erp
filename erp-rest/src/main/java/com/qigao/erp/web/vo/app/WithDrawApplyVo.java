package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019/11/20 17:59
 * @Description:
 */
@Data
@ApiModel("提现申请")
public class WithDrawApplyVo {
    @ApiModelProperty("提现金额")
    @DecimalMin(value = "0.01",message = "最小金额为0.01")
    private String withDrawAmt;
    @ApiModelProperty("提现原因")
    private String withDrawReason;

}
