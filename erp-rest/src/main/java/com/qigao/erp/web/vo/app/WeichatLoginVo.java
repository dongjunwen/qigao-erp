package com.qigao.erp.web.vo.app;

import lombok.Data;

@Data
public class WeichatLoginVo {
    /**
     * 微信小程序验证CODE
     */
    private String weichatCode;
}
