package com.qigao.erp.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan("com.qigao.erp")
@MapperScan(value = {"com.qigao.erp.jdbc.mapper"})
@EnableTransactionManagement
public class ErpMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(ErpMainApplication.class);
    }
}
