package com.qigao.erp.web.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "qigao.user")
@Configuration
@Data
public class ShiroProperties {
    private String shiroAnon;
    private String shiroAuth;
    private Boolean enableCros = false;
}
