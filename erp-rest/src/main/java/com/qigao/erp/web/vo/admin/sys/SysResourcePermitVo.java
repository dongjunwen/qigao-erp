package com.qigao.erp.web.vo.admin.sys;

import com.qigao.erp.commons.enums.SourceTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Date:2017/12/25 0025 14:46
 * @Author lu.dong
 * @Description：
 **/
@Data
@NoArgsConstructor
@ApiModel(value = "资源授权操作实体")
public class SysResourcePermitVo {
    @NotEmpty(message = "角色代码不能为空")
    @Length(min = 1, max = 32, message = "角色代码长度不能超过32")
    @ApiModelProperty(value = "角色代码", required = true)
    private String sourceNo;
    @ApiModelProperty("来源类型")
    @NotNull(message = "来源类型不能为空!")
    private SourceTypeEnum sourceTypeEnum;
    @NotEmpty(message = "资源代码不能为空")
    @Length(min = 1, max = 32, message = "资源代码长度不能超过32")
    @ApiModelProperty(value = "资源代码", required = true)
    private String resourceNo;
}
