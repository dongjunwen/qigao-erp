package com.qigao.erp.web.vo.app;

import com.qigao.erp.commons.enums.AuthTypeEnum;
import com.qigao.erp.commons.enums.WebSourceEnum;
import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: ThirdAuthVo
 * Author:   luiz
 * Date:     2019/12/15 11:04
 * Description: 第三方授权登录
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/12/15 11:04       版本号              描述
 */
@Data
public class ThirdAuthVo {
    /**
     * 应用类型 微信、支付宝、新浪等
     */
    private AuthTypeEnum authTypeEnum;
    /**
     * 请求来源
     */
    private WebSourceEnum webSourceEnum=WebSourceEnum.H5_WEB;
}
