package com.qigao.erp.web.controller.admin.sys;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.qigao.erp.commons.SysOperLogService;
import com.qigao.erp.commons.dto.SysOperLogCondDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.jdbc.model.SysOperLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author luiz
 * @Title: SysOperLogController
 * @ProjectName user
 * @Description: TODO
 * @date 2019-04-01 15:12
 */
@RestController
@RequestMapping("/admin/sysOper")
@Api(tags = "Admin日志操作", description = "日志操作相关api")
public class SysOperLogController {
    @Resource
    private SysOperLogService sysOperLogService;

    @RequestMapping(value = "listPage", method = RequestMethod.POST)
    @ApiOperation(value = "日志操作分页查询", notes = "日志操作分页查询")
    @ApiParam(name = "sysOperLogCondDto", value = "日志操作分页查询", required = true)
    //  @RequiresPermissions("SYS-USER-LIST")
    public Result listPage(@RequestBody SysOperLogCondDto sysOperLogCondDto) {
        Page<SysOperLog> sysOperLogPage = sysOperLogService.listPage(sysOperLogCondDto);
        PageInfo pageInfo = new PageInfo(sysOperLogPage);
        return Result.newSuccess(pageInfo);
    }
}
