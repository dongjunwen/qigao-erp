package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 11:28
 * @Description:
 */
@Data
@ApiModel("创建商品图片")
public class ItemPicVo {
    @NotNull(message = "缩略图片编号不能为空!")
    @Length(min = 1,max = 32)
    @ApiModelProperty("缩略图片编号")
    private String narrowPicNo;
    @NotNull(message = "图片编号不能为空!")
    @Length(min = 1,max = 32)
    @ApiModelProperty("图片编号")
    private String picNo;
   /** @NotNull(message = "图片标题不能为空!")
    @Length(min = 1,max = 128)
    @ApiModelProperty("图片标题")
    private String picTitle;
    @ApiModelProperty("设置为主图 0:否 1:是")
    private String isMain;**/
}
