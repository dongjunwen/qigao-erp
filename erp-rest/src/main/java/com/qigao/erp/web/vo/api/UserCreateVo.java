package com.qigao.erp.web.vo.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@ApiModel(value = "用户创建实体 UserCreateVo")
public class UserCreateVo {
    @ApiModelProperty(value = "姓名", required = true)
    @NotEmpty(message = "姓名不能为空!")
    private String userName;
    @ApiModelProperty(value = "手机号", required = false)
    @NotEmpty(message = "手机号不能为空!")
    private String phoneNum;
    @ApiModelProperty(value = "电话号", required = false)
    private String tel;
    @ApiModelProperty(value = "默认登录密码 123456", required = false)
    private String loginPass="123456";
    @ApiModelProperty(value = "邮箱地址", required = false)
    private String emailAddr;
    @ApiModelProperty(value = "所属公司编号", required = false)
    private String compNo;
    @ApiModelProperty(value = "所属公司名称", required = false)
    private String compName;
    @ApiModelProperty(value = "省份编号", required = false)
    private String provinceCode;
    @ApiModelProperty(value = "省份名称", required = false)
    private String provinceName;
    @ApiModelProperty(value = "城市编号", required = false)
    private String cityCode;
    @ApiModelProperty(value = "城市名称", required = false)
    private String cityName;
    @ApiModelProperty(value = "区县编号", required = false)
    private String districtCode;
    @ApiModelProperty(value = "区县名称", required = false)
    private String districtName;
    @ApiModelProperty(value = "详细地址", required = false)
    private String contactAddr;
    private Integer userSource;
    @ApiModelProperty(value = "用户类型", required = false)
    private Integer userType;
    @ApiModelProperty(value = "用户级别", required = false)
    private String userLevel;
    @ApiModelProperty(value = "个人推荐码", required = false)
    private String inviteCode;
    @ApiModelProperty(value = "被推荐人", required = false)
    private String inviteUserNo;
    @ApiModelProperty(value = "是否转正 0:不是 1:是 ", required = false)
    private Integer ifFormal;
    @ApiModelProperty(value = "是否管理员 0:不是 1:是 ", required = false)
    private Integer ifAdmin;
    @ApiModelProperty(value = "用户状态 0:禁用 1:启用 ", required = false)
    private Integer status;
    @ApiModelProperty(value = "入职日期", required = false)
    private String entryDate;
    @ApiModelProperty(value = "转正日期", required = false)
    private String formalDate;

}
