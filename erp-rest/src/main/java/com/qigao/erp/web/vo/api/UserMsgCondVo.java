package com.qigao.erp.web.vo.api;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("站内信查询条件")
public class UserMsgCondVo extends PageVo {
    @ApiModelProperty(value = "接受者用户编号", required = false)
    private String recvUserNo;
    @ApiModelProperty(value = "发送者用户编号", required = false)
    private String sendUserNo;
    @ApiModelProperty(value = "站内信状态 0:未被阅读 1:已阅读", required = false)
    private String status;
}
