package com.qigao.erp.web.vo.app;

import com.qigao.erp.commons.enums.PayTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 13:21
 * @Description:
 */
@Data
@ApiModel("订单创建实体")
public class OrderCreateVo {
   @ApiModelProperty("付款类型 0:在线支付 1:线下支付 ")
    /* @NotNull(message = "付款类型不能为空!")
    @Min(value = 0,message = "付款类型最小值为0")
    @Max(value = 1,message = "付款类型最大值为1")*/
    private PayTypeEnum payType=PayTypeEnum.ONLINE_PAY;
   @ApiModelProperty("取货方式 0:送货上门 1:自提 ")
    /* @NotNull(message = "付款类型不能为空!")
    @Min(value = 0,message = "付款类型最小值为0")
    @Max(value = 1,message = "付款类型最大值为1")*/
   private String recvType="0";
    // @ApiModelProperty("订单来源 0:单个商品 1:购物车 默认为0")
   // private String orderSource;//是否来自购物车
    @ApiModelProperty("订单类型 NORMAL:正常订单 RESALE:重销订单 字典类型：ORDER_TYPE")
    private String orderType;
    @ApiModelProperty("联系人")
    private String recvUserNo;
    @NotNull(message = "联系人不能为空!")
    @ApiModelProperty("联系人")
    private String recvName;
    @ApiModelProperty("联系人电话")
    @NotNull(message = "联系人电话不能为空!")
    private String recvPhone;
    @ApiModelProperty("省份代码")
    private String provinceCode;
    @ApiModelProperty("省份")
    @NotNull(message = "省份不能为空!")
    private String recvProvince;
    @ApiModelProperty("城市代码")
    private String cityCode;
    @ApiModelProperty("城市")
    @NotNull(message = "城市不能为空!")
    private String recvCity;
    @ApiModelProperty("区县代码")
    private String districtCode;
    @ApiModelProperty("区县")
    @NotNull(message = "区县不能为空!")
    private String recvDistrict;
    @ApiModelProperty("地址")
    @NotNull(message = "地址不能为空!")
    private String recvAddress;
   /* @ApiModelProperty("邮编")
    @Length(min = 6,max = 6,message = "邮编长度应为6")
    private String recvZip="000000";*/
   @ApiModelProperty("物流公司")
   private String shippingWay;
   @ApiModelProperty("物流单号")
   private String shippingNo;
    @ApiModelProperty("运费")
    @DecimalMin(value = "0.00",message = "运费最小数字为0.00")
    private String shippingAmt="0.00";
    //买家备注
    @ApiModelProperty("买家备注")
    private String buylerMemo;
    @ApiModelProperty("店铺编号")
    @NotNull(message = "店铺编号不能为空!")
    private String shopNo;
    @ApiModelProperty("客户获取积分")
    @DecimalMin(value = "0.00",message = "客户获取积分最小数字为0.00")
    private String customerScore="0.00";
    @ApiModelProperty("订单明细")
    private List<OrderCreateDetailVo> orderCreateDetailVos;
}
