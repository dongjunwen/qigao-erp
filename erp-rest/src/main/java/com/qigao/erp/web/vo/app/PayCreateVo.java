package com.qigao.erp.web.vo.app;

import com.qigao.erp.commons.enums.PayWayEnum;
import com.qigao.erp.commons.enums.WebSourceEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/8 17:13
 * @Description:
 */
@Data
public class PayCreateVo {
    @ApiModelProperty("订单号")
    @NotNull(message = "订单号不能为空!")
    private String orderNo;
    @ApiModelProperty("支付方式")
    private PayWayEnum payWay;
    @ApiModelProperty("网页来源")
    private WebSourceEnum webSource;
}
