package com.qigao.erp.web.vo.admin.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @Date:2017/10/19 0019 15:26
 * @Author lu.dong
 * @Description：
 **/
@Data
@ApiModel(value = "用户角色操作实体 SysUserRoleVo")
public class SysUserRoleVo implements Serializable {

    @NotEmpty(message = "角色代码不能为空")
    @ApiModelProperty(value = "角色代码", required = true)
    private String roleCode;
    @NotEmpty(message = "角色名称不能为空")
    @ApiModelProperty(value = "角色名称", required = true)
    private String roleName;

}
