package com.qigao.erp.web.vo.app;

import com.qigao.erp.commons.enums.AuditActionEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/20 17:59
 * @Description:
 */
@Data
@ApiModel("退款审核")
public class RefundAuditVo {
    @ApiModelProperty("退款单号")
    @NotNull(message = "退款单号不能为空")
    private String refundOrderNo;
    @ApiModelProperty("实际退款金额")
    @NotNull(message = "实际退款金额不能为空")
    private String actRefundAmt;
    @ApiModelProperty("退款单动作")
    @NotNull(message = "审核动作不能为空")
    private AuditActionEnum auditActionEnum;
    @ApiModelProperty("审核备注")
    @Length(max = 128,message = "审核备注不能超过128")
    private String auditDesc;

}
