package com.qigao.erp.web.vo.admin.merchant;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.admin
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-18 15:13
 * @Description:
 */
@Data
@ApiModel("费用项")
public class TbBillItemCreateVo {
    @ApiModelProperty("用户类型 字典类型：USER_TYPE")
    private Integer userType;
    @ApiModelProperty("用户级别 字典类型：USER_LEVEL")
    private String userLevel;
    @ApiModelProperty("费用项 字典类型：BILL_ITEM")
    private String itemCode;
    @ApiModelProperty("分润类型 字典类型：ITEM_TYPE")
    private String itemType;
    @ApiModelProperty("分润值")
    private BigDecimal profitPercent=BigDecimal.ZERO;
}
