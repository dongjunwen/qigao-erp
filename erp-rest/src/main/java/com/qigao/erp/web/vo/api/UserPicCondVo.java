package com.qigao.erp.web.vo.api;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserPicCondVo extends PageVo {
    @ApiModelProperty(value = "用户编号")
    private String userNo;
    @ApiModelProperty(value = "开始日期 yyyy-mm-dd格式")
    private String beginDate;
    @ApiModelProperty(value = "结束日期 yyyy-mm-dd格式")
    private String endDate;
}
