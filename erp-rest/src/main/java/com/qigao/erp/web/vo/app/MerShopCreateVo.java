package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MerShopCreateVo {
    @ApiModelProperty("商户号")
    private String merNo;
    @ApiModelProperty("商户名称")
    private String merName;
    @ApiModelProperty("店铺名称")
    @NotNull(message = "店铺名称不能为空")
    private String shopName;
    @ApiModelProperty("省份代码")
    private String provinceCode;
    @ApiModelProperty("省份名称")
    private String recvProvince;
    @ApiModelProperty("城市代码")
    private String cityCode;
    @ApiModelProperty("城市名称")
    private String recvCity;
    @ApiModelProperty("区、县代码")
    private String districtCode;
    @ApiModelProperty("区、县名称")
    private String recvDistrict;
    @ApiModelProperty("联系地址")
    private String contactAddr;
    @ApiModelProperty("邮件地址")
    private String emailAddr;
    @ApiModelProperty("联系人")
    private String contactName;
    @ApiModelProperty("手机号")
    private String contactPhone;
    @ApiModelProperty("微信号")
    private String weichatNo;
    @ApiModelProperty("微信图片")
    private String weichatPicNo;
}
