package com.qigao.erp.web.vo.admin.merchant;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.admin.merchant
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-18 15:24
 * @Description:
 */
@Data
@ApiModel("费用项查询条件")
public class TbBillItemCondVo extends PageVo {
}
