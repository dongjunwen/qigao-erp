package com.qigao.erp.web.vo.app;

import com.qigao.erp.web.vo.PageVo;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: NoticeQueryVo
 * Author:   luiz
 * Date:     2019/11/10 15:52
 * Description: 公告查询信息
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/10 15:52       版本号              描述
 */
public class NoticeQueryVo extends PageVo {

}
