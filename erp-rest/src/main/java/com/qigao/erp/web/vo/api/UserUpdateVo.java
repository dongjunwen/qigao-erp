package com.qigao.erp.web.vo.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: UserRegisterVo
 * @ProjectName tokyo
 * @Description: TODO
 * @date 2019-04-04 10:11
 */
@Data
public class UserUpdateVo extends UserCreateVo {
    @ApiModelProperty(value = "主键ID", required = false)
    private String Id;

}
