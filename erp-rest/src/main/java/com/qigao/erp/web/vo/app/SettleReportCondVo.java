package com.qigao.erp.web.vo.app;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: OrderQueryVo
 * Author:   luiz
 * Date:     2019/11/18 21:40
 * Description: 订单查询vo
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/18 21:40       版本号              描述
 */
@Data
@ApiModel("结算单汇总报表查询条件")
public class SettleReportCondVo {
    @ApiModelProperty("结算月份 YYYY-MM格式 2020-02")
    private String settleMonth;
}
