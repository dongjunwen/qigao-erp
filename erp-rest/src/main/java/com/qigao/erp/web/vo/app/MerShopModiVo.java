package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MerShopModiVo extends MerShopCreateVo {
    @ApiModelProperty("店铺编号")
    private String id;
}
