package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/4 11:19
 * @Description:
 */
@Data
@ApiModel("商品修改")
public class ItemModiVo {
    /**
     * 商品编号
     */
    @NotNull(message = "商品编号不能为空!")
    @Length(min = 1,max = 32)
    private String itemNo;
    /**
     * 所属分类
     */
    @Length(min = 1,max = 128)
    @ApiModelProperty("所属一级分类")
    private String catNo1;
    /**
     * 标题
     */
    @NotNull(message = "商品标题不能为空!")
    @Length(min = 1,max = 128)
    @ApiModelProperty("商品标题")
    private String itemTitle;
    @ApiModelProperty("预览图片信息")
    @NotEmpty(message = "预览图片列表不能为空!")
    private List<ItemPicModiVo> itemPicTopVos;
    @ApiModelProperty("库存信息")
    @NotEmpty(message = "规格列表不能为空!")
    private List<ItemSkuModiVo> itemSkuModiVos;
    @ApiModelProperty("底部详情图片信息")
    private List<ItemPicModiVo> itemPicDetailVos;
    @ApiModelProperty("店铺号")
    private String shopNo;
}
