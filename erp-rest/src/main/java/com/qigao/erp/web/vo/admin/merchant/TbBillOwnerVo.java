package com.qigao.erp.web.vo.admin.merchant;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("费用项角色对应")
public class TbBillOwnerVo {
    @ApiModelProperty("所属类型")
    private String ownerType;
    @ApiModelProperty("所属类型名称")
    private String ownerTypeName;
}