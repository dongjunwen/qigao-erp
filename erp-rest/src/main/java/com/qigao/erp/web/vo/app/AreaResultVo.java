package com.qigao.erp.web.vo.app;

import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.utils
 * @ProjectName three-mall
 * @date 2019-11-2019/11/14 11:26
 * @Description:
 */
@Data
public class AreaResultVo {
    //区域代码
    private String areaCode;
    //区域名称
    private String areaName;
}
