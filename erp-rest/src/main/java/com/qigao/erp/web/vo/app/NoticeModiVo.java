package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: NoticeQueryVo
 * Author:   luiz
 * Date:     2019/11/10 15:52
 * Description: 公告查询信息
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/10 15:52       版本号              描述
 */
@Data
public class NoticeModiVo {
    @ApiModelProperty("公告编号")
    private String id;
    @ApiModelProperty("公告标题")
    private String noticeTitle;
    @ApiModelProperty("公告内容")
    private String noticeContent;

}
