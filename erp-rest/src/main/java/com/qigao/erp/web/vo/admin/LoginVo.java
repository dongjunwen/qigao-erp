package com.qigao.erp.web.vo.admin;

/**
 * @author luiz
 * @Title: LoginVo
 * @ProjectName csuser
 * @Description: 登录信息
 * @date 2018/11/30 14:19
 */
public class LoginVo {
    /**
     * 登录账号
     */
    private String loginNo;
    /**
     * 登录密码
     */
    private String loginPass;
    /**
     * 随机值
     */
    private String randomStr;

    /**
     * 验证码
     */
    private String verfiyCode;

    public String getLoginNo() {
        return loginNo;
    }

    public void setLoginNo(String loginNo) {
        this.loginNo = loginNo;
    }

    public String getLoginPass() {
        return loginPass;
    }

    public void setLoginPass(String loginPass) {
        this.loginPass = loginPass;
    }

    public String getVerfiyCode() {
        return verfiyCode;
    }

    public void setVerfiyCode(String verfiyCode) {
        this.verfiyCode = verfiyCode;
    }

    public String getRandomStr() {
        return randomStr;
    }

    public void setRandomStr(String randomStr) {
        this.randomStr = randomStr;
    }
}
