package com.qigao.erp.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * @author luiz
 * @Title: AppInfoController
 * @ProjectName gpay-spring-boot-starter
 * @Description: TODO
 * @date 2018-12-20 12:57
 */
@Component
public class AppVersion implements HealthIndicator {
    @Value("${application.version:default}")
    private String appVersion;

    @Override
    public Health health() {
        return new Health.Builder().withDetail("application.version", appVersion).up().build();
    }
}
