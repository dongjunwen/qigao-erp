package com.qigao.erp.web.vo.admin.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @Date:2017/11/6 0006 16:31
 * @Author lu.dong
 * @Description：
 **/
@ApiModel(value = "用户操作实体 SysUserModiPassVo")
public class SysUserModiPassVo {

    @NotNull(message = "登录号不能为空")
    @ApiModelProperty(value = "登录号", required = true)
    private String userNo;

    @Length(min = 6, max = 16, message = "新密码不能为空")
    @ApiModelProperty(value = "新密码", required = true)
    private String passwordNew1;
    @Length(min = 6, max = 16, message = "确认新密码不能为空")
    @ApiModelProperty(value = "确认新密码", required = true)
    private String passwordNew2;

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getPasswordNew1() {
        return passwordNew1;
    }

    public void setPasswordNew1(String passwordNew1) {
        this.passwordNew1 = passwordNew1;
    }

    public String getPasswordNew2() {
        return passwordNew2;
    }

    public void setPasswordNew2(String passwordNew2) {
        this.passwordNew2 = passwordNew2;
    }
}
