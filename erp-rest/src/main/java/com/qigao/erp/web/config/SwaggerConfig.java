package com.qigao.erp.web.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author luiz
 * @Title: SwaggerConfig
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-04 12:18
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${application.version:null}")
    private String appVersion;

    @Bean(name = "swaggerProperties")
    public SwaggerProperties swaggerProperties() {
        return new SwaggerProperties();
    }



    @Bean
    @ConditionalOnBean(SwaggerProperties.class)
    public Docket createCommonApi(@Qualifier("swaggerProperties") SwaggerProperties sp) {
        //添加head参数start
        List<Parameter> pars = new ArrayList<>();
        if (sp.getEnableAuth()) {
            ParameterBuilder tokenPar = new ParameterBuilder();
            tokenPar.name("Authorization").description("令牌").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
            pars.add(tokenPar.build());
         /*   ParameterBuilder headPar = new ParameterBuilder();
            headPar.name("Content-Type").description("类型").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
            pars.add(headPar.build());*/
        }

        //添加head参数end
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("共用操作相关Api")
                .apiInfo(apiInfo(sp))
                .select()
                .apis(RequestHandlerSelectors.basePackage(sp.getBasePkg()))
                .paths(PathSelectors.regex("/common.*"))
                .build()
                .globalOperationParameters(pars)
                .enable(sp.getEnableOpen());
    }



    @Bean
    @ConditionalOnBean(SwaggerProperties.class)
    public Docket createAppApi(@Qualifier("swaggerProperties") SwaggerProperties sp) {
        //添加head参数start
        List<Parameter> pars = new ArrayList<>();
        if (sp.getEnableAuth()) {
            ParameterBuilder tokenPar = new ParameterBuilder();
            tokenPar.name("Authorization").description("令牌").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
            pars.add(tokenPar.build());
         /*   ParameterBuilder headPar = new ParameterBuilder();
            headPar.name("Content-Type").description("类型").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
            pars.add(headPar.build());*/
        }

        //添加head参数end
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("所有操作相关Api")
                .apiInfo(apiInfo(sp))
                .select()
                .apis(RequestHandlerSelectors.basePackage(sp.getBasePkg()))
                .paths(PathSelectors.regex("/.*"))
                .build()
                .globalOperationParameters(pars)
                .enable(sp.getEnableOpen());
    }


    @Bean
    @ConditionalOnBean(SwaggerProperties.class)
    public Docket createAdminApi(@Qualifier("swaggerProperties") SwaggerProperties sp) {
        //添加head参数start
        List<Parameter> pars = new ArrayList<>();
        if (sp.getEnableAuth()) {
            ParameterBuilder tokenPar = new ParameterBuilder();
            tokenPar.name("Authorization").description("令牌").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
            pars.add(tokenPar.build());
         /*   ParameterBuilder headPar = new ParameterBuilder();
            headPar.name("Content-Type").description("类型").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
            pars.add(headPar.build());*/
        }

        //添加head参数end
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("admin后台管理Api")
                .apiInfo(apiInfo(sp))
                .select()
                .apis(RequestHandlerSelectors.basePackage(sp.getBaseAdminPkg()))
                .paths(PathSelectors.regex("/admin.*"))
                .build()
                .globalOperationParameters(pars)
                .enable(sp.getEnableOpen());
    }

    private ApiInfo apiInfo(SwaggerProperties sp) {
        return new ApiInfoBuilder()
                .title(sp.getTitle())
                .termsOfServiceUrl(sp.getTermOfService())
                .description(sp.getDescription())
                .contact(new Contact(sp.getTitle(), sp.getTermOfService(), sp.getContact()))
                .version(appVersion)
                .build();
    }
}
