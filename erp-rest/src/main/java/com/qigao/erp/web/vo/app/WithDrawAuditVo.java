package com.qigao.erp.web.vo.app;

import com.qigao.erp.commons.enums.AuditActionEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/20 17:59
 * @Description:
 */
@Data
@ApiModel("提现审核")
public class WithDrawAuditVo {
    @ApiModelProperty("提现单号")
    @NotNull(message = "提现单号不能为空")
    private List<String> orderNos;
    @ApiModelProperty("审核动作")
    @NotNull(message = "审核动作不能为空")
    private AuditActionEnum auditActionEnum;
    @ApiModelProperty("审核备注")
    @Length(max = 128,message = "审核备注不能超过128")
    private String auditDesc;

}
