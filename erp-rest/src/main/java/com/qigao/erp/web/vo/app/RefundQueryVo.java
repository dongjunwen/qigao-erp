package com.qigao.erp.web.vo.app;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/21 14:13
 * @Description:
 */
@Data
@ApiModel("退款单查询条件")
public class RefundQueryVo extends PageVo {
    @ApiModelProperty("店铺编号")
    @NotNull(message = "店铺号不能为空")
    private String shopNo;
    @ApiModelProperty("退款单状态")
    private String refundStatus;
}
