package com.qigao.erp.web.vo.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author luiz
 * @Title: UserRegisterVo
 * @ProjectName tokyo
 * @Description: TODO
 * @date 2019-04-04 10:11
 */
@Data
public class UserRegisterVo {
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱地址", required = true)
    @NotEmpty(message = "邮箱不能为空")
    private String emailAddr;
    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称", required = true)
    @NotEmpty(message = "昵称不能为空")
    private String nickName;
    /**
     * 密码1
     */
    @ApiModelProperty(value = "密码", required = true)
    @NotEmpty(message = "密码不能为空")
    private String pass1;
    /**
     * 确认密码
     */
    @ApiModelProperty(value = "确认密码", required = true)
    @NotEmpty(message = "确认密码不能为空")
    private String pass2;
    @ApiModelProperty(value = "微信号")
    private String weichatNo;
    @ApiModelProperty(value = "FaceBook")
    private String facebookNo;
    @ApiModelProperty(value = "line")
    private String lineNo;
    @ApiModelProperty(value = "性别 0:男 1:女 ", required = true)
    private Integer sexType = 0;
    @ApiModelProperty(value = "年龄", required = false)
    private Integer age = 0;
    @ApiModelProperty(value = "常住城市", required = false)
    private String activeCity;
    @ApiModelProperty(value = "国籍", required = false)
    private String country;
    @ApiModelProperty(value = "语言 0:中文 1:日文 2:英文", required = false)
    private Integer languageType = 0;
    @ApiModelProperty(value = "是否激活交友功能 0:否 1:是", required = false)
    private Integer friendActive = 0;
    @ApiModelProperty(value = "身高", required = false)
    private Integer height = 0;
    @ApiModelProperty(value = "体重", required = false)
    private Integer weight = 0;
    @ApiModelProperty(value = "标签", required = false)
    private List<String> tagList;

}
