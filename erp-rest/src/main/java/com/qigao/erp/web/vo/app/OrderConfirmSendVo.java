package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 13:21
 * @Description:
 */
@Data
@ApiModel("确认发货")
public class OrderConfirmSendVo {
    @NotNull(message = "订单号不能为空!")
    @ApiModelProperty("店铺编号")
    private String shopNo;
    @NotNull(message = "订单号不能为空!")
    @ApiModelProperty("订单号")
    private String orderNo;

}
