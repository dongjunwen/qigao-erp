package com.qigao.erp.web.controller.admin.sys;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.qigao.erp.commons.SysDictService;
import com.qigao.erp.commons.dto.SysDictCondDto;
import com.qigao.erp.commons.dto.SysDictDto;
import com.qigao.erp.commons.dto.SysDictResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.web.controller.AbstractDictController;
import com.qigao.erp.web.utils.RequestUtils;
import com.qigao.erp.web.utils.ValidatorUtil;
import com.qigao.erp.web.vo.admin.sys.SysDictCondVo;
import com.qigao.erp.web.vo.admin.sys.SysDictVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author:luiz
 * @Date: 2018/2/2 10:01
 * @Descripton:
 * @Modify :
 **/
@RestController
@RequestMapping("/admin/sysDict/")
@Api(tags = "Admin数据字典")
public class SysDictController extends AbstractDictController {
    private static final Logger logger = LoggerFactory.getLogger(SysDictController.class);
    @Resource
    private SysDictService sysDictService;

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    @ApiOperation(value = "新增数据字典", notes = "新增数据字典")
    @ApiParam(name = "RiskDictVo", value = "新增数据字典 RiskDictVo", required = true)
    public Result insert(@RequestBody SysDictVo sysDictVo, HttpServletRequest request) {
        try {
            ValidatorUtil.validateEntity(sysDictVo);
            SysDictDto sysDictDto = new SysDictDto();
            BeanUtils.copyProperties(sysDictVo, sysDictDto);
            sysDictDto.setOperUserNo(RequestUtils.getCurrentUserNo(request));
            return sysDictService.create(sysDictDto);
        } catch (Exception e) {
            logger.error("[数据字典]新增数据字典异常:{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ApiOperation(value = "修改数据字典", notes = "修改数据字典")
    @ApiParam(name = "sysDictVo", value = "修改数据字典 sysDictVo", required = true)
    public Result update(@RequestBody SysDictVo sysDictVo, HttpServletRequest request) {
        try {
            ValidatorUtil.validateEntity(sysDictVo);
            SysDictDto sysDictDto = new SysDictDto();
            BeanUtils.copyProperties(sysDictVo, sysDictDto);
            sysDictDto.setOperUserNo(RequestUtils.getCurrentUserNo(request));
            sysDictDto.setId(Integer.valueOf(sysDictVo.getId()));
            return sysDictService.update(sysDictDto);
        } catch (Exception e) {
            logger.error("[数据字典]修改数据字典异常:{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "findById/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "按照字典id查询字典数据", notes = "按照字典id查询字典数据")
    @ApiParam(name = "id", value = "按照字典id查询字典数据 id", required = true)
    public Result<SysDictResultDto> findById(@PathVariable("id") String id) {
        try {
            return sysDictService.findById(id);
        } catch (Exception e) {
            logger.error("[数据字典]按照ID查询异常:{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }


    @RequestMapping(value = "listPage", method = RequestMethod.POST)
    @ApiOperation(value = "按照查询条件查询字典数据", notes = "按照查询条件查询字典数据")
    @ApiParam(name = "sysDictCondVo", value = "按照查询条件查询字典数据 sysDictCondVo", required = true)
    public Result listPage(@RequestBody SysDictCondVo sysDictCondVo) {
        try {
            SysDictCondDto sysDictCondDto = new SysDictCondDto();
            BeanUtils.copyProperties(sysDictCondVo, sysDictCondDto);
            Page<SysDictResultDto> sysDictResultDtoPage = sysDictService.findListPage(sysDictCondDto);
            PageInfo<SysDictResultDto> dictResultDtoPageInfo = new PageInfo(sysDictResultDtoPage);
            return Result.newSuccess(dictResultDtoPageInfo);
        } catch (Exception e) {
            logger.error("[数据字典]按照查询条件查询字典数据异常:{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "listPDictAll", method = RequestMethod.POST)
    @ApiOperation(value = "查询所有一级字典数据", notes = "查询所有一级字典数据")
    public Result listPDictAll() {
        try {
            return  sysDictService.listPDictAll();
        } catch (Exception e) {
            logger.error("[数据字典]按照查询条件查询字典数据异常:{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

}
