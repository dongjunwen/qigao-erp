package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 13:21
 * @Description:
 */
@Data
@ApiModel("用户订单修改实体")
public class UserOrderModiVo {
    @ApiModelProperty("订单号 ")
    @NotNull(message = "订单号不能为空!")
    private String orderNo;
    @NotNull(message = "联系人不能为空!")
    @ApiModelProperty("联系人")
    private String recvName;
    @ApiModelProperty("联系人电话")
    @NotNull(message = "联系人电话不能为空!")
    private String recvPhone;
    @ApiModelProperty("省份")
    @NotNull(message = "省份不能为空!")
    private String recvProvince;
    @ApiModelProperty("城市")
    @NotNull(message = "城市不能为空!")
    private String recvCity;
    @ApiModelProperty("区县")
    @NotNull(message = "区县不能为空!")
    private String recvDistrict;
    @ApiModelProperty("地址")
    @NotNull(message = "地址不能为空!")
    private String recvAddress;
    @ApiModelProperty("邮政编码")
    @Length(min = 6,max = 6,message = "邮编长度应为6")
    private String recvZip="000000";
    //买家备注
    @ApiModelProperty("买家备注")
    private String userMemo;

}
