package com.qigao.erp.web.vo.app;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/4 15:15
 * @Description:
 */
@Data
public class ItemParamVo {
    @NotNull(message = "参数名称不能为空!")
    @Length(min = 1,max = 32)
    private String paramName;
    @NotNull(message = "参数值不能为空!")
    private List<String> paramVal;
    //顶部显示
    private String topSHow;
}
