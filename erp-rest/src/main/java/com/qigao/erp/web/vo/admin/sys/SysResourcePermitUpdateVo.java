package com.qigao.erp.web.vo.admin.sys;

import com.qigao.erp.commons.enums.SourceTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Date:2017/12/25 0025 14:46
 * @Author lu.dong
 * @Description：
 **/
@Data
@NoArgsConstructor
@ApiModel(value = "资源授权更新操作实体")
public class SysResourcePermitUpdateVo {
    @ApiModelProperty("来源编号")
    @NotNull(message = "来源编号不能为空!")
    private String sourceCode;
    @ApiModelProperty("来源类型")
    @NotNull(message = "来源类型不能为空!")
    private SourceTypeEnum sourceTypeEnum;
    @ApiModelProperty("资源列表")
    private List<String> resourceNoLists;
}
