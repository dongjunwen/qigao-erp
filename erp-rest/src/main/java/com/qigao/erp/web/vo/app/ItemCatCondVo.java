package com.qigao.erp.web.vo.app;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/11 10:35
 * @Description:商品分类查询参数
 */
@Data
public class ItemCatCondVo extends PageVo {
    @ApiModelProperty("分类名称")
    private String catName;
}
