package com.qigao.erp.web.filter;

import com.alibaba.fastjson.JSONObject;
import com.qigao.erp.api.DataStoreService;
import com.qigao.erp.commons.enums.IfAdminEnum;
import com.qigao.erp.jdbc.model.SysUser;
import com.qigao.erp.web.constants.SsoConstant;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.filter.authc.UserFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author luiz
 * @Title: SsoTokenFilter
 * @ProjectName csuser
 * @Description: TODO
 * @date 2018/10/26 14:35
 */
@Component
public class SsoTokenFilter extends FormAuthenticationFilter {

    private DataStoreService dataStoreService;

    private static Logger logger = LoggerFactory.getLogger(SsoTokenFilter.class);

    public SsoTokenFilter(DataStoreService dataStoreService) {
        this.dataStoreService = dataStoreService;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        String resultJson = JSONObject.toJSONString(SsoConstant.SSO_LOGIN_FAIL_RESULT);
        logger.info("[sso-client]尚未登录:{}", resultJson);
        // response
        HttpServletResponse res = (HttpServletResponse) response;
        try {
            res.setStatus(HttpServletResponse.SC_OK);
            res.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(resultJson);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("[sso-client]尚未登录,发生异常:{}", e);
        }
        return false;
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object mappedValue) {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        String link = req.getRequestURL().toString();
        /**解决跨域问题 begin
        String reqOriginRealPath = req.getHeader("Referer");
        String reqPath = "*";
        if (reqOriginRealPath != null) {
            int len = reqOriginRealPath.indexOf("/", 7);
            reqPath = reqOriginRealPath.substring(0, len);
        } else {
            reqPath = req.getHeader("Origin");
        }**/
        res.setStatus(HttpServletResponse.SC_OK);
        res.setContentType("application/json;charset=UTF-8");
        res.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
        res.setHeader("Access-Control-Allow-Credentials", "true");
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE,PUT");
        res.setHeader("Access-Control-Max-Age", "86400");
        /**
         * 头部需要携带 Authorization
         */
        res.setHeader("Access-Control-Allow-Headers", "Accept,Authorization,Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With");
        res.setHeader("XDomainRequestAllowed", "1");
        /**解决跨域问题 end**/
        if (req.getMethod().equals("OPTIONS")) {
            return true;
        }
        if (this.isLoginRequest(req, res)) {
            return true;
        }
        String tokenId = req.getHeader(SsoConstant.SSO_TOKENID);
        if(StringUtils.isAllEmpty(tokenId)){
            logger.info("Authorization里TOKEN不存在");
            return false;
        }
        logger.info("[sso-client]请求地址:{},Authorization:{}", link, tokenId);
        // login filter
        if (dataStoreService.checkOverTime(tokenId)) {
            logger.info("TOKEN已经超时");
            return false;
        }
        String userInfoStr = dataStoreService.getVal(tokenId);
        if(StringUtils.isAllEmpty(userInfoStr)){
            logger.info("{}用户信息不存在,尚未登陆",tokenId);
            return false;
        }
        SysUser userInfo=JSONObject.parseObject(userInfoStr, SysUser.class);
        //刷新缓存
        dataStoreService.put(tokenId,userInfoStr,userInfo.getMerNo());
        if(link.contains("/app/merchant")||link.contains("/admin")){
            //权限校验 普通用户无法访问 管理员路径
            int adminRole=userInfo.getIfAdmin()==null?0:userInfo.getIfAdmin();
            if(IfAdminEnum.YES.getCode()!=adminRole){
                logger.error("不是系统管理员，无法访问...");
                return false;
            }
        }
        return true;
    }
}
