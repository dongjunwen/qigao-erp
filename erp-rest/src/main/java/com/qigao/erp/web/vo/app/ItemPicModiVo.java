package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 11:28
 * @Description:
 */
@Data
@ApiModel("修改商品图片")
public class ItemPicModiVo extends ItemPicVo{
    @ApiModelProperty("图片编号")
    private String id;
}
