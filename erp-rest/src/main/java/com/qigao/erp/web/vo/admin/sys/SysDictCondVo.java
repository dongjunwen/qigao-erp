package com.qigao.erp.web.vo.admin.sys;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author:luiz
 * @Date: 2018/2/5 11:02
 * @Descripton:
 * @Modify :
 **/
@Getter
@Setter
@ToString
@ApiModel(value = "数据字典存储对象", description = "数据字典存储对象")
public class SysDictCondVo extends PageVo {
    @ApiModelProperty(value = "字典代码")
    private String dictNo;
    @ApiModelProperty(value = "字典名称")
    private String dictName;
    @ApiModelProperty(value = "字典值")
    private String dictVal;
    @ApiModelProperty(value = "上级字典")
    private String pDictNo;
}
