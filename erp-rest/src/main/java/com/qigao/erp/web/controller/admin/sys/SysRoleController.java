package com.qigao.erp.web.controller.admin.sys;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.qigao.erp.commons.SysRoleService;
import com.qigao.erp.commons.dto.SysRoleCondDto;
import com.qigao.erp.commons.dto.SysRoleCreateDto;
import com.qigao.erp.commons.dto.SysRoleModiDto;
import com.qigao.erp.commons.dto.SysRoleResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.web.utils.RequestUtils;
import com.qigao.erp.web.utils.ValidatorUtil;
import com.qigao.erp.web.vo.admin.sys.SysRoleCondVo;
import com.qigao.erp.web.vo.admin.sys.SysRoleCreateVo;
import com.qigao.erp.web.vo.admin.sys.SysRoleModiVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author:luiz
 * @Date: 2018/3/12 14:43
 * @Descripton:
 * @Modify :
 **/
@RestController
@RequestMapping("/admin/sysRole")
@Api(tags = "Admin角色信息", description = "角色信息相关api")
public class SysRoleController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    SysRoleService sysRoleService;


    /**
     * restful api 增删改查
     */
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ApiOperation(value = "创建角色", notes = "根据角色对象创建角色")
    @ApiParam(name = "sysRoleVo", value = "角色信息实体 sysRoleVo", required = true)
    public Result<String> create(@RequestBody SysRoleCreateVo sysRoleCreateVo, HttpServletRequest request) {
        try {
            ValidatorUtil.validateEntity(sysRoleCreateVo);//校验
            SysRoleCreateDto sysRoleCreateDto = new SysRoleCreateDto();
            BeanUtils.copyProperties(sysRoleCreateVo, sysRoleCreateDto);
            sysRoleCreateDto.setCreateUserNo(RequestUtils.getCurrentUserNo(request));
            return sysRoleService.create(sysRoleCreateDto);
        } catch (Exception e) {
            logger.error("添加角色异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @ApiOperation(value = "更新角色详细信息", notes = "根据url的角色编号来指定更新对象，并根据传过来的角色信息来更新角色详细信息")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public Result<String> update(@RequestBody SysRoleModiVo sysRoleModiVo, HttpServletRequest request) {
        try {
            ValidatorUtil.validateEntity(sysRoleModiVo);//校验
            SysRoleModiDto sysRoleModiDto = new SysRoleModiDto();
            BeanUtils.copyProperties(sysRoleModiVo, sysRoleModiDto);
            sysRoleModiDto.setId(Integer.valueOf(sysRoleModiVo.getId()));
            sysRoleModiDto.setModiUserNo(RequestUtils.getCurrentUserNo(request));
            return sysRoleService.update(sysRoleModiDto);
        } catch (Exception e) {
            logger.error("修改角色异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "删除角色", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(name = "id", value = "角色编号", required = true, dataType = "string", paramType = "path")
    public Result<String> delete(@PathVariable("id") String id) {
        try {
            return sysRoleService.delete(id);
        } catch (Exception e) {
            logger.error("删除角色异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/findRoleLike/{condStr}", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    @ApiOperation(value = "根据条件模糊匹配角色列表", notes = "根据查询条件模糊匹配角色列表")
    @ApiImplicitParam(name = "condStr", value = "查询条件", required = true, dataType = "string", paramType = "path")
    public Result<List<SysRoleResultDto>> findMaterialLike(@PathVariable("condStr") String condStr) {
        return sysRoleService.findRoleLike(condStr);
    }

    @RequestMapping(value = "/{roleCode}", method = RequestMethod.GET)
    @ApiOperation(value = "获取角色详细信息", notes = "根据url的roleCode来获取角色详细信息")
    @ApiImplicitParam(name = "roleCode", value = "角色编号", required = true, dataType = "string", paramType = "path")
    public Result<SysRoleResultDto> getEntityByNo(@PathVariable("roleCode") String roleCode) {
        return sysRoleService.getEntityByNo(roleCode);
    }

    @RequestMapping(value = "listPage", method = RequestMethod.POST)
    @ApiOperation(value = "角色分页查询", notes = "角色分页查询")
    @ApiParam(name = "sysRoleCondVo", value = "角色分页查询 sysRoleCondVo", required = true)
    public Result listPage(@RequestBody SysRoleCondVo sysRoleCondVo) {
        try {
            ValidatorUtil.validateEntity(sysRoleCondVo);//校验
            SysRoleCondDto sysRoleCondDto = new SysRoleCondDto();
            BeanUtils.copyProperties(sysRoleCondVo, sysRoleCondDto);
            Page<SysRoleResultDto> sysRoleResultDtoPage = sysRoleService.listPage(sysRoleCondDto);
            PageInfo pageInfo = new PageInfo(sysRoleResultDtoPage);
            return Result.newSuccess(pageInfo);
        } catch (Exception e) {
            logger.error("角色分页查询异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }


    @RequestMapping(value = "listAll", method = RequestMethod.POST)
    @ApiOperation(value = "获取所有角色信息", notes = "获取所有角色信息")
    public Result<List<SysRoleResultDto>> listAll() {
        return sysRoleService.listAll();
    }


}
