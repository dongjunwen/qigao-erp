package com.qigao.erp.web.controller;

import com.alibaba.fastjson.JSONObject;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.qigao.erp.api.DataStoreService;
import com.qigao.erp.api.LoginInfoService;
import com.qigao.erp.commons.dto.AuthTokenDto;
import com.qigao.erp.commons.dto.UserModiPassDto;
import com.qigao.erp.commons.dto.UserRegisterDto;
import com.qigao.erp.commons.enums.LoginTypeEnum;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.commons.exceptions.BusinessException;
import com.qigao.erp.commons.po.UserInfo;
import com.qigao.erp.commons.utils.IDUtils;
import com.qigao.erp.web.config.QiGaoProperties;
import com.qigao.erp.web.constants.SsoConstant;
import com.qigao.erp.web.utils.IpUtils;
import com.qigao.erp.web.utils.ValidatorUtil;
import com.qigao.erp.web.vo.admin.LoginVo;
import com.qigao.erp.web.vo.admin.sys.SysUserModiPassVo;
import com.qigao.erp.web.vo.api.UserModiVo;
import com.qigao.erp.web.vo.api.UserRegisterVo;
import com.qigao.erp.web.vo.app.ThirdAuthVo;
import com.qigao.erp.web.vo.app.ThirdCheckAuthVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

/**
 * @author luiz
 * @Title:com.qigao.erp.web.controller.app
 * @ProjectName three-mall
 * @Description: App登录相关API
 * @date 2019-10-25  16:56
 */
@RestController
@RequestMapping("/common/login")
@Api(tags = "App登录相关API",value = "App登录相关API")
public class AppLoginController {
    private static Logger logger= LoggerFactory.getLogger(AppLoginController.class);
    @Resource
    LoginInfoService loginInfoService;
    @Resource
    DataStoreService dataStoreService;
    @Resource
    QiGaoProperties qiGaoProperties;
    @Resource
    DefaultKaptcha defaultKaptcha;

    /**
     * 是否显示验证码
     */
    @Value("${user.login.verify:N}")
    private String showVerify;

    @GetMapping("/showKaptcha")
    @ApiOperation(value = "验证码", notes = "验证码")
    public void defaultKaptcha(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        byte[] captchaChallengeAsJpeg = null;
        ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
        try {
            String randomStr = httpServletRequest.getParameter("d");
            //生产验证码字符串并保存到session中
            String kaptchaCode = defaultKaptcha.createText();
            //session会话在分布式环境不能保持，临时存到redis
            dataStoreService.put(randomStr, kaptchaCode,null);
            //使用生产的验证码字符串返回一个BufferedImage对象并转为byte写入到byte数组中
            BufferedImage challenge = defaultKaptcha.createImage(kaptchaCode);
            ImageIO.write(challenge, "jpg", jpegOutputStream);
            logger.info("sessionId:{},vrifyCode:{}", randomStr, kaptchaCode);
        } catch (IllegalArgumentException e) {
            httpServletResponse.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        //定义response输出类型为image/jpeg类型，使用response输出流输出图片的byte数组
        captchaChallengeAsJpeg = jpegOutputStream.toByteArray();
        httpServletResponse.setHeader("Cache-Control", "no-store");
        httpServletResponse.setHeader("Pragma", "no-cache");
        httpServletResponse.setDateHeader("Expires", 0);
        httpServletResponse.setContentType("image/jpeg");
        ServletOutputStream responseOutputStream = httpServletResponse.getOutputStream();
        responseOutputStream.write(captchaChallengeAsJpeg);
        responseOutputStream.flush();
        responseOutputStream.close();
    }

    /**
     * 注册
     * @return
     */
    @ResponseBody
    @PostMapping("/register")
    @ApiOperation(value = "注册", notes = "注册")
    public Result register(@RequestBody UserRegisterVo userRegisterVo, HttpServletRequest request){
        try{
            ValidatorUtil.validateEntity(userRegisterVo);//校验用户实体字段，
            if(!userRegisterVo.getPass1().equals(userRegisterVo.getPass2())){
                return Result.newError(ResultCode.USER_PASS_NOT_EQUAL);
            }
            UserRegisterDto userRegisterDto=new UserRegisterDto();
            BeanUtils.copyProperties(userRegisterVo,userRegisterDto);
            userRegisterDto.setRegisterIp(IpUtils.getIpAddress(request));
            return loginInfoService.doRegister(userRegisterDto);
        }catch (BusinessException be){
            logger.warn("用户注册业务异常!{}",be);
            return Result.newError(be.getCode(),be.getMsg());
        }
        catch (Exception e){
            logger.error("用户注册异常!{}",e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/active/{activeCode}",method={RequestMethod.GET,RequestMethod.POST})
    @ApiOperation(value = "激活用户", notes = "激活用户")
    public Result activeUser(@PathVariable("activeCode") String activeCode, HttpServletRequest request) {
        try {
            //校验用户实体字段，
            return loginInfoService.activeUser(activeCode);
        } catch (Exception e) {
            logger.error("激活用户异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    /**
     * Login
     *
     * @return
     */
    @PostMapping("/doLogin")
    @ResponseBody
    @ApiOperation(value = "登录接口", notes = "登录接口")
    public Result<String> login(@RequestBody LoginVo loginVo) {
        String loginNo = loginVo.getLoginNo();
        if (StringUtils.isBlank(loginNo)) {
            return Result.newError(ResultCode.COMMON_PARAM_INVALID.getCode(), "登录号不能为空");
        }
        String loginPass = loginVo.getLoginPass();
        if (StringUtils.isBlank(loginPass)) {
            return Result.newError(ResultCode.COMMON_PARAM_INVALID.getCode(), "密码不能为空");
        }
        try {
            Result result = loginInfoService.doLogin(loginNo, loginPass);
            if (!result.isSuccess()) {
                return result;
            }
            UserInfo userInfo = (UserInfo) result.getData();
            String tokenId = IDUtils.genIdStr(LoginTypeEnum.ADMIN_TOKEN.getCode());
            dataStoreService.put(tokenId, JSONObject.toJSONString(userInfo),null);
            return Result.newSuccess(tokenId);
        } catch (Exception e) {
            logger.error("登录发生异常:{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    /**
     * Logout
     *
     * @param request
     * @return
     */
    @PostMapping("/logout")
    @ResponseBody
    @ApiOperation(value = "登出接口", notes = "登出接口")
    public Result<String> logout(HttpServletRequest request) {
        String tokenId = request.getHeader(SsoConstant.SSO_TOKENID);
        if (StringUtils.isBlank(tokenId)) {
            return Result.newError(ResultCode.COMMON_PARAM_INVALID.getCode(), "tokenId 不能为空.");
        }
        // logout
        dataStoreService.del(tokenId);
        return Result.newSuccess();
    }


    @ResponseBody
    @RequestMapping(value = "modiPass", method = RequestMethod.POST)
    @ApiParam(name = "userModiVo", value = "用户修改实体 userModiVo", required = true)
    @ApiOperation(value = "修改密码接口", notes = "修改密码接口")
    public Result<String> modiPass(@RequestBody UserModiVo userModiVo) {
        try {
            //校验用户实体字段，
            ValidatorUtil.validateEntity(userModiVo);
            UserModiPassDto userModiPassDto = new UserModiPassDto();
            BeanUtils.copyProperties(userModiVo, userModiPassDto);
            return loginInfoService.doModiPass(userModiPassDto);
        } catch (Exception e) {
            logger.error("修改密码修改异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    /**
     * 忘记密码
     * @return
     */
    @ResponseBody
    @PostMapping("/forgetPass/{emailAddr}")
    @ApiOperation(value="忘记密码", notes="忘记密码")
    public Result forgetPass(@PathVariable("emailAddr") String emailAddr,HttpServletRequest request){
        try{
            return loginInfoService.forgetPass(emailAddr);
        }catch (Exception e){
            logger.error("用户忘记密码异常!{}",e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    /**
     * 忘记密码修改
     *
     * @param sysUserModiPassVo
     * @return
     */
    @ResponseBody
    @PostMapping("/modiPass/{forgetRadomStr}/")
    @ApiOperation(value="忘记密码修改", notes="忘记密码修改")
    public Result modiPass(@PathVariable("forgetRadomStr") String forgetRadomStr, @RequestBody SysUserModiPassVo sysUserModiPassVo, HttpServletRequest request) {
        try {
            //校验用户实体字段，
            ValidatorUtil.validateEntity(sysUserModiPassVo);
            UserModiPassDto userModiPassDto = new UserModiPassDto();
            BeanUtils.copyProperties(sysUserModiPassVo, userModiPassDto);
            userModiPassDto.setForgetRadomStr(forgetRadomStr);
            return loginInfoService.modiForgetPass(userModiPassDto);
        } catch (Exception e) {
            logger.error("用户忘记密码修改异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }



}
