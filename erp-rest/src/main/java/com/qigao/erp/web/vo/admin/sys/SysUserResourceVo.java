package com.qigao.erp.web.vo.admin.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @Date:2017/12/25 0025 14:46
 * @Author lu.dong
 * @Description：
 **/
@Getter
@Setter
@ToString
@ApiModel(value = "用户资源操作实体 SysUserResourceVo")
public class SysUserResourceVo {
    @NotNull(message = "用户代码不能为空")
    @Length(min = 1, max = 32, message = "用户代码长度不能超过32")
    @ApiModelProperty(value = "用户代码", required = true)
    private String userNo;
    @NotNull(message = "资源代码不能为空")
    @Length(min = 1, max = 32, message = "资源代码长度不能超过32")
    @ApiModelProperty(value = "资源代码", required = true)
    private String sourceNo;

}
