package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/4 15:15
 * @Description:
 */
@Data
public class ItemSkuVo {
    @ApiModelProperty("库存编号")
    private String stockNo;
    @ApiModelProperty("商品标题")
    private String stockTitle;
    @ApiModelProperty("库存类型 0:不限制库存 1:有库存限制")
    private String stockType="0";
    @ApiModelProperty("产品图片")
    private String picUrl;
    @ApiModelProperty("产品缩略图片")
    private String narrowPicUrl;
    /**
     * 商品价格
     */
    @NotNull(message = "商品单价不能为空!")
    @Length(min = 1,max = 16)
    @DecimalMin(value = "0.01",message = "商品单价最小为0.01")
    @DecimalMax(value = "999999999.99",message = "单价设置过大")
    @ApiModelProperty("商品单价")
    private String price;
    /**
     * 库存数量
     */
    @ApiModelProperty("库存数量")
    private String stockNum;

}
