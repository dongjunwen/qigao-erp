package com.qigao.erp.web.vo.admin.sys;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "公司查询条件", description = "公司查询条件")
public class SysCompnayCondVo extends PageVo {
    @ApiModelProperty(value = "公司名称", required = true)
    private String compName;
    @ApiModelProperty(value = "公司类型", required = false)
    private String compType;
}
