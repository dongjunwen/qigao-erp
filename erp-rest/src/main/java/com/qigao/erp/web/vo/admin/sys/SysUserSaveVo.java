package com.qigao.erp.web.vo.admin.sys;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Date:2017/10/19 0019 15:26
 * @Author lu.dong
 * @Description：
 **/
@Data
@ApiModel(value = "用户角色操作实体 SysUserSaveVo")
public class SysUserSaveVo implements Serializable {

    private String userNo;

    List<SysUserRoleVo> sysUserRoleVoList;

}
