package com.qigao.erp.web.controller.admin.sys;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.qigao.erp.commons.SysDepartService;
import com.qigao.erp.commons.dto.SysDepartCondDto;
import com.qigao.erp.commons.dto.SysDepartCreateDto;
import com.qigao.erp.commons.dto.SysDepartModiDto;
import com.qigao.erp.commons.dto.SysDepartResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.web.utils.RequestUtils;
import com.qigao.erp.web.utils.ValidatorUtil;
import com.qigao.erp.web.vo.admin.sys.SysDepartCondVo;
import com.qigao.erp.web.vo.admin.sys.SysDepartCreateVo;
import com.qigao.erp.web.vo.admin.sys.SysDepartModiVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author:luiz
 * @Date: 2018/3/12 14:43
 * @Descripton:
 * @Modify :
 **/
@RestController
@RequestMapping("/admin/sysDepart")
@Api(tags = "Admin部门信息", description = "部门信息相关api")
public class SysDepartController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    SysDepartService sysDepartService;


    /**
     * restful api 增删改查
     */
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ApiOperation(value = "创建部门", notes = "根据部门对象创建部门")
    @ApiParam(name = "sysDepartCreateVo", value = "部门信息实体 sysDepartCreateVo", required = true)
    public Result<String> create(@RequestBody SysDepartCreateVo sysDepartCreateVo, HttpServletRequest request) {
        ValidatorUtil.validateEntity(sysDepartCreateVo);//校验
        try {
            SysDepartCreateDto sysDepartCreateDto = new SysDepartCreateDto();
            BeanUtils.copyProperties(sysDepartCreateVo, sysDepartCreateDto);
            sysDepartCreateDto.setCreateUserNo(RequestUtils.getCurrentUserNo(request));
            return sysDepartService.create(sysDepartCreateDto);
        } catch (Exception e) {
            logger.error("添加部门异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @ApiOperation(value = "更新部门详细信息", notes = "根据url的部门编号来指定更新对象，并根据传过来的部门信息来更新部门详细信息")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public Result<String> update(@RequestBody SysDepartModiVo sysDepartModiVo, HttpServletRequest request) {
        ValidatorUtil.validateEntity(sysDepartModiVo);//校验
        try {
            SysDepartModiDto sysDepartModiDto = new SysDepartModiDto();
            BeanUtils.copyProperties(sysDepartModiVo, sysDepartModiDto);
            sysDepartModiDto.setId(Integer.valueOf(sysDepartModiVo.getId()));
            sysDepartModiDto.setModiUserNo(RequestUtils.getCurrentUserNo(request));
            return sysDepartService.update(sysDepartModiDto);
        } catch (Exception e) {
            logger.error("修改部门异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "删除部门", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(name = "id", value = "部门编号", required = true, dataType = "string", paramType = "path")
    public Result<String> delete(@PathVariable("id") String id) {
        try {
            return sysDepartService.delete(id);
        } catch (Exception e) {
            logger.error("删除部门异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/findCondLike/{condStr}", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    @ApiOperation(value = "根据条件模糊匹配部门列表", notes = "根据查询条件模糊匹配部门列表")
    @ApiImplicitParam(name = "condStr", value = "查询条件", required = true, dataType = "string", paramType = "path")
    public Result<List<SysDepartResultDto>> findCondLike(@PathVariable("condStr") String condStr) {
        return sysDepartService.findCondLike(condStr);
    }

    @RequestMapping(value = "/{compCode}", method = RequestMethod.GET)
    @ApiOperation(value = "获取部门详细信息", notes = "根据url的compCode来获取部门详细信息")
    @ApiImplicitParam(name = "compCode", value = "部门编号", required = true, dataType = "string", paramType = "path")
    public Result<SysDepartResultDto> getEntityByNo(@PathVariable("compCode") String compCode) {
        return sysDepartService.findEntityByNo(compCode);
    }

    @RequestMapping(value = "listPage", method = RequestMethod.POST)
    @ApiOperation(value = "部门分页查询", notes = "部门分页查询")
    @ApiParam(name = "sysDepartCondVo", value = "部门分页查询", required = true)
    public Result listPage(@RequestBody SysDepartCondVo sysDepartCondVo) {
        ValidatorUtil.validateEntity(sysDepartCondVo);//校验
        try {
            SysDepartCondDto sysDepartCondDto = new SysDepartCondDto();
            BeanUtils.copyProperties(sysDepartCondVo, sysDepartCondDto);
            Page<SysDepartResultDto> sysDepartResultDtos = sysDepartService.listPage(sysDepartCondDto);
            PageInfo pageInfo = new PageInfo(sysDepartResultDtos);
            return Result.newSuccess(pageInfo);
        } catch (Exception e) {
            logger.error("部门分页查询异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }


    @RequestMapping(value = "listAll", method = RequestMethod.POST)
    @ApiOperation(value = "获取所有部门信息", notes = "获取所有部门信息")
    public Result<List<SysDepartResultDto>> listAll() {
        return sysDepartService.listAll();
    }


}
