package com.qigao.erp.web.controller.admin.sys;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.qigao.erp.commons.SysCompanyService;
import com.qigao.erp.commons.dto.SysCompanyCreateDto;
import com.qigao.erp.commons.dto.SysCompanyModiDto;
import com.qigao.erp.commons.dto.SysCompanyResultDto;
import com.qigao.erp.commons.dto.SysCompnayCondDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.web.utils.RequestUtils;
import com.qigao.erp.web.utils.ValidatorUtil;
import com.qigao.erp.web.vo.admin.sys.SysCompanyCreateVo;
import com.qigao.erp.web.vo.admin.sys.SysCompanyModiVo;
import com.qigao.erp.web.vo.admin.sys.SysCompnayCondVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author:luiz
 * @Date: 2018/3/12 14:43
 * @Descripton:
 * @Modify :
 **/
@RestController
@RequestMapping("/admin/sysCompany")
@Api(tags = "Admin公司信息", description = "公司信息相关api")
public class SysCompanyController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    SysCompanyService sysCompanyService;


    /**
     * restful api 增删改查
     */
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ApiOperation(value = "创建公司", notes = "根据公司对象创建公司")
    @ApiParam(name = "sysCompanyCreateVo", value = "公司信息实体 sysCompanyCreateVo", required = true)
    public Result<String> create(@RequestBody SysCompanyCreateVo sysCompanyCreateVo, HttpServletRequest request) {
        ValidatorUtil.validateEntity(sysCompanyCreateVo);//校验
        try {
            SysCompanyCreateDto sysCompanyCreateDto = new SysCompanyCreateDto();
            BeanUtils.copyProperties(sysCompanyCreateVo, sysCompanyCreateDto);
            sysCompanyCreateDto.setCreateUserNo(RequestUtils.getCurrentUserNo(request));
            return sysCompanyService.create(sysCompanyCreateDto);
        } catch (Exception e) {
            logger.error("添加公司异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @ApiOperation(value = "更新公司详细信息", notes = "根据url的公司编号来指定更新对象，并根据传过来的公司信息来更新公司详细信息")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public Result<String> update(@RequestBody SysCompanyModiVo sysCompanyModiVo, HttpServletRequest request) {
        ValidatorUtil.validateEntity(sysCompanyModiVo);//校验
        try {
            SysCompanyModiDto sysCompanyModiDto = new SysCompanyModiDto();
            BeanUtils.copyProperties(sysCompanyModiVo, sysCompanyModiDto);
            if(StringUtils.isEmpty(sysCompanyModiVo.getParentCompNo())){
                sysCompanyModiDto.setParentCompNo(null);
            }
            sysCompanyModiDto.setId(Integer.valueOf(sysCompanyModiVo.getId()));
            sysCompanyModiDto.setModiUserNo(RequestUtils.getCurrentUserNo(request));
            return sysCompanyService.update(sysCompanyModiDto);
        } catch (Exception e) {
            logger.error("修改公司异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "删除公司", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(name = "id", value = "公司编号", required = true, dataType = "string", paramType = "path")
    public Result<String> delete(@PathVariable("id") String id) {
        try {
            return sysCompanyService.delete(id);
        } catch (Exception e) {
            logger.error("删除公司异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/findCondLike/{condStr}", method = RequestMethod.GET)
    @ApiOperation(value = "根据条件模糊匹配公司列表", notes = "根据查询条件模糊匹配公司列表")
    @ApiImplicitParam(name = "condStr", value = "查询条件", required = true, dataType = "string", paramType = "path")
    public Result findCondLike(@PathVariable("condStr") String condStr) {
        return sysCompanyService.findCondLike(condStr);
    }

    @RequestMapping(value = "/{compCode}", method = RequestMethod.GET)
    @ApiOperation(value = "获取公司详细信息", notes = "根据url的compCode来获取公司详细信息")
    @ApiImplicitParam(name = "compCode", value = "公司编号", required = true, dataType = "string", paramType = "path")
    public Result<SysCompanyResultDto> getEntityByNo(@PathVariable("compCode") String compCode) {
        return sysCompanyService.findEntityByNo(compCode);
    }

    @RequestMapping(value = "listPage", method = RequestMethod.POST)
    @ApiOperation(value = "公司分页查询", notes = "公司分页查询")
    @ApiParam(name = "sysCompnayCondVo", value = "公司分页查询", required = true)
    public Result listPage(@RequestBody SysCompnayCondVo sysCompnayCondVo) {
        ValidatorUtil.validateEntity(sysCompnayCondVo);//校验
        try {
            SysCompnayCondDto sysCompanyCondDto = new SysCompnayCondDto();
            BeanUtils.copyProperties(sysCompnayCondVo, sysCompanyCondDto);
            Page<SysCompanyResultDto> sysCompanyResultDtos = sysCompanyService.listPage(sysCompanyCondDto);
            PageInfo pageInfo = new PageInfo(sysCompanyResultDtos);
            return Result.newSuccess(pageInfo);
        } catch (Exception e) {
            logger.error("公司分页查询异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "listAll", method = RequestMethod.POST)
    @ApiOperation(value = "获取所有公司信息", notes = "获取所有公司信息")
    public Result<List<SysCompanyResultDto>> listAll() {
        return sysCompanyService.listAll();
    }

    @RequestMapping(value = "listTree", method = RequestMethod.POST)
    @ApiOperation(value = "公司信息树状展示", notes = "公司信息树状展示")
    public Result<List<SysCompanyResultDto>> listTree() {
        return sysCompanyService.listTree();
    }


}
