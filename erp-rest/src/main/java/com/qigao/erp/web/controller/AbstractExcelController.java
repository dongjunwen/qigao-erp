package com.qigao.erp.web.controller;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;

@Component
public class AbstractExcelController {
    public void sendHttpResponse(HttpServletResponse response, String fileName, Workbook workbook) {
        try {
            if(workbook==null) {
                response.sendError(HttpServletResponse.SC_NO_CONTENT);
                return;
            }
            fileName += System.currentTimeMillis() + ".xlsx";
            fileName = URLEncoder.encode(fileName,"utf-8");
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM.toString());
            response.setHeader("Content-Disposition", "attachment;fileName=" + fileName);
           // response.addHeader("Pargam", "no-cache");
          //  response.addHeader("Cache-Control", "no-cache");
            OutputStream os = response.getOutputStream();
            workbook.write(os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
