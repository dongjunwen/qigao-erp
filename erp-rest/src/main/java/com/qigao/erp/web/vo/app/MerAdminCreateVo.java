package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MerAdminCreateVo {
    @ApiModelProperty("商户号")
    @NotNull(message = "商户号不能为空!")
    private String merNo;
    @ApiModelProperty("商户名称")
    @NotNull(message = "商户名称不能为空!")
    private String merName;
    @ApiModelProperty("用户号")
    @NotNull(message = "用户号不能为空!")
    private String userNo;
    @ApiModelProperty("用户名称")
    @NotNull(message = "用户名称不能为空!")
    private String userName;
    @ApiModelProperty("是否有效 0:禁用 1:启用")
    private Integer validStatus;

}
