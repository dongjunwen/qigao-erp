package com.qigao.erp.web.controller.admin.sys;


import com.qigao.erp.commons.UserService;
import com.qigao.erp.commons.dto.UserCreateDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.commons.exceptions.BusinessException;
import com.qigao.erp.commons.utils.DateUtil;
import com.qigao.erp.service.core.LoginCoreService;
import com.qigao.erp.web.controller.AbstractUserController;
import com.qigao.erp.web.utils.RequestUtils;
import com.qigao.erp.web.utils.ValidatorUtil;
import com.qigao.erp.web.vo.api.UserCreateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/admin/user")
@Api(tags = "Admin用户信息api")
public class SysUserController extends AbstractUserController {
    private static final Logger logger = LoggerFactory.getLogger(SysUserController.class);
    @Resource
    private UserService userService;
    @Resource
    private LoginCoreService loginCoreService;

    /**
     * 创建用户
     * @param userCreateVo
     * @param request
     * @return
     */
    @ResponseBody
    @PostMapping("/create")
    public Result create(@RequestBody UserCreateVo userCreateVo, HttpServletRequest request) {
        //校验用户实体字段，
        ValidatorUtil.validateEntity(userCreateVo);
        try {
            UserCreateDto userCreateDto = new UserCreateDto();
            BeanUtils.copyProperties(userCreateVo, userCreateDto);
            //初始密码
            userCreateDto.setLoginPass("123456");
            userCreateDto.setNickName(userCreateVo.getUserName());
            //默认等于手机号
            userCreateDto.setUserNo(userCreateVo.getPhoneNum());
            userCreateDto.setTel(userCreateVo.getPhoneNum());
            if(!StringUtils.isEmpty(userCreateVo.getEntryDate())){
                userCreateDto.setEntryDate(DateUtil.formatDate(userCreateVo.getEntryDate(),"yyyy-MM-dd"));
            }
            if(!StringUtils.isEmpty(userCreateVo.getFormalDate())){
                userCreateDto.setFormalDate(DateUtil.formatDate(userCreateVo.getFormalDate(),"yyyy-MM-dd"));
            }
            userCreateDto.setOperNo(RequestUtils.getCurrentUserNo(request));
            Result registerResult = userService.createUser(userCreateDto);
            return registerResult;
        } catch (BusinessException be) {
            logger.warn("添加用户业务异常!{}", be);
            return Result.newError(be.getCode(), be.getMsg());
        } catch (Exception e) {
            logger.error("添加用户异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    /**
     * 密码重置
     * @param userNo
     * @return
     */
    @RequestMapping(value = "/reset/{userNo:.+}", method = RequestMethod.POST)
    @ApiOperation(value = "重置密码", notes = "重置密码")
    @ApiImplicitParam(name = "userNo", value = "用户编号", required = true, dataType = "string", paramType = "path")
    // @RequiresPermissions("SYS-USER-RESETPASS")
    public Result<String> resetPass(@PathVariable("userNo") String userNo) {
        try {
            return loginCoreService.resetPass(userNo);
        } catch (Exception e) {
            logger.error("重置密码异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }


    /**
     * 启用禁用
     * @param userNo
     * @return
     */
    @RequestMapping(value = "/openClose/{userNo:.+}", method = RequestMethod.POST)
    @ApiOperation(value = "启用禁用", notes = "启用禁用")
    @ApiImplicitParam(name = "userNo", value = "用户编号", required = true, dataType = "string", paramType = "path")
    // @RequiresPermissions("SYS-USER-RESETPASS")
    public Result<String> openClose(@PathVariable("userNo") String userNo) {
        try {
            return loginCoreService.openClose(userNo);
        } catch (Exception e) {
            logger.error("启用禁用异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }






}
