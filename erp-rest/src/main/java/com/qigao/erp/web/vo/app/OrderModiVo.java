package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 13:21
 * @Description:
 */
@Data
@ApiModel("订单修改实体")
public class OrderModiVo {
    @NotNull(message = "店铺编号不能为空!")
    @ApiModelProperty("店铺编号")
    private String shopNo;
    @ApiModelProperty("订单号 ")
    @NotNull(message = "订单号不能为空!")
    private String orderNo;
    @ApiModelProperty("订单实际金额")
    @DecimalMin(value = "0.00",message = "订单实际金额最小数字为0.00")
    private String actOrderAmt;
    //买家备注
    @ApiModelProperty("卖家备注")
    private String merMemo;

}
