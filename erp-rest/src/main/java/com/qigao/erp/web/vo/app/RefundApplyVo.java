package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/20 17:59
 * @Description:
 */
@Data
@ApiModel("退款申请")
public class RefundApplyVo {
    @ApiModelProperty("订单号")
    @NotNull(message = "订单号不能为空")
    private String orderNo;
    @ApiModelProperty("服务类型")
   // @NotNull(message = "服务类型不能为空")
    private String serviceType;
    @ApiModelProperty("服务类型描述")
   // @NotNull(message = "服务类型描述不能为空")
    private String serviceTypeDesc;
    @ApiModelProperty("货物状态")
   // @NotNull(message = "货物状态不能为空")
    private String goodsStatus;
    @ApiModelProperty("货物状态描述")
   // @NotNull(message = "货物状态描述不能为空")
    private String goodsStatusDesc;
    @ApiModelProperty("售后原因")
    private String sellReason;
    @ApiModelProperty("售后原因描述")
    private String sellReasonDesc;
    @ApiModelProperty("退款金额")
    @DecimalMin(value = "0.01",message = "最小金额为0.01")
    private String refundAmt;
    @ApiModelProperty("退款原因")
    private String refundReason;
    @ApiModelProperty("退货图片")
    private List<String> picList;
}
