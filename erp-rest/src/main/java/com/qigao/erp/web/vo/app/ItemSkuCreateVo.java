package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/4 15:15
 * @Description:
 */
@Data
public class ItemSkuCreateVo {
    @ApiModelProperty("商品标题")
    private String stockTitle;
    /**
     * 商品价格
     */
    @NotNull(message = "商品单价不能为空!")
    @Length(min = 1,max = 16)
    @DecimalMin(value = "0.01",message = "商品单价最小为0.01")
    @DecimalMax(value = "999999999.99",message = "单价设置过大")
    @ApiModelProperty("商品单价")
    private String price;
    @NotNull(message = "成本价格不能为空!")
    @Length(min = 1,max = 16)
    @DecimalMin(value = "0.01",message = "成本价格最小为0.01")
    @DecimalMax(value = "999999999.99",message = "成本价格设置过大")
    @ApiModelProperty("成本价格")
    private String costPrice;
    /**
     * 库存数量
     */
    @ApiModelProperty("库存数量")
    private String stockNum;
    @ApiModelProperty("产品图片编号")
    private String picNo;
    @ApiModelProperty("产品缩略图片编号")
    private String narrowPicNo;
    @ApiModelProperty("分润模式 数据字典:PROFIT_TYPE ")
    private String profitType;
    @Length(min = 1,max = 16)
    @DecimalMin(value = "0.00",message = "积分比例最小为0.00")
    @DecimalMax(value = "100.00",message = "积分比例设置过大")
    @ApiModelProperty("积分比例")
    private String scorePercent="0.00";
}
