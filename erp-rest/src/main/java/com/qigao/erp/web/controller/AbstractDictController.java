package com.qigao.erp.web.controller;

import com.qigao.erp.commons.SysDictService;
import com.qigao.erp.commons.dto.SysDictResultDto;
import com.qigao.erp.commons.enums.DictTypeEnum;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.commons.po.UserInfo;
import com.qigao.erp.web.utils.RequestUtils;
import com.qigao.erp.web.vo.app.SelfSettingVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author:luiz
 * @Date: 2018/2/1 14:49
 * @Descripton:基础数据
 * @Modify :
 **/
@RestController
@RequestMapping("/common/dict/")
@Api(tags = "Common数据字典")
public class AbstractDictController {
    @Resource
    SysDictService sysDictService;
    private static final Logger logger= LoggerFactory.getLogger(AbstractDictController.class);

    @GetMapping(value = "/findByPdictCode/{pDictCode}")
    @ApiOperation(value = "按照上级字典编码查询下级字典", notes = "按照上级字典编码查询下级字典")
    @ApiParam(name = "pDictCode", value = "按照上级字典编码查询下级字典 pDictCode", required = true)
    public Result<List<SysDictResultDto>> findByPDict(@PathVariable("pDictCode") String pDictCode) {
        try {
            return sysDictService.findByPDictEnable(pDictCode);
        } catch (Exception e) {
            logger.error("[数据字典]按照上级字典编码查询下级字典异常:{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    //1.保存订单区间、运费；工作时间
    @GetMapping("/findSelfSetting")
    @ApiOperation("查询自定义设置 比如订单区间、运费、工作时间等")
    public Result findSelfSetting(){
        try{
            Result<List<SysDictResultDto>> dictResult=sysDictService.findByPDict(DictTypeEnum.SELF_SETTING.getCode());
            if(dictResult.isSuccess()){
                List<SysDictResultDto> sysDictResultDtos=dictResult.getData();
                SelfSettingVo selfSettingVo=new SelfSettingVo();
                for(SysDictResultDto sysDictResultDto:sysDictResultDtos){
                    if(sysDictResultDto.getDictNo().equals(DictTypeEnum.WORK_START_TIME.getCode())){
                        selfSettingVo.setStartWorkTime(sysDictResultDto.getDictVal());
                    } else if(sysDictResultDto.getDictNo().equals(DictTypeEnum.WORK_END_TIME.getCode())){
                        selfSettingVo.setEndWorkTime(sysDictResultDto.getDictVal());
                    } else if(sysDictResultDto.getDictNo().equals(DictTypeEnum.ORDER_OVER_VAL.getCode())){
                        selfSettingVo.setMinOrderAmt(sysDictResultDto.getDictVal());
                    } else if(sysDictResultDto.getDictNo().equals(DictTypeEnum.SHIPPING_AMT.getCode())){
                        selfSettingVo.setShippingAmt(sysDictResultDto.getDictVal());
                    }
                }
                return Result.newSuccess(selfSettingVo);
            }
            return dictResult;
        }catch (Exception e){
            logger.error("商户自定义设置失败:{}",e);
            return Result.newError(ResultCode.FAIL);
        }
    }


    //根据金额计算运费
    @PostMapping("/calShippingAmt/{orderAmt}")
    @ApiOperation(value = "根据金额计算运费", notes = "根据金额计算运费")
    public Result<String> createOrder(@PathVariable("orderAmt")String orderAmt, HttpServletRequest request){
        if(!StringUtils.isNumeric(orderAmt)){
            logger.warn("传入金额非数字格式:{}",orderAmt);
            return Result.newSuccess("0");
        }
        String miniOrderAmt = sysDictService.findByUniq(DictTypeEnum.SELF_SETTING.getCode(),DictTypeEnum.ORDER_OVER_VAL.getCode());
        String shippingAmt = sysDictService.findByUniq(DictTypeEnum.SELF_SETTING.getCode(),DictTypeEnum.SHIPPING_AMT.getCode());
        BigDecimal miniOrderAmount=new BigDecimal(miniOrderAmt==null?"0":miniOrderAmt);
        if(new BigDecimal(orderAmt).compareTo(miniOrderAmount)<0){
            return Result.newSuccess(shippingAmt);
        }
        return Result.newSuccess("0");
    }


    //按照当前用户获取购买方式列表
    @GetMapping("findBuyWayList")
    @ApiOperation(value = "按照当前用户获取购买方式列表", notes = "按照当前用户获取购买方式列表")
    public Result findBuyWayList(HttpServletRequest request){
        try{
            UserInfo userInfo= RequestUtils.getCurrentUser(request);
            if(userInfo==null)return Result.newError(ResultCode.USER_NO_LOGGED_IN);
            Integer userType=userInfo.getUserType();
            Result<List<SysDictResultDto>> dictResult=sysDictService.findByPDictAndDictNo(DictTypeEnum.BUY_TYPE.getCode(),userType.toString());
            return Result.newSuccess(dictResult.getData());
        }catch (Exception e){
            logger.error("按照当前用户获取购买方式列表发生异常:{}",e);
            return Result.newError(ResultCode.FAIL);
        }
    }


}
