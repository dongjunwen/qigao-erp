package com.qigao.erp.web.vo.app;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/12 17:36
 * @Description:
 */
@ApiModel("商品分页查询条件")
@Data
public class ItemQueryPageVo extends PageVo {
    @ApiModelProperty("关键字")
    private String keyWords;
}
