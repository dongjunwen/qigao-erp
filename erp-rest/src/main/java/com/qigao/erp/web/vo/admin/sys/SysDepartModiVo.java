package com.qigao.erp.web.vo.admin.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel("公司修改实体")
public class SysDepartModiVo extends SysDepartCreateVo {
    @NotEmpty(message = "主键ID不能为空")
    @ApiModelProperty(value = "主键ID", required = true)
    private String id;


}
