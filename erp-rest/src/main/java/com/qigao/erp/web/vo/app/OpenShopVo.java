package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 13:21
 * @Description:
 */
@Data
@ApiModel("商户在线开通店铺")
public class OpenShopVo {
   @ApiModelProperty("套餐编号")
   private String  packageNo;
   @ApiModelProperty("店铺名称")
   @NotNull(message = "店铺名称不能为空!")
   private String shopName;
   @ApiModelProperty("联系人")
   @NotNull(message = "联系人不能为空!")
   private String contactName;
   @ApiModelProperty("手机号")
   @NotNull(message = "手机号不能为空!")
   private String contactPhone;
   @ApiModelProperty("邮件地址")
   private String emailAddr;
   @ApiModelProperty("微信号")
   private String weichatNo;
   @ApiModelProperty("微信图片")
   private String weichatPicNo;
   @ApiModelProperty("省份代码")
   private String provinceCode;
   @ApiModelProperty("省份名称")
   private String recvProvince;
   @ApiModelProperty("城市代码")
   private String cityCode;
   @ApiModelProperty("城市名称")
   private String recvCity;
   @ApiModelProperty("区、县代码")
   private String districtCode;
   @ApiModelProperty("区、县名称")
   private String recvDistrict;
   @ApiModelProperty("联系地址")
   private String contactAddr;
}
