package com.qigao.erp.web.vo.admin.sys;

import com.qigao.erp.web.vo.PageVo;
import lombok.Data;

/**
 * @author luiz
 * @Title: SysOperLogCondVo
 * @ProjectName csuser
 * @Description: TODO
 * @date 2019-04-01 15:13
 */
@Data
public class SysOperLogCondVo extends PageVo {
    //操作人
    private String operUserNo;
    //操作开始时间     YYYY-MM-DD
    private String operBeginTime;
    //操作结束时间 YYYY-MM-DD
    private String operEndTime;
}
