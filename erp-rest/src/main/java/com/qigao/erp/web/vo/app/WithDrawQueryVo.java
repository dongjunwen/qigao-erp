package com.qigao.erp.web.vo.app;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/21 14:13
 * @Description:
 */
@Data
@ApiModel("提现查询条件")
public class WithDrawQueryVo extends PageVo {
    @ApiModelProperty("店铺编号")
    @NotNull(message = "店铺号不能为空")
    private String shopNo;
    @ApiModelProperty("提现状态 字典类型:WITHDRAW_STATUS")
    private String withDrawStatus;
    @ApiModelProperty("开始日期")
    private String startDate;
    @ApiModelProperty("结束日期")
    private String endDate;
}
