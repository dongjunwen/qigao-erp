package com.qigao.erp.web.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.config
 * @ProjectName three-mall
 * @date 2019-11-2019/11/8 16:45
 * @Description:
 */
@Configuration
@Data
public class QiGaoProperties {
    @Value("${qigao.merchant.isSingle:true}")
    private boolean isSingle;
    @Value("${qigao.merchantNo:S0001}")
    private String merNo;
    @Value("${qigao.shopNo:S0001}")
    private String shopNo;
    @Value("${qigao.shopName:蚂蚁付自营店}")
    private String shopName;
    @Value("${qigao.sysMerNo:S0001}")
    private String sysMerNo;
    @Value("${qigao.sysShopNo:S0001}")
    private String sysShopNo;
    @Value("${qigao.sysName:S0001}")
    private String sysName;

}
