package com.qigao.erp.web.vo.admin.sys;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

/**
 * @Date:2017/10/23 0023 14:39
 * @Author lu.dong
 * @Description：
 **/
@Getter
@Setter
@ToString
@ApiModel(value = "资源修改实体 SysResourceModiVo")
public class SysResourceModiVo extends SysResourceCreateVo {
    @NotEmpty(message = "主键ID不能为空!")
    private String id;

}
