package com.qigao.erp.web.controller.admin.sys;


import com.qigao.erp.commons.SysUserRoleService;
import com.qigao.erp.commons.dto.SysUserRoleDto;
import com.qigao.erp.commons.dto.SysUserRoleResultDto;
import com.qigao.erp.commons.dto.SysUserRoleSaveDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.web.utils.RequestUtils;
import com.qigao.erp.web.vo.admin.sys.SysUserRoleVo;
import com.qigao.erp.web.vo.admin.sys.SysUserSaveVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author:luiz
 * @Date: 2018/3/12 15:24
 * @Descripton:
 * @Modify :
 **/
@RestController
@RequestMapping("/admin/sysUserRole")
@Api(tags = "Admin用户角色", description = "用户角色相关api")
public class SysUserRoleController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    SysUserRoleService sysUserRoleService;

    /**
     * restful api 增删改查
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "创建用户角色", notes = "根据用户角色对象创建用户角色")
    @ApiParam(name = "sysUserSaveVo", value = "用户角色信息实体 sysUserSaveVo", required = true)
    public Result save(@RequestBody SysUserSaveVo sysUserSaveVo, HttpServletRequest request) {
        try {
            if (sysUserSaveVo == null) {//校验
                return Result.newError(ResultCode.COMMON_DATA_NOT_EXISTS);
            }
            SysUserRoleSaveDto sysUserRoleSaveDto = new SysUserRoleSaveDto();
            sysUserRoleSaveDto.setOperUserNo(RequestUtils.getCurrentUserNo(request));
            sysUserRoleSaveDto.setUserNo(sysUserSaveVo.getUserNo());
            List<SysUserRoleVo> sysUserRoleVos = sysUserSaveVo.getSysUserRoleVoList();
            List<SysUserRoleDto> sysUserRoleDtoList = new ArrayList<>();
            for (SysUserRoleVo sysUserRoleVo : sysUserRoleVos) {
                SysUserRoleDto sysUserRoleDto = new SysUserRoleDto();
                BeanUtils.copyProperties(sysUserRoleVo, sysUserRoleDto);
                sysUserRoleDtoList.add(sysUserRoleDto);
            }
            sysUserRoleSaveDto.setSysUserRoleDtoList(sysUserRoleDtoList);
            return sysUserRoleService.save(sysUserRoleSaveDto);
        } catch (Exception e) {
            logger.error("添加用户角色异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }


    @RequestMapping(value = "/del/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "删除用户角色", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(name = "id", value = "用户角色编号", required = true, dataType = "string", paramType = "path")
    public Result delete(@PathVariable("id") String id) {
        try {
            return sysUserRoleService.delete(id);
        } catch (Exception e) {
            logger.error("删除用户角色异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/batchDel/{ids}", method = RequestMethod.POST)
    @ApiOperation(value = "批量删除用户角色", notes = "根据url的ids来指定删除对象")
    @ApiImplicitParam(name = "ids", value = "用户角色编号", required = true, dataType = "string", paramType = "path")
    public Result deleteByIds(@RequestBody List<String> ids) {
        try {
            return sysUserRoleService.deleteByIds(ids);
        } catch (Exception e) {
            logger.error("批量删除用户角色异常!{}", e);
            return Result.newError(ResultCode.FAIL);
        }
    }

    @RequestMapping(value = "/{userNo:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户角色详细信息", notes = "根据url的userNo来获取用户角色详细信息")
    @ApiImplicitParam(name = "userNo", value = "用户角色编号", required = true, dataType = "string", paramType = "path")
    public Result<List<SysUserRoleResultDto>> getEntityByNo(@PathVariable("userNo") String userNo) {
        return sysUserRoleService.findByUserNo(userNo);
    }



}
