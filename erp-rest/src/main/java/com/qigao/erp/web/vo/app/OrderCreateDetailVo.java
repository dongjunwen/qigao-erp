package com.qigao.erp.web.vo.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 14:06
 * @Description:
 */
@Data
@ApiModel("订单明细创建实体")
public class OrderCreateDetailVo {
    @NotNull(message = "商品编号不能为空!")
    @Length(min = 1,max = 32)
    @ApiModelProperty("商品编号")
    private String itemNo;
    @Length(min = 1,max = 32)
    @ApiModelProperty("库存编号")
    private String stockNo;
    @ApiModelProperty("购买数量")
    @DecimalMin(value = "0.01",message = "购买数量最小数字为0.01")
    private String buyNum;
    @ApiModelProperty("优惠金额")
    @DecimalMin(value = "0.00",message = "优惠金额最小数字为0.00")
    private String coupAmt="0.00";
    @ApiModelProperty("抵扣积分")
    @DecimalMin(value = "0.00",message = "抵扣积分最小数字为0.00")
    private String scoreAmt="0.00";
}
