package com.qigao.erp.web.vo.app;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2020-04-2020/4/13 10:15
 * @Description:
 */
@Data
@ApiModel("按照月份查询销售报表")
public class MonthReportVo extends PageVo {
    @ApiModelProperty("年月 例如:2020-04")
    private String monthNum;
    @ApiModelProperty("店铺号")
    private String shopNo;
}
