package com.qigao.erp.web.vo.admin.sys;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.admin.sys
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-16 14:11
 * @Description:
 */
@ApiModel("部门查询条件")
public class SysDepartCondVo extends PageVo {
    @ApiModelProperty("部门名称")
    private String departName;
}
