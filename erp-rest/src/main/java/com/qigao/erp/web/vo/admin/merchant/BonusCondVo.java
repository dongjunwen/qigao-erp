package com.qigao.erp.web.vo.admin.merchant;

import com.qigao.erp.web.vo.PageVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: OrderQueryVo
 * Author:   luiz
 * Date:     2019/11/18 21:40
 * Description: 订单查询vo
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/18 21:40       版本号              描述
 */
@Data
@ApiModel("结算订单查询条件")
public class BonusCondVo extends PageVo {
    @ApiModelProperty("开始日期")
    private String dateBegin;
    @ApiModelProperty("结束日期")
    private String dateEnd;
    @ApiModelProperty("姓名")
    private String userName;
}
