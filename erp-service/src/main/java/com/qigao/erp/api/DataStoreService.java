package com.qigao.erp.api;


/**
 * @author luiz
 * @Title: DataStoreService
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 18:01
 */
public interface DataStoreService {
    /**
     * 存储key val
     *
     * @param key
     * @param objVal
     */
    public void put(String key, String objVal,String merNo);

    public void put(String key, String objVal, int defaultSeconds,String merNo);

    String getVal(String randomStr);

    boolean checkOverTime(String randomStr);

    void del(String tokenId);

    void delOverTime();

    String findOneByMerNo(String sysMerNo);
}
