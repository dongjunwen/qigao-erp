package com.qigao.erp.commons;

import com.github.pagehelper.Page;
import com.qigao.erp.commons.dto.UserCondDto;
import com.qigao.erp.commons.dto.UserCreateDto;
import com.qigao.erp.commons.dto.UserResultDto;
import com.qigao.erp.commons.dto.UserUpdateDto;
import com.qigao.erp.commons.enums.Result;

import java.util.List;

public interface UserService {
    Page<UserResultDto> findUserPage(UserCondDto userCondDto);

    Result<UserResultDto> findByUniqIndex(String uniqIndexNo);

    Result update(UserUpdateDto userUpdateDto);

    Result createUser(UserCreateDto userCreateDto);

    Result updateUser(UserUpdateDto userUpdateDto);

    Result<String> deleteById(Integer valueOf);

    List<UserResultDto> findAll();
}
