package com.qigao.erp.api;

import com.qigao.erp.commons.dto.*;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.po.UserPermitResult;

import java.util.List;

/**
 * <p>
 * 用户登录表 服务类
 * </p>
 *
 * @author luiz
 * @since 2019-04-03
 */
public interface LoginInfoService {

    Result findByLoginNo(String loginNo);

    Result doLogin(String loginNo, String loginPass);

    Result doModiPass(UserModiPassDto userModiPassDto);

    Result doRegister(UserRegisterDto userRegisterDto);

    Result forgetPass(String emailAddr);

    Result modiForgetPass(UserModiPassDto userModiPassDto);

    Result activeUser(String activeCode);

    List<UserPermitResult> getCurrentUserPermit(String userNo);
}
