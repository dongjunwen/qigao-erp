package com.qigao.erp.commons;

import com.github.pagehelper.Page;
import com.qigao.erp.commons.dto.SysResourceCondDto;
import com.qigao.erp.commons.dto.SysResourceCreateDto;
import com.qigao.erp.commons.dto.SysResourceModiDto;
import com.qigao.erp.commons.dto.SysResourceResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.jdbc.model.SysResource;

import java.util.List;

/**
 * @author luiz
 * @Title: SysResourceService
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-08 17:44
 */
public interface SysResourceService {
    Result<String> create(SysResourceCreateDto sysResourceCreateDto);

    Result<String> update(SysResourceModiDto sysResourceModiDto);

    Result<String> delete(String id);

    Result<SysResource> findById(String id);

    Result<List<SysResourceResultDto>> getListByCurrentUser(String currentUserNo);

    Result<List<SysResourceResultDto>> findReSourceLike(String condStr);

    Result<List<SysResourceResultDto>> listAll();

    Page<SysResource> listPage(SysResourceCondDto sysResourceCondDto);

    Result<List<SysResourceResultDto>> listTree();
}
