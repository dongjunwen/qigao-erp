package com.qigao.erp.commons;

import com.github.pagehelper.Page;
import com.qigao.erp.commons.dto.SysDepartCondDto;
import com.qigao.erp.commons.dto.SysDepartCreateDto;
import com.qigao.erp.commons.dto.SysDepartModiDto;
import com.qigao.erp.commons.dto.SysDepartResultDto;
import com.qigao.erp.commons.enums.Result;

import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-16 13:55
 * @Description:
 */
public interface SysDepartService {
    Result<String> create(SysDepartCreateDto sysDepartCreateDto);

    Result<String> update(SysDepartModiDto sysDepartModiDto);

    Result<String> delete(String id);

    Result<List<SysDepartResultDto>> findCondLike(String condStr);

    Result<SysDepartResultDto> findEntityByNo(String compCode);

    Page<SysDepartResultDto> listPage(SysDepartCondDto sysDepartCondDto);

    Result<List<SysDepartResultDto>> listAll();
}
