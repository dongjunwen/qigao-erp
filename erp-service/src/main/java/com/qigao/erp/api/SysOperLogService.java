package com.qigao.erp.commons;

import com.github.pagehelper.Page;
import com.qigao.erp.commons.dto.SysOperLogCondDto;
import com.qigao.erp.jdbc.model.SysOperLog;

/**
 * @author luiz
 * @Title: SysOperLogService
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-08 17:50
 */
public interface SysOperLogService {
    Page<SysOperLog> listPage(SysOperLogCondDto sysOperLogCondDto);
}
