package com.qigao.erp.commons;

import com.qigao.erp.commons.dto.SysResourcePermitDto;
import com.qigao.erp.commons.dto.SysResourcePermitResultDto;
import com.qigao.erp.commons.dto.SysResourcePermitUpdateDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.po.UserPermitResult;

import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-16 14:16
 * @Description:
 */
public interface SysResourcePermitService {
    Result<String> create(SysResourcePermitDto sysResourcePermitDto);

    Result<String> save(SysResourcePermitUpdateDto sysResourcePermitUpdateDto);

    Result<String> delete(String id);

    Result<String> deleteByIds(List<String> ids);

    Result<List<SysResourcePermitResultDto>> findBySourceCode(String sourceNo);

    List<UserPermitResult> selectLevelOne(String userNo);

    List<UserPermitResult> selectChild(String userNo, String resourceNo);

    Result<List<SysResourcePermitResultDto>> listTreeBySourceCode(String sourceCode);

    Result<List<SysResourcePermitResultDto>> findByIndex(String sourceCode, String sourceType);
}
