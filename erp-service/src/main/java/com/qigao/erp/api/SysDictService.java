package com.qigao.erp.commons;

import com.github.pagehelper.Page;
import com.qigao.erp.commons.dto.SysDictCondDto;
import com.qigao.erp.commons.dto.SysDictDto;
import com.qigao.erp.commons.dto.SysDictResultDto;
import com.qigao.erp.commons.enums.Result;

import java.util.List;

/**
 * @author luiz
 * @Title: SysDictService
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-17 12:59
 */
public interface SysDictService {
    Result create(SysDictDto sysDictDto);

    Result update(SysDictDto sysDictDto);

    Result<List<SysDictResultDto>> findByPDictEnable(String pDictCode);

    Result<List<SysDictResultDto>> findByPDict(String pDictCode);

    Result<SysDictResultDto> findById(String id);

    Page<SysDictResultDto> findListPage(SysDictCondDto sysDictCondDto);

    Result<List<SysDictResultDto>> listPDictAll();

    void save(SysDictDto sysDictDto);

    String findByUniq(String pdictCode, String dictCode);

    Result<List<SysDictResultDto>> findByPDictAndDictNo(String pdictNo, String dictNo);
}

