package com.qigao.erp.commons;

import com.alibaba.fastjson.JSONObject;
import com.qigao.erp.commons.dto.AreaResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.region.Area;
import com.qigao.erp.commons.region.City;
import com.qigao.erp.commons.region.Province;

import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons
 * @ProjectName three-mall
 * @date 2019-11-2019/11/15 10:02
 * @Description:
 */
public interface RegionInfoService {
    void saveProvince(Province province);

    void saveCity(City city);

    void saveArea(Area area);

    Result<List<AreaResultDto>> findListByLevel(String ym,int areaLevel);

    Result findChildListByAreaCode(String publishYm, String areaCode);

    JSONObject genVantAreaData(String publishYm);
}
