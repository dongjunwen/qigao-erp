package com.qigao.erp.commons;

import com.qigao.erp.commons.dto.FileResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.jdbc.model.TbFileInfo;
import org.springframework.web.multipart.MultipartFile;

public interface FileInfoService {

    TbFileInfo findByFileNo(String fileNo);

    Result<FileResultDto> saveFile(MultipartFile multipartFile);
}
