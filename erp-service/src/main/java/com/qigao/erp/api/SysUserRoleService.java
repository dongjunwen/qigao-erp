package com.qigao.erp.commons;


import com.qigao.erp.commons.dto.SysUserRoleResultDto;
import com.qigao.erp.commons.dto.SysUserRoleSaveDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.jdbc.model.SysUserRole;

import java.util.List;

/**
 * @author luiz
 * @Title: SysUserRoleService
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-09 11:31
 */
public interface SysUserRoleService {
    Result<List<SysUserRoleResultDto>> findByUserNo(String userNo);

    Result deleteByIds(List<String> ids);

    Result delete(String id);

    Result save(SysUserRoleSaveDto sysUserRoleSaveDto);

    List<SysUserRole> findAll();
}
