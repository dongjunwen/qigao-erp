package com.qigao.erp.commons;

import com.github.pagehelper.Page;
import com.qigao.erp.commons.dto.SysRoleCondDto;
import com.qigao.erp.commons.dto.SysRoleCreateDto;
import com.qigao.erp.commons.dto.SysRoleModiDto;
import com.qigao.erp.commons.dto.SysRoleResultDto;
import com.qigao.erp.commons.enums.Result;

import java.util.List;

/**
 * @author luiz
 * @Title: SysRoleService
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-09 9:34
 */
public interface SysRoleService {
    Result create(SysRoleCreateDto sysRoleCreateDto);

    Result update(SysRoleModiDto sysRoleModiDto);

    Result delete(String id);

    Result<List<SysRoleResultDto>> findRoleLike(String condStr);

    Result<SysRoleResultDto> getEntityByNo(String roleCode);

    Result<List<SysRoleResultDto>> listAll();

    Page<SysRoleResultDto> listPage(SysRoleCondDto sysRoleCondDto);
}
