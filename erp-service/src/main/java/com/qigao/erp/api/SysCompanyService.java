package com.qigao.erp.commons;

import com.github.pagehelper.Page;
import com.qigao.erp.commons.dto.*;
import com.qigao.erp.commons.enums.Result;

import java.util.List;

public interface SysCompanyService {
    Result<String> create(SysCompanyCreateDto sysCompanyCreateDto);

    Result<String> update(SysCompanyModiDto sysCompanyModiDto);

    Result<String> delete(String id);

    Result<List<SysCompanyResultDto>> findCondLike(String condStr);

    Result<SysCompanyResultDto> findEntityByNo(String compCode);

    Page<SysCompanyResultDto> listPage(SysCompnayCondDto sysCompanyCondDto);

    Result<List<SysCompanyResultDto>> listAll();

    Result<List<SysCompanyResultDto>> listTree();
}
