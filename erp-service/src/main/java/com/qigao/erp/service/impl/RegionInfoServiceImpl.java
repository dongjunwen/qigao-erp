package com.qigao.erp.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.qigao.erp.commons.RegionInfoService;
import com.qigao.erp.commons.dto.AreaResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.region.Area;
import com.qigao.erp.commons.region.City;
import com.qigao.erp.commons.region.Province;
import com.qigao.erp.jdbc.mapper.TbRegionInfoMapper;
import com.qigao.erp.jdbc.model.TbRegionInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.service.impl
 * @ProjectName three-mall
 * @date 2019-11-2019/11/15 12:55
 * @Description:
 */
@Service
public class RegionInfoServiceImpl implements RegionInfoService {
    @Resource
    TbRegionInfoMapper tbRegionInfoMapper;
    @Override
    public void saveProvince(Province province) {
        TbRegionInfo tbRegionInfo=new TbRegionInfo();
        tbRegionInfo.setAreaCode(province.getCode()+"-1");
        tbRegionInfo.setAreaName(province.getName());
        tbRegionInfo.setAreaLevel(1);
        tbRegionInfo.setPublishYm(province.getPublishYm());
        TbRegionInfo oldRegionInfo=tbRegionInfoMapper.selectByIndex(tbRegionInfo.getAreaCode(),tbRegionInfo.getPublishYm(),1);
        if(oldRegionInfo!=null){
            tbRegionInfo.setId(oldRegionInfo.getId());
            tbRegionInfoMapper.updateByPrimaryKeySelective(tbRegionInfo);
        }else{
            tbRegionInfoMapper.insertSelective(tbRegionInfo);
        }

    }

    @Override
    public void saveCity(City city) {
        TbRegionInfo tbRegionInfo=new TbRegionInfo();
        tbRegionInfo.setPareaCode(city.getProvinceCode()+"-1");
        tbRegionInfo.setAreaCode(city.getCode()+"-2");
        tbRegionInfo.setAreaName(city.getName());
        tbRegionInfo.setAreaLevel(2);
        tbRegionInfo.setPublishYm(city.getPublishYm());
        TbRegionInfo oldRegionInfo=tbRegionInfoMapper.selectByIndex(tbRegionInfo.getAreaCode(),tbRegionInfo.getPublishYm(),2);
        if(oldRegionInfo!=null){
            tbRegionInfo.setId(oldRegionInfo.getId());
            tbRegionInfoMapper.updateByPrimaryKeySelective(tbRegionInfo);
        }else{
            tbRegionInfoMapper.insertSelective(tbRegionInfo);
        }
    }

    @Override
    public void saveArea(Area area) {
        TbRegionInfo tbRegionInfo=new TbRegionInfo();
        tbRegionInfo.setPareaCode(area.getCityCode()+"-2");
        tbRegionInfo.setAreaCode(area.getCode()+"-3");
        tbRegionInfo.setAreaName(area.getName());
        tbRegionInfo.setAreaLevel(3);
        tbRegionInfo.setPublishYm(area.getPublishYm());
        TbRegionInfo oldRegionInfo=tbRegionInfoMapper.selectByIndex(tbRegionInfo.getAreaCode(),tbRegionInfo.getPublishYm(),3);
        if(oldRegionInfo!=null){
            tbRegionInfo.setId(oldRegionInfo.getId());
            tbRegionInfoMapper.updateByPrimaryKeySelective(tbRegionInfo);
        }else{
            tbRegionInfoMapper.insertSelective(tbRegionInfo);
        }
    }

    @Override
    public Result<List<AreaResultDto>> findListByLevel(String ym, int areaLevel) {
       List<TbRegionInfo> tbRegionInfos = tbRegionInfoMapper.selectByYmAndLevel(ym,areaLevel);
        List<AreaResultDto> areaResultDtos=new ArrayList<>();
       for(TbRegionInfo tbRegionInfo:tbRegionInfos){
           AreaResultDto areaResultDto=new AreaResultDto();
           areaResultDto.setAreaCode(tbRegionInfo.getAreaCode());
           areaResultDto.setAreaName(tbRegionInfo.getAreaName());
           areaResultDtos.add(areaResultDto);

       }
       return Result.newSuccess(areaResultDtos);
    }

    @Override
    public Result findChildListByAreaCode(String publishYm, String areaCode) {
        List<TbRegionInfo> tbRegionInfos = tbRegionInfoMapper.selectChildByYmAndAreaCode(publishYm,areaCode);
        List<AreaResultDto> areaResultDtos=new ArrayList<>();
        for(TbRegionInfo tbRegionInfo:tbRegionInfos){
            AreaResultDto areaResultDto=new AreaResultDto();
            areaResultDto.setAreaCode(tbRegionInfo.getAreaCode());
            areaResultDto.setAreaName(tbRegionInfo.getAreaName());
            areaResultDtos.add(areaResultDto);

        }
        return Result.newSuccess(areaResultDtos);
    }

    @Override
    public JSONObject genVantAreaData(String publishYm) {
        List<TbRegionInfo> tbRegionInfos=tbRegionInfoMapper.selectByYmAndLevel(publishYm,1);
       JSONObject provinceJson=new JSONObject();
        tbRegionInfos.stream().forEach(item->{
            provinceJson.put(item.getAreaCode().split("-")[0],item.getAreaName());
        });
        List<TbRegionInfo> tbRegionInfos2=tbRegionInfoMapper.selectByYmAndLevel(publishYm,2);
        JSONObject cityJson=new JSONObject();
        tbRegionInfos2.stream().forEach(item->{
            if(item.getPareaCode().split("-")[0].equals(item.getAreaCode().split("-")[0])){
                cityJson.put(item.getAreaCode().substring(0,2).concat("0100"),item.getAreaName());
            }else {
                item.setAreaCode(item.getAreaCode().split("-")[0]);
                cityJson.put(item.getAreaCode().split("-")[0],item.getAreaName());
            }
        });
        List<TbRegionInfo> tbRegionInfos3=tbRegionInfoMapper.selectByYmAndLevel(publishYm,3);
        JSONObject areaJson=new JSONObject();
        tbRegionInfos3.stream().forEach(item->{
            areaJson.put(item.getAreaCode().split("-")[0],item.getAreaName());
        });
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("province_list",provinceJson);
        jsonObject.put("city_list",cityJson);
        jsonObject.put("county_list",areaJson);
        return jsonObject;
    }
}
