package com.qigao.erp.service.impl.admin;

import com.qigao.erp.commons.SysUserResourceService;
import com.qigao.erp.jdbc.mapper.SysUserRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author luiz
 * @Title: SysUserResourceService
 * @ProjectName csuser
 * @Description: TODO
 * @date 2018/11/12 14:11
 */
@Service
public class SysUserResourceServiceImpl implements SysUserResourceService {
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

}
