package com.qigao.erp.service.impl.admin;

import com.qigao.erp.commons.SysResourcePermitService;
import com.qigao.erp.commons.SysResourceService;
import com.qigao.erp.commons.dto.*;
import com.qigao.erp.commons.dto.SysResourcePermitDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.commons.po.UserPermitResult;
import com.qigao.erp.jdbc.mapper.SysResourceMapper;
import com.qigao.erp.jdbc.mapper.SysResourcePermitMapper;
import com.qigao.erp.jdbc.model.SysResource;
import com.qigao.erp.jdbc.model.SysResourcePermit;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author luiz
 * @Title: com.qigao.erp.service.impl.admin
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-16 14:41
 * @Description:
 */
@Service
public class SysResourcePermitServiceImpl implements SysResourcePermitService {

    @Resource
    private SysResourcePermitMapper sysResourcePermitMapper;
    @Resource
    private SysResourceMapper sysResourceMapper;
    @Resource
    private SysResourceService sysResourceService;

    @Override
    public Result<String> create(SysResourcePermitDto sysResourcePermitDto) {
        return null;
    }

    @Override
    public Result<String> save(SysResourcePermitUpdateDto sysResourcePermitUpdateDto) {
        sysResourcePermitMapper.deleteBySourceCode(sysResourcePermitUpdateDto.getSourceCode());
        if(!CollectionUtils.isEmpty(sysResourcePermitUpdateDto.getSysResourcePermitDtos())){
            List<SysResourcePermit> sysResourcePermits = new ArrayList<SysResourcePermit>();
            for (SysResourcePermitDto sysResourcePermitDto : sysResourcePermitUpdateDto.getSysResourcePermitDtos()) {
                SysResourcePermit sysResourcePermit = new SysResourcePermit();
                sysResourcePermit.setSourceCode(sysResourcePermitDto.getSourceCode());
                sysResourcePermit.setSourceType(sysResourcePermitDto.getSourceTypeEnum().getCode());
                sysResourcePermit.setResourceNo(sysResourcePermitDto.getResourceNo());
                sysResourcePermit.setCreateNo(sysResourcePermitDto.getOperUserNo());
                sysResourcePermit.setCreateTime(new Date());
                sysResourcePermits.add(sysResourcePermit);
            }
            sysResourcePermitMapper.insertBatch(sysResourcePermits);
        }

        return Result.newSuccess();
    }

    @Override
    public Result<String> delete(String id) {
        sysResourcePermitMapper.deleteByPrimaryKey(Integer.valueOf(id));
        return Result.newSuccess();
    }

    @Override
    public Result<String> deleteByIds(List<String> ids) {
        List<SysResourcePermit> sysResourcePermitArrayList = new ArrayList<>();
        for (String id : ids) {
            SysResourcePermit sysResourcePermit = new SysResourcePermit();
            sysResourcePermit.setId(Integer.valueOf(id));
            sysResourcePermitArrayList.add(sysResourcePermit);
        }
        sysResourcePermitMapper.deleteInBatch(sysResourcePermitArrayList);
        return Result.newSuccess();
    }

    @Override
    public Result<List<SysResourcePermitResultDto>> findBySourceCode(String sourceNo) {
        List<SysResourcePermit> sysResourcePermits= sysResourcePermitMapper.selectBySourceCode(sourceNo);
        if(CollectionUtils.isEmpty(sysResourcePermits)){
            return Result.newError(ResultCode.COMMON_DATA_NOT_EXISTS);
        }
        return Result.newSuccess(buildResultDto(sysResourcePermits));
    }


    @Override
    public Result<List<SysResourcePermitResultDto>> findByIndex(String sourceCode, String sourceType) {
        List<SysResourcePermit> sysResourcePermits= sysResourcePermitMapper.selectByIndex(sourceCode,sourceType);
        if(CollectionUtils.isEmpty(sysResourcePermits)){
            return Result.newError(ResultCode.COMMON_DATA_NOT_EXISTS);
        }
        return Result.newSuccess(buildResultDto(sysResourcePermits));
    }

    private List<SysResourcePermitResultDto> buildResultDto(List<SysResourcePermit> sysResourcePermits) {
        List<SysResource> sysResources=sysResourceMapper.selectAll();
        List<SysResourcePermitResultDto> sysResourcePermitResultDtos=new ArrayList<>();
        for(SysResourcePermit sysResourcePermit:sysResourcePermits){
            SysResourcePermitResultDto sysResourcePermitResultDto=new SysResourcePermitResultDto();
            BeanUtils.copyProperties(sysResourcePermit,sysResourcePermitResultDto);
            SysResource sysResource=findResourceByNo(sysResourcePermit.getResourceNo(),sysResources);
            if(sysResource!=null){
                sysResourcePermitResultDto.setResourceName(sysResource.getResourceName());
                sysResourcePermitResultDto.setResourceIcon(sysResource.getResourceIcon());
                sysResourcePermitResultDto.setReqUrl(sysResource.getReqUrl());
                sysResourcePermitResultDto.setResourceType(sysResource.getResourceType());
                sysResourcePermitResultDto.setPresourceNo(sysResource.getPresourceNo());
            }
            sysResourcePermitResultDtos.add(sysResourcePermitResultDto);
        }
        return sysResourcePermitResultDtos;
    }

    private SysResource findResourceByNo(String resourceNo, List<SysResource> sysResources) {
        for(SysResource sysResource:sysResources){
            if(sysResource.getResourceNo().equals(resourceNo)){
                return sysResource;
            }
        }
        return null;
    }

    @Override
    public List<UserPermitResult> selectLevelOne(String userNo) {
        return sysResourcePermitMapper.selectByLevelOne(userNo);
    }

    @Override
    public List<UserPermitResult> selectChild(String userNo, String resourceNo) {
        return sysResourcePermitMapper.selectChild(userNo,resourceNo);
    }

    @Override
    public Result<List<SysResourcePermitResultDto>> listTreeBySourceCode(String sourceCode) {
        List<SysResource> sysResourceList = sysResourceMapper.selectAll();
        List<SysResourcePermit> sysResourcePermits= sysResourcePermitMapper.selectBySourceCode(sourceCode);
        List<SysResourcePermitResultDto> sysResourcePermitResultDtos=buldDto(sysResourceList,sysResourcePermits);
        return Result.newSuccess(buildTree(sysResourcePermitResultDtos));
    }




    private List<SysResourcePermitResultDto> buildTree(List<SysResourcePermitResultDto> sysResourcePermitResultDtos) {
        List<SysResourcePermitResultDto> collect = sysResourcePermitResultDtos.stream().filter(m ->  m.getResourceLevel()==1).map(
                (m) -> {
                    m.setChildren(buildChild(m, sysResourcePermitResultDtos));
                    return m;
                }
        ).collect(Collectors.toList());
        return collect;
    }

    private  List<SysResourcePermitResultDto>  buildChild(SysResourcePermitResultDto root,List<SysResourcePermitResultDto> all) {
        List<SysResourcePermitResultDto> children = all.stream().filter(m -> {
            return Objects.equals(m.getPresourceNo(), root.getResourceNo());
        }).map(
                (m) -> {
                    m.setChildren(buildChild(m, all));
                    return m;
                }
        ).collect(Collectors.toList());
        return children;
    }

    private List<SysResourcePermitResultDto> buldDto(List<SysResource> sysResourceList, List<SysResourcePermit> sysResourcePermits) {
        List<SysResourcePermitResultDto> sysResourcePermitResultDtos=new ArrayList<SysResourcePermitResultDto>();
        for(SysResource sysResource:sysResourceList){
            SysResourcePermitResultDto sysResourcePermitResultDto=new SysResourcePermitResultDto();
            BeanUtils.copyProperties(sysResource,sysResourcePermitResultDto);
            boolean checkFlag=checkHasPermit(sysResource,sysResourcePermits);
            sysResourcePermitResultDto.setChecked(checkFlag);
            sysResourcePermitResultDtos.add(sysResourcePermitResultDto);
        }
        return sysResourcePermitResultDtos;
    }

    private boolean checkHasPermit(SysResource sysResource, List<SysResourcePermit> sysResourcePermits) {
        for(SysResourcePermit sysResourcePermit:sysResourcePermits){
            if(sysResourcePermit.getResourceNo().equals(sysResource.getResourceNo())){
                return true;
            }
        }
        return false;
    }

}
