package com.qigao.erp.service.impl.admin;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qigao.erp.commons.SysCompanyService;
import com.qigao.erp.commons.dto.*;
import com.qigao.erp.commons.enums.CompTypeEnum;
import com.qigao.erp.commons.enums.IdTypeEnum;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.commons.utils.DateUtil;
import com.qigao.erp.commons.utils.IDUtils;
import com.qigao.erp.jdbc.mapper.SysCompanyMapper;
import com.qigao.erp.jdbc.model.SysCompany;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class SysCompanyServiceImpl implements SysCompanyService {
    @Resource
    private SysCompanyMapper sysCompanyMapper;
    @Override
    public Result<String> create(SysCompanyCreateDto sysCompanyCreateDto) {
        SysCompany sysCompany=sysCompanyMapper.selectByCompanyName(sysCompanyCreateDto.getCompName());
        if(sysCompany!=null){
            return Result.newError(ResultCode.COMMON_DATA_EXISTS);
        }
        sysCompany=new SysCompany();
        BeanUtils.copyProperties(sysCompanyCreateDto,sysCompany);
        if(StringUtils.isEmpty(sysCompanyCreateDto.getCompNo()))
            sysCompany.setCompNo(IDUtils.genIdStr(IdTypeEnum.COMPNAY));
        sysCompany.setCreateNo(sysCompanyCreateDto.getCreateUserNo());
        sysCompany.setCreateTime(new Date());
        sysCompany.setModiNo(sysCompanyCreateDto.getCreateUserNo());
        sysCompany.setModiTime(new Date());
        sysCompanyMapper.insertSelective(sysCompany);
        return Result.newSuccess();
    }

    @Override
    public Result<String> update(SysCompanyModiDto sysCompanyModiDto) {
        SysCompany sysCompany=new SysCompany();
        BeanUtils.copyProperties(sysCompanyModiDto,sysCompany);
        sysCompany.setModiNo(sysCompanyModiDto.getModiUserNo());
        sysCompany.setModiTime(new Date());
        sysCompanyMapper.updateByPrimaryKeySelective(sysCompany);
        return Result.newSuccess();
    }

    @Override
    public Result<String> delete(String id) {
         sysCompanyMapper.deleteByPrimaryKey(Integer.valueOf(id));
         return Result.newSuccess();
    }

    @Override
    public Result<List<SysCompanyResultDto>> findCondLike(String condStr) {
        List<SysCompany> sysCompanys=sysCompanyMapper.selectByCond(condStr);
        if(CollectionUtils.isEmpty(sysCompanys)){
            return Result.newSuccess();
        }
        List<SysCompanyResultDto> sysCompanyResultDtos=new ArrayList<SysCompanyResultDto>(sysCompanys.size());
        for(SysCompany sysCompany:sysCompanys){
            SysCompanyResultDto sysCompanyResultDto=singleConvertDto(sysCompany);
            sysCompanyResultDtos.add(sysCompanyResultDto);
        }
        return Result.newSuccess(sysCompanyResultDtos);
    }

    private SysCompanyResultDto singleConvertDto(SysCompany sysCompany) {
        SysCompanyResultDto sysCompanyResultDto=new SysCompanyResultDto();
        BeanUtils.copyProperties(sysCompany,sysCompanyResultDto);
       sysCompanyResultDto.setCompTypeName(CompTypeEnum.getNameByCode(sysCompany.getCompType()));
        sysCompanyResultDto.setCreateTime(DateUtil.getDateTimeFormat(sysCompany.getCreateTime()));
        sysCompanyResultDto.setModiTime(DateUtil.getDateTimeFormat(sysCompany.getModiTime()));
        return sysCompanyResultDto;
    }

    @Override
    public Result<SysCompanyResultDto> findEntityByNo(String compCode) {
        SysCompany sysCompany=sysCompanyMapper.selectByCompNo(compCode);
        if(sysCompany==null){
            return Result.newError(ResultCode.COMMON_DATA_NOT_EXISTS);
        }
        SysCompanyResultDto sysCompanyResultDto=singleConvertDto(sysCompany);
        return Result.newSuccess(sysCompanyResultDto);
    }

    @Override
    public Page<SysCompanyResultDto> listPage(SysCompnayCondDto sysCompanyCondDto) {
        //使用PageHelper类进行分页
        PageHelper.startPage(sysCompanyCondDto.getPageNum(), sysCompanyCondDto.getPageSize());
        Page<SysCompany> sysCompanyPage = (Page<SysCompany>) sysCompanyMapper.selectList(sysCompanyCondDto);
        List<SysCompany> sysCompanyList = sysCompanyPage.getResult();
        Page<SysCompanyResultDto> sysCompanyResultDtoPage = new Page<>();
        if(!CollectionUtils.isEmpty(sysCompanyList)){
            BeanUtils.copyProperties(sysCompanyPage, sysCompanyResultDtoPage);
            List<SysCompanyResultDto> sysCompanyResultDtos = buildDto(sysCompanyList);
            sysCompanyResultDtoPage.addAll(sysCompanyResultDtos);
        }
        return sysCompanyResultDtoPage;
    }

    private List<SysCompanyResultDto> buildDto(List<SysCompany> sysCompanys) {
        if(CollectionUtils.isEmpty(sysCompanys)){
            return null;
        }
        List<SysCompanyResultDto> sysCompanyResultDtos=new ArrayList<SysCompanyResultDto>(sysCompanys.size());
        for(SysCompany sysCompany:sysCompanys){
            SysCompanyResultDto sysCompanyResultDto=singleConvertDto(sysCompany);
            sysCompanyResultDtos.add(sysCompanyResultDto);
        }
        return sysCompanyResultDtos;
    }

    @Override
    public Result<List<SysCompanyResultDto>> listAll() {
        List<SysCompany> sysCompanyList = sysCompanyMapper.selectAll();
        return Result.newSuccess(buildDto(sysCompanyList));
    }

    @Override
    public Result<List<SysCompanyResultDto>> listTree() {
        List<SysCompany> sysCompanyList = sysCompanyMapper.selectAll();
        List<SysCompanyResultDto> sysCompanyResultDtos=buildDto(sysCompanyList);
        return Result.newSuccess(buildTree(sysCompanyResultDtos));
    }

    private List<SysCompanyResultDto> buildTree(List<SysCompanyResultDto> sysCompanyResultDtos) {
        List<SysCompanyResultDto> collect = sysCompanyResultDtos.stream().filter(m -> m.getParentCompNo() == null).map(
                (m) -> {
                    m.setChildren(buildChild(m, sysCompanyResultDtos));
                    return m;
                }
        ).collect(Collectors.toList());
        return collect;
    }

    private  List<SysCompanyResultDto>  buildChild(SysCompanyResultDto root,List<SysCompanyResultDto> all) {
        List<SysCompanyResultDto> children = all.stream().filter(m -> {
            return Objects.equals(m.getParentCompNo(), root.getCompNo());
        }).map(
                (m) -> {
                    m.setChildren(buildChild(m, all));
                    return m;
                }
        ).collect(Collectors.toList());
        return children;
    }

}
