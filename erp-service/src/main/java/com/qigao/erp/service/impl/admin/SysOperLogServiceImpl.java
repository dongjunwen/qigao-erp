package com.qigao.erp.service.impl.admin;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qigao.erp.commons.SysOperLogService;
import com.qigao.erp.commons.dto.SysOperLogCondDto;
import com.qigao.erp.jdbc.mapper.SysOperLogMapper;
import com.qigao.erp.jdbc.model.SysOperLog;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author luiz
 * @Title: SysOperLogService
 * @ProjectName csuser
 * @Description: TODO
 * @date 2019-04-01 15:15
 */
@Service
public class SysOperLogServiceImpl implements SysOperLogService {
    @Resource
    private SysOperLogMapper sysOperLogMapper;

    @Override
    public Page<SysOperLog> listPage(SysOperLogCondDto sysOperLogCondDto) {
        //使用PageHelper类进行分页
        PageHelper.startPage(sysOperLogCondDto.getPageNum(), sysOperLogCondDto.getPageSize());
        Page<SysOperLog> sysOperLogPage = (Page<SysOperLog>) sysOperLogMapper.selectList(sysOperLogCondDto);
        return sysOperLogPage;
    }
}
