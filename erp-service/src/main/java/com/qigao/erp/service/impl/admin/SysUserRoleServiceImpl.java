package com.qigao.erp.service.impl.admin;

import com.qigao.erp.commons.SysUserRoleService;
import com.qigao.erp.commons.dto.SysUserRoleDto;
import com.qigao.erp.commons.dto.SysUserRoleResultDto;
import com.qigao.erp.commons.dto.SysUserRoleSaveDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.jdbc.mapper.SysRoleMapper;
import com.qigao.erp.jdbc.mapper.SysUserRoleMapper;
import com.qigao.erp.jdbc.model.SysRole;
import com.qigao.erp.jdbc.model.SysUserRole;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author:luiz
 * @Date: 2018/3/9 10:23
 * @Descripton:
 * @Modify :
 **/
@Service
public class SysUserRoleServiceImpl implements SysUserRoleService {
    @Resource
    private SysUserRoleMapper sysUserRoleMapper;
    @Resource
    private SysRoleMapper sysRoleMapper;

    @Override
    public Result<List<SysUserRoleResultDto>> findByUserNo(String userNo) {
        List<SysUserRole> sysUserRoleList = sysUserRoleMapper.selectByUserNo(userNo);
        List<SysUserRoleResultDto> sysUserRoleResultDtos = convertDto(sysUserRoleList);
        return Result.newSuccess(sysUserRoleResultDtos);
    }

    private List<SysUserRoleResultDto> convertDto(List<SysUserRole> sysUserRoleList) {
        List<SysUserRoleResultDto> sysUserRoleResultDtos = new ArrayList<>();
        List<SysRole> sysRoles = sysRoleMapper.selectAll();
        for(SysRole sysRole:sysRoles){
            SysUserRoleResultDto sysUserRoleResultDto = new SysUserRoleResultDto();
            BeanUtils.copyProperties(sysRole, sysUserRoleResultDto);
            sysUserRoleResultDto.setName(sysRole.getRoleName());
            checkIfExists(sysUserRoleList, sysUserRoleResultDto);
            sysUserRoleResultDtos.add(sysUserRoleResultDto);
        }
        return sysUserRoleResultDtos;
    }

    private boolean checkIfExists(List<SysUserRole> sysUserRoleList,SysUserRoleResultDto sysUserRoleResultDto) {
        sysUserRoleResultDto.setChecked(false);
        for (SysUserRole sysUserRole : sysUserRoleList) {
            if (sysUserRoleResultDto.getRoleCode().equals(sysUserRole.getRoleCode())) {
                sysUserRoleResultDto.setChecked(true);
                sysUserRoleResultDto.setUserNo(sysUserRole.getUserNo());
                return true;
            }
        }
        return false;
    }

    public Result deleteByIds(List<String> ids) {
        List<SysUserRole> sysUserRoles = new ArrayList<>();
        for (String id : ids) {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setId(Integer.valueOf(id));
            sysUserRoles.add(sysUserRole);
        }
        sysUserRoleMapper.deleteInBatch(sysUserRoles);
        return Result.newSuccess();
    }

    public Result delete(String id) {
        sysUserRoleMapper.deleteByPrimaryKey(Integer.valueOf(id));
        return Result.newSuccess();
    }

    @Override
    public Result save(SysUserRoleSaveDto sysUserRoleSaveDto) {
        deleteByUserNo(sysUserRoleSaveDto.getUserNo());
        List<SysUserRoleDto> sysUserRoleDtos = sysUserRoleSaveDto.getSysUserRoleDtoList();
        for (SysUserRoleDto sysUserRoleDto : sysUserRoleDtos) {
            SysUserRole sysUserRole = new SysUserRole();
            BeanUtils.copyProperties(sysUserRoleDto, sysUserRole);
            sysUserRole.setUserNo(sysUserRoleSaveDto.getUserNo());
            sysUserRole.setCreateNo(sysUserRoleSaveDto.getOperUserNo());
            sysUserRole.setCreateTime(new Date());
            sysUserRoleMapper.insertSelective(sysUserRole);
        }
        return Result.newSuccess();
    }

    @Override
    public List<SysUserRole> findAll() {
        return sysUserRoleMapper.selectAll();
    }

    private void deleteByUserNo(String userNo) {
        sysUserRoleMapper.deleteByUserNo(userNo);
    }

}
