package com.qigao.erp.service.impl;

import com.github.pagehelper.PageHelper;
import com.qigao.erp.api.DataStoreService;
import com.qigao.erp.commons.dto.PageDto;
import com.qigao.erp.commons.utils.DateUtil;
import com.qigao.erp.jdbc.mapper.TbCacheInfoMapper;
import com.qigao.erp.jdbc.model.TbCacheInfo;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author luiz
 * @Title: TkCacheInfoServiceImpl
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 18:41
 */
@Component
public class TbCacheInfoServiceImpl implements DataStoreService {
    @Resource
    private TbCacheInfoMapper tbCacheInfoMapper;

    @Override
    public void put(String key, String objVal,String merNo) {
        //默认30分钟
        put(key, objVal, 1800,merNo);
    }

    @Override
    public void put(String key, String objVal, int defaultSeconds,String merNo) {
        TbCacheInfo tbCacheInfo = new TbCacheInfo();
        tbCacheInfo.setCacheKey(key);
        tbCacheInfo.setCacheVal(objVal);
        tbCacheInfo.setStartTime(new Date());
        tbCacheInfo.setMerNo(merNo);
        tbCacheInfo.setVersion(1);
        Date endDate = DateUtil.addSeconds(new Date(), defaultSeconds);
        tbCacheInfo.setEndTime(endDate);
        TbCacheInfo tkCacheInfoOld = tbCacheInfoMapper.selctByIndex(key);
        if(tkCacheInfoOld==null){
            tbCacheInfoMapper.insertSelective(tbCacheInfo);
        }else {
            tbCacheInfo.setId(tkCacheInfoOld.getId());
            tbCacheInfo.setStartTime(null);
            tbCacheInfo.setVersion(tkCacheInfoOld.getVersion());
            tbCacheInfoMapper.updateWithVersion(tbCacheInfo);
        }
    }

    @Override
    public String getVal(String cacheKey) {
        TbCacheInfo tbCacheInfo = tbCacheInfoMapper.selctByIndex(cacheKey);
        if (tbCacheInfo != null) {
            return tbCacheInfo.getCacheVal();
        }
        return null;
    }

    /**
     * 检查是否已经过期 true:已过期 false:未过期
     * @param randomStr
     * @return
     */
    @Override
    public boolean checkOverTime(String randomStr) {
        TbCacheInfo tbCacheInfo = tbCacheInfoMapper.selctByIndex(randomStr);
        if (tbCacheInfo != null) {
            Date endTime=tbCacheInfo.getEndTime();
            Date nowDate=new Date();
            if(endTime.compareTo(nowDate)<0){
                return true;
            }
        }
        return false;
    }



    @Override
    public void del(String cacheKey) {
        tbCacheInfoMapper.deleteByCacheKey(cacheKey);
    }

    @Override
    public void delOverTime() {
        PageDto pageDto=new PageDto();
        pageDto.setPageNum(1);
        pageDto.setPageSize(50);
        PageHelper.startPage(pageDto.getPageNum(), pageDto.getPageSize());
        List<TbCacheInfo> tbCacheInfos = tbCacheInfoMapper.selectPageList(pageDto);
        if(!CollectionUtils.isEmpty(tbCacheInfos)){
            tbCacheInfoMapper.deleteByIds(tbCacheInfos);
        }
    }

    @Override
    public String findOneByMerNo(String merNo) {
        TbCacheInfo tbCacheInfo =tbCacheInfoMapper.selectTopOneByMerNo(merNo);
        if (tbCacheInfo != null) {
            return tbCacheInfo.getCacheVal();
        }
        return null;
    }


}
