package com.qigao.erp.service.impl;

import com.qigao.erp.commons.FileInfoService;
import com.qigao.erp.commons.dto.FileResultDto;
import com.qigao.erp.commons.enums.FileTypeEnum;
import com.qigao.erp.commons.enums.IdTypeEnum;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.commons.utils.FileUtils;
import com.qigao.erp.commons.utils.IDUtils;
import com.qigao.erp.jdbc.mapper.TbFileInfoMapper;
import com.qigao.erp.jdbc.model.TbFileInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class FileInfoServiceImpl implements FileInfoService {
    private static final Logger logger= LoggerFactory.getLogger(FileInfoServiceImpl.class);
    @Value("${qigao.file.savePath:/opt/files/}")
    private String fileSavePath;

    @Value("${qigao.pay.domain}")
    private String payDomain;
    @Resource
    TbFileInfoMapper tbFileInfoMapper;

    @Override
    public TbFileInfo findByFileNo(String fileNo) {
        return  tbFileInfoMapper.findByFileNo(fileNo);
    }

    @Override
    public Result<FileResultDto> saveFile(MultipartFile multipartFile) {
        String fileNo= IDUtils.genIdStr(IdTypeEnum.FILE.getCode());
        TbFileInfo shareFileInfo=new TbFileInfo();
        String originFileName=multipartFile.getOriginalFilename();
        int lastCharIndex=originFileName.lastIndexOf(".");
        String fileType=originFileName.substring(lastCharIndex+1);
        //文件大小
        long fileSize=multipartFile.getSize();
        shareFileInfo.setFileNo(fileNo);
        shareFileInfo.setFileType(fileType);
        shareFileInfo.setFileName(originFileName);
        shareFileInfo.setFileUrl(fileSavePath);
        shareFileInfo.setFileSize(Integer.valueOf(fileSize+""));
        try {
            logger.info("文件名称:{},文件类型:{},文件大小:{}开始存储",originFileName,fileType,fileSize);
            FileUtils.uploadFile(multipartFile.getBytes(),fileSavePath,fileNo+"."+fileType);
            FileResultDto fileResultDto=new FileResultDto();
            fileResultDto.setFileNo(fileNo);
            fileResultDto.setFileUrl(payDomain+"/common/file/findByNo/"+fileNo);
            if(FileTypeEnum.ifImage(fileType)){
                String fromPicPath=fileSavePath+fileNo+"."+fileType;
                String zipPicFileNo=IDUtils.genIdStr(IdTypeEnum.PIC.getCode());
                String zipPicPath=fileSavePath+zipPicFileNo+"."+fileType;
                //图片压缩
                FileUtils.zipImage(fromPicPath,zipPicPath);
                String narrowPicFileNo=IDUtils.genIdStr(IdTypeEnum.PIC.getCode());
                String narrowPicPath=fileSavePath+narrowPicFileNo+"."+fileType;
                shareFileInfo.setNarrowFileNo(narrowPicFileNo);
                fileResultDto.setNarrowFileNo(narrowPicFileNo);
                fileResultDto.setNarrowFileUrl(payDomain+"/common/file/findByNo/"+narrowPicFileNo);
                //图片缩放
                FileUtils.narrowImage(fromPicPath,narrowPicPath);
                shareFileInfo.setZipFileNo(zipPicFileNo);
                fileResultDto.setZipFileNo(zipPicFileNo);
                fileResultDto.setZipFileUrl(payDomain+"/common/file/findByNo/"+zipPicFileNo);
            }
            shareFileInfo.setModiTime(new Date());
            shareFileInfo.setCreateTime(new Date());
            tbFileInfoMapper.insertSelective(shareFileInfo);
            logger.info("文件名称:{},文件类型:{},文件大小:{}存储成功!",originFileName,fileType,fileSize);

            return Result.newSuccess(fileResultDto);
        } catch (Exception e) {
            logger.error("存储文件发生异常:{}",e);
            return Result.newError(ResultCode.COMMON_DATA_EXISTS);
        }
    }


}
