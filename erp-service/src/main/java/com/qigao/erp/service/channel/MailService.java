package com.qigao.erp.service.channel;

/**
 * @author luiz
 * @Title: com.qigao.erp.service.channel
 * @ProjectName three-mall
 * @date 2019-11-2019/11/28 15:02
 * @Description:
 */
public interface MailService {

  void sendSimpleMail(String to, String subject, String content) ;

    /**
     * 发送HTML邮件
     * @param to 收件人
     * @param subject 主题
     * @param content 内容
     */
    public void sendHtmlMail(String to, String subject, String content);



    /**
     * 发送带附件的邮件
     * @param to 收件人
     * @param subject 主题
     * @param content 内容
     * @param filePath 附件
     */
    public void sendAttachmentsMail(String to, String subject, String content, String filePath);

}
