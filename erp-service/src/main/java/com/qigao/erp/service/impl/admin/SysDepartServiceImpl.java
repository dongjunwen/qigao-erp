package com.qigao.erp.service.impl.admin;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qigao.erp.commons.SysDepartService;
import com.qigao.erp.commons.dto.SysDepartCondDto;
import com.qigao.erp.commons.dto.SysDepartCreateDto;
import com.qigao.erp.commons.dto.SysDepartModiDto;
import com.qigao.erp.commons.dto.SysDepartResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.commons.utils.DateUtil;
import com.qigao.erp.jdbc.mapper.SysDepartMapper;
import com.qigao.erp.jdbc.model.SysDepart;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.service.impl.admin
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-16 14:02
 * @Description:
 */
@Service
public class SysDepartServiceImpl implements SysDepartService {
    @Resource
    private SysDepartMapper sysDepartMapper;
    @Override
    public Result<String> create(SysDepartCreateDto sysDepartCreateDto) {
        SysDepart sysDepart=new SysDepart();
        BeanUtils.copyProperties(sysDepartCreateDto,sysDepart);
        sysDepart.setCreateNo(sysDepartCreateDto.getCreateUserNo());
        sysDepart.setCreateTime(new Date());
        sysDepart.setModiNo(sysDepartCreateDto.getCreateUserNo());
        sysDepart.setModiTime(new Date());
        sysDepartMapper.insertSelective(sysDepart);
        return Result.newSuccess();
    }

    @Override
    public Result<String> update(SysDepartModiDto sysDepartModiDto) {
        SysDepart sysDepart=new SysDepart();
        BeanUtils.copyProperties(sysDepartModiDto,sysDepart);
        sysDepart.setModiNo(sysDepartModiDto.getModiUserNo());
        sysDepart.setModiTime(new Date());
        sysDepartMapper.updateByPrimaryKeySelective(sysDepart);
        return Result.newSuccess();
    }

    @Override
    public Result<String> delete(String id) {
        sysDepartMapper.deleteByPrimaryKey(Integer.valueOf(id));
        return Result.newSuccess();
    }

    @Override
    public Result<List<SysDepartResultDto>> findCondLike(String condStr) {
        List<SysDepart> sysDeparts=sysDepartMapper.selectByCond(condStr);
        if(CollectionUtils.isEmpty(sysDeparts)){
            return Result.newError(ResultCode.COMMON_DATA_EXISTS);
        }
        List<SysDepartResultDto> sysDepartResultDtos=new ArrayList<SysDepartResultDto>(sysDeparts.size());
        for(SysDepart sysDepart:sysDeparts){
            SysDepartResultDto sysDepartResultDto=singleConvertDto(sysDepart);
            sysDepartResultDtos.add(sysDepartResultDto);
        }
        return Result.newSuccess(sysDepartResultDtos);
    }

    private SysDepartResultDto singleConvertDto(SysDepart sysDepart) {
        SysDepartResultDto sysDepartResultDto=new SysDepartResultDto();
        BeanUtils.copyProperties(sysDepart,sysDepartResultDto);
        sysDepartResultDto.setCreateTime(DateUtil.getDateTimeFormat(sysDepart.getCreateTime()));
        sysDepartResultDto.setModiTime(DateUtil.getDateTimeFormat(sysDepart.getModiTime()));
        return sysDepartResultDto;
    }

    @Override
    public Result<SysDepartResultDto> findEntityByNo(String compCode) {
        SysDepart sysDepart=sysDepartMapper.selectByDepartNo(compCode);
        if(sysDepart==null){
            return Result.newError(ResultCode.COMMON_DATA_NOT_EXISTS);
        }
        SysDepartResultDto sysDepartResultDto=singleConvertDto(sysDepart);
        return Result.newSuccess(sysDepartResultDto);
    }

    @Override
    public Page<SysDepartResultDto> listPage(SysDepartCondDto sysDepartCondDto) {
        //使用PageHelper类进行分页
        PageHelper.startPage(sysDepartCondDto.getPageNum(), sysDepartCondDto.getPageSize());
        Page<SysDepart> sysDepartPage = (Page<SysDepart>) sysDepartMapper.selectList(sysDepartCondDto);
        List<SysDepart> sysDepartList = sysDepartPage.getResult();
        Page<SysDepartResultDto> sysDepartResultDtoPage = new Page<>();
        BeanUtils.copyProperties(sysDepartPage, sysDepartResultDtoPage);
        List<SysDepartResultDto> sysDepartResultDtos = buildDto(sysDepartList);
        sysDepartResultDtoPage.addAll(sysDepartResultDtos);
        return sysDepartResultDtoPage;
    }
    private List<SysDepartResultDto> buildDto(List<SysDepart> sysDeparts) {
        if(CollectionUtils.isEmpty(sysDeparts)){
            return null;
        }
        List<SysDepartResultDto> sysDepartResultDtos=new ArrayList<SysDepartResultDto>(sysDeparts.size());
        for(SysDepart sysDepart:sysDeparts){
            SysDepartResultDto sysDepartResultDto=singleConvertDto(sysDepart);
            sysDepartResultDtos.add(sysDepartResultDto);
        }
        return sysDepartResultDtos;
    }


    @Override
    public Result<List<SysDepartResultDto>> listAll() {
        List<SysDepart> sysDepartList = sysDepartMapper.selectAll();
        return Result.newSuccess(buildDto(sysDepartList));
    }
}
