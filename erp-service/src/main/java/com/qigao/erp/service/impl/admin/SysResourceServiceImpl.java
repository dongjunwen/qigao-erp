package com.qigao.erp.service.impl.admin;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qigao.erp.commons.SysResourceService;
import com.qigao.erp.commons.dto.SysResourceCondDto;
import com.qigao.erp.commons.dto.SysResourceCreateDto;
import com.qigao.erp.commons.dto.SysResourceModiDto;
import com.qigao.erp.commons.dto.SysResourceResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.jdbc.mapper.SysResourceMapper;
import com.qigao.erp.jdbc.model.SysResource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Author:luiz
 * @Date: 2018/3/9 10:21
 * @Descripton:
 * @Modify :
 **/
@Service
public class SysResourceServiceImpl implements SysResourceService {
    @Resource
    private SysResourceMapper sysResourceMapper;

    @Override
    public Result<String> create(SysResourceCreateDto sysResourceCreateDto) {
        SysResource oldSysResource = sysResourceMapper.selectBySourceNo(sysResourceCreateDto.getResourceNo());
        if (oldSysResource != null) {
            return Result.newError(ResultCode.COMMON_DATA_EXISTS);
        }
        SysResource sysResource = new SysResource();
        BeanUtils.copyProperties(sysResourceCreateDto, sysResource);
        sysResource.setCreateTime(new Date());
        sysResource.setCreateNo(sysResourceCreateDto.getUserNo());
        sysResourceMapper.insertSelective(sysResource);
        return Result.newSuccess();
    }

    public Result update(SysResourceModiDto sysResourceModiDto) {
        SysResource oldResource = sysResourceMapper.selectByPrimaryKey(Integer.valueOf(sysResourceModiDto.getId()));
        if (oldResource == null) {
            return Result.newError(ResultCode.COMMON_DATA_NOT_EXISTS);
        }
        SysResource sysResource = new SysResource();
        BeanUtils.copyProperties(sysResourceModiDto, sysResource);
        sysResource.setModiTime(new Date());
        sysResource.setModiNo(sysResourceModiDto.getUserNo());
        sysResourceMapper.updateByPrimaryKeySelective(sysResource);
        return Result.newSuccess();
    }

    public Result delete(String id) {
        sysResourceMapper.deleteByPrimaryKey(Integer.valueOf(id));
        return Result.newSuccess();
    }

    public Result<SysResource> findById(String id) {
        return Result.newSuccess(sysResourceMapper.selectByPrimaryKey(Integer.valueOf(id)));
    }

    public Result<List<SysResourceResultDto>> getListByCurrentUser(String currentUserNo) {
        List<SysResource> sysResourceList = sysResourceMapper.selectByUserNo(currentUserNo);
        return Result.newSuccess(buldDto(sysResourceList));
    }

    private List<SysResourceResultDto> buldDto(List<SysResource> sysResourceList) {
        List<SysResourceResultDto> sysResourceResultDtos = new ArrayList<SysResourceResultDto>();
        for (SysResource sysResource : sysResourceList) {
            sysResourceResultDtos.add(buildSingleDto(sysResource));
        }
        return sysResourceResultDtos;
    }

    private SysResourceResultDto buildSingleDto(SysResource sysResource) {
        SysResourceResultDto sysResourceResultDto = new SysResourceResultDto();
        BeanUtils.copyProperties(sysResource,sysResourceResultDto);
        if (sysResource.getResourceLevel() == 1) {
            sysResourceResultDto.setpId(null);
            sysResourceResultDto.setBpid(null);
            sysResourceResultDto.setMpid(null);
        } else {
            sysResourceResultDto.setpId(sysResource.getPresourceNo());
            sysResourceResultDto.setBpid(sysResource.getPresourceNo());
            sysResourceResultDto.setMpid(sysResource.getPresourceNo());
        }
        sysResourceResultDto.setIcon(sysResource.getResourceIcon());
        return sysResourceResultDto;
    }

    public Page<SysResource> listPage(SysResourceCondDto sysResourceCondDto) {
        //使用PageHelper类进行分页
        PageHelper.startPage(sysResourceCondDto.getPageNum(), sysResourceCondDto.getPageSize());
        Page<SysResource> sysResourcePage = sysResourceMapper.selectList(sysResourceCondDto);
        return sysResourcePage;
    }

    @Override
    public Result<List<SysResourceResultDto>> listTree() {
        List<SysResource> sysResourceList = sysResourceMapper.selectAll();
        List<SysResourceResultDto> sysResourceResultDtos=buldDto(sysResourceList);
        return Result.newSuccess( buildTree(sysResourceResultDtos));
    }

    private List<SysResourceResultDto> buildTree(List<SysResourceResultDto> sysResourceResultDtos) {
        List<SysResourceResultDto> collect = sysResourceResultDtos.stream().filter(m -> m.getResourceLevel() == 1).map(
                (m) -> {
                    m.setChildren(buildChild(m, sysResourceResultDtos));
                    return m;
                }
        ).collect(Collectors.toList());
        return collect;
    }

    private  List<SysResourceResultDto>  buildChild(SysResourceResultDto root,List<SysResourceResultDto> all) {
        List<SysResourceResultDto> children = all.stream().filter(m -> {
            return Objects.equals(m.getPresourceNo(), root.getResourceNo());
        }).map(
                (m) -> {
                    m.setChildren(buildChild(m, all));
                    return m;
                }
        ).collect(Collectors.toList());
        return children;
    }

    public Result<List<SysResourceResultDto>> findReSourceLike(String condStr) {
        List<SysResource> sysResourceList = sysResourceMapper.selectByCond(condStr);
        return Result.newSuccess(buldDto(sysResourceList));
    }

    public Result<List<SysResourceResultDto>> listAll() {
        List<SysResource> sysResourceList = sysResourceMapper.selectAll();
        return Result.newSuccess(buldDto(sysResourceList));
    }


}
