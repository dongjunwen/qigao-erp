package com.qigao.erp.service.channel;

/**
 * @author luiz
 * @Title: com.qigao.erp.service.channel
 * @ProjectName three-mall
 * @date 2019-11-2019/11/28 15:07
 * @Description:
 */
public class RegisterTemplate {

    public static final String startRegisterHtmlTemplate="<html>"+
            "\n" +
            "\n" +
            "\n" +
            "<div id=\"content\">\n" +
            "\n" +
            "<style></style>\n" +
            "<div class=\"mail_area mail_pc\" id=\"app_mail\">\n" +
            "    <div class=\"mail\">\n" +
            "        <div class=\"mail_inner\">\n" +
            "            <h1>%s</h1>\n" +
            "            <div class=\"mail_msg\">\n" +
            "                <p>\n" +
            "                    你好，<br>\n" +
            "                    感谢你注册%s。<br>\n" +
            "                    你的登录邮箱为：<a data-auto-link=\"1\" href=\"mailto:%s\">%s</a>。请点击以下链接激活账号，\n" +
            "                </p>\n" +
            "                <p>\n" +
            "                    <a href=\"%s\" target=\"_blank\">%s</a>\n" +
            "                </p>\n" +
            "                <p>\n" +
            "                    如果以上链接无法点击，请将上面的地址复制到你的浏览器地址栏进入%s平台。\n" +
            "                </p>\n" +
            "                <div class=\"mail_info\" align=\"right\">\n" +
            "                    <strong>%s团队</strong>\n" +
            "                </div>\n" +
            "            </div>\n" +
            "            <div class=\"pic_skin_top\"></div>\n" +
            "            <div class=\"pic_skin_bottom\"></div>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            " \n" +
            "\n" +
            "</div>\n" +
            "</html>" ;


    public static final String forgetPassHtmlTemplate="<html>"+
            "\n" +
            "\n" +
            "\n" +
            "<div id=\"content\">\n" +
            "\n" +
            "<style></style>\n" +
            "<div class=\"mail_area mail_pc\" id=\"app_mail\">\n" +
            "    <div class=\"mail\">\n" +
            "        <div class=\"mail_inner\">\n" +
            "            <h1>%s</h1>\n" +
            "            <div class=\"mail_msg\">\n" +
            "                <p>\n" +
            "                    你好，<br>\n" +
            "                    感谢你使用%s。<br>\n" +
            "                    你的登录邮箱为：<a data-auto-link=\"1\" href=\"mailto:%s\">%s</a>。请点击以下链接重新设置密码，\n" +
            "                </p>\n" +
            "                <p>\n" +
            "                    <a href=\"%s\" target=\"_blank\">%s</a>\n" +
            "                </p>\n" +
            "                <p>\n" +
            "                    如果以上链接无法点击，请将上面的地址复制到你的浏览器地址栏进入%s平台。\n" +
            "                </p>\n" +
            "                <div class=\"mail_info\" align=\"right\">\n" +
            "                    <strong>%s团队</strong>\n" +
            "                </div>\n" +
            "            </div>\n" +
            "            <div class=\"pic_skin_top\"></div>\n" +
            "            <div class=\"pic_skin_bottom\"></div>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            " \n" +
            "\n" +
            "</div>\n" +
            "</html>" ;

    public static final String registerSuccessHtmlTemplate="<div style=\"background-color:#ECECEC; padding: 35px;\">\n" +
            "    <table cellpadding=\"0\" align=\"center\"\n" +
            "           style=\"width: 600px; margin: 0px auto; text-align: left; position: relative; border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; font-size: 14px; font-family:微软雅黑, 黑体; line-height: 1.5; box-shadow: rgb(153, 153, 153) 0px 0px 5px; border-collapse: collapse; background-position: initial initial; background-repeat: initial initial;background:#fff;\">\n" +
            "        <tbody>\n" +
            "        <tr>\n" +
            "            <th valign=\"middle\"\n" +
            "                style=\"height: 25px; line-height: 25px; padding: 15px 35px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #42a3d3; background-color: #49bcff; border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px;\">\n" +
            "                <font face=\"微软雅黑\" size=\"5\" style=\"color: rgb(255, 255, 255); \">注册成功! （%s）</font>\n" +
            "            </th>\n" +
            "        </tr>\n" +
            "        <tr>\n" +
            "            <td>\n" +
            "                <div style=\"padding:25px 35px 40px; background-color:#fff;\">\n" +
            "                    <h2 style=\"margin: 5px 0px; \">\n" +
            "                        <font color=\"#333333\" style=\"line-height: 20px; \">\n" +
            "                            <font style=\"line-height: 22px; \" size=\"4\">\n" +
            "                                亲爱的 %s</font>\n" +
            "                        </font>\n" +
            "                    </h2>\n" +
            "                    <p>首先感谢您加入本站！下面是您的账号信息<br>\n" +
            "                        您的账号：<b>%s</b><br>\n" +
            "                        您的密码：<b>%s</b><br>\n" +
            "                        您的邮箱：<b>%s</b><br>\n" +
            "                        当您在使用本网站时，遵守当地法律法规。<br>\n" +
            "                        如果您有什么疑问可以联系管理员，Email:%s</p>\n" +
            "                    <div style=\"width:700px;margin:0 auto;\">\n" +
            "                        <div style=\"padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px;\">\n" +
            "                            <p>此为系统邮件，请勿回复<br>\n" +
            "                                请保管好您的邮箱，避免账号被他人盗用\n" +
            "                            </p>\n" +
            "                            <p>©%s</p>\n" +
            "                        </div>\n" +
            "                    </div>\n" +
            "                </div>\n" +
            "            </td>\n" +
            "        </tr>\n" +
            "        </tbody>\n" +
            "    </table>\n" +
            "</div>\n" ;



    public static String registerConfirm(String sysName,String emailAddr,String activeUrl){
        String registerConfirmStr=String.format(
                RegisterTemplate.startRegisterHtmlTemplate,sysName,sysName,
                emailAddr,emailAddr,
                activeUrl,activeUrl,
                sysName, sysName
        );
        return registerConfirmStr;
    }

    public static String forgetPass(String sysName,String emailAddr,String activeUrl){
        String registerConfirmStr=String.format(
                RegisterTemplate.forgetPassHtmlTemplate,
                sysName,sysName,
                emailAddr,emailAddr,
                activeUrl,activeUrl,
                sysName, sysName
        );
        return registerConfirmStr;
    }
}
