package com.qigao.erp.service.impl.admin;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qigao.erp.commons.SysRoleService;
import com.qigao.erp.commons.dto.SysRoleCondDto;
import com.qigao.erp.commons.dto.SysRoleCreateDto;
import com.qigao.erp.commons.dto.SysRoleModiDto;
import com.qigao.erp.commons.dto.SysRoleResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.ResultCode;
import com.qigao.erp.jdbc.mapper.SysRoleMapper;
import com.qigao.erp.jdbc.model.SysRole;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author:luiz
 * @Date: 2018/3/9 10:22
 * @Descripton:
 * @Modify :
 **/
@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    SysRoleMapper sysRoleMapper;

    public Result create(SysRoleCreateDto sysRoleCreateDto) {
        SysRole oldSysRole = selectByRoleCode(sysRoleCreateDto.getRoleCode());
        if (oldSysRole != null) {
            return Result.newError(ResultCode.COMMON_DATA_EXISTS);
        }
        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(sysRoleCreateDto, sysRole);
        sysRole.setCreateNo(sysRoleCreateDto.getCreateUserNo());
        sysRole.setCreateTime(new Date());
        sysRole.setModiNo(sysRoleCreateDto.getCreateUserNo());
        sysRole.setModiTime(new Date());
        sysRoleMapper.insertSelective(sysRole);
        return Result.newSuccess();
    }

    @Override
    public Result update(SysRoleModiDto sysRoleModiDto) {
        SysRole oldSysRole = selectByRoleCode(sysRoleModiDto.getRoleCode());
        if (oldSysRole == null) {
            return Result.newError(ResultCode.COMMON_DATA_NOT_EXISTS);
        }
        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(sysRoleModiDto, sysRole);
        sysRole.setId(oldSysRole.getId());
        sysRole.setModiNo(sysRoleModiDto.getModiUserNo());
        sysRole.setModiTime(new Date());
        sysRoleMapper.updateByPrimaryKeySelective(sysRole);
        return Result.newSuccess();
    }


    public Result delete(String id) {
        sysRoleMapper.deleteByPrimaryKey(Integer.valueOf(id));
        return Result.newSuccess();
    }

    public SysRole selectByRoleCode(String roleCode) {
        return sysRoleMapper.selectByRoleCode(roleCode);
    }

    @Override
    public Result<SysRoleResultDto> getEntityByNo(String roleCode) {
        SysRole sysRole = selectByRoleCode(roleCode);
        if (sysRole != null) {
            SysRoleResultDto sysRoleResultDto = new SysRoleResultDto();
            BeanUtils.copyProperties(sysRole, sysRoleResultDto);
            return Result.newSuccess(sysRoleResultDto);
        }
        return Result.newError(ResultCode.COMMON_DATA_NOT_EXISTS);
    }

    public Result<List<SysRoleResultDto>> findRoleLike(String condStr) {
        List<SysRole> sysRoles = sysRoleMapper.selectRoleLike(condStr);
        return Result.newSuccess(buildDto(sysRoles));
    }


    public Result listAll() {
        List<SysRole> sysRoles = sysRoleMapper.selectAll();
        return Result.newSuccess(buildDto(sysRoles));
    }

    private List<SysRoleResultDto> buildDto(List<SysRole> sysRoles) {
        List<SysRoleResultDto> sysRoleResultDtos = new ArrayList<>();
        for (SysRole sysRole : sysRoles) {
            SysRoleResultDto sysRoleResultDto = new SysRoleResultDto();
            BeanUtils.copyProperties(sysRole, sysRoleResultDto);
            sysRoleResultDto.setId(String.valueOf(sysRole.getId()));
            sysRoleResultDtos.add(sysRoleResultDto);
        }
        return sysRoleResultDtos;
    }

    @Override
    public Page<SysRoleResultDto> listPage(SysRoleCondDto sysRoleCondDto) {
        //使用PageHelper类进行分页
        PageHelper.startPage(sysRoleCondDto.getPageNum(), sysRoleCondDto.getPageSize());
        Page<SysRole> sysRolePage = (Page<SysRole>) sysRoleMapper.selectList(sysRoleCondDto);
        List<SysRole> sysRoles = sysRolePage.getResult();
        Page<SysRoleResultDto> sysRoleResultDtoPage = new Page<>();
        BeanUtils.copyProperties(sysRolePage, sysRoleResultDtoPage);
        List<SysRoleResultDto> sysRoleResultDtos = buildDto(sysRoles);
        sysRoleResultDtoPage.addAll(sysRoleResultDtos);
        return sysRoleResultDtoPage;
    }
}
