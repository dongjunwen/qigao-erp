package com.qigao.erp.service.impl.admin;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qigao.erp.commons.SysDictService;
import com.qigao.erp.commons.dto.SysDictCondDto;
import com.qigao.erp.commons.dto.SysDictDto;
import com.qigao.erp.commons.dto.SysDictResultDto;
import com.qigao.erp.commons.enums.Result;
import com.qigao.erp.commons.enums.StatusCharEnum;
import com.qigao.erp.jdbc.mapper.SysDictInfoMapper;
import com.qigao.erp.jdbc.model.SysDictInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author luiz
 * @Title: SysDictServiceImpl
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-17 13:32
 */
@Service
public class SysDictServiceImpl implements SysDictService {
    @Autowired
    private SysDictInfoMapper sysDictInfoMapper;

    @Override
    public Result create(SysDictDto sysDictDto) {
        SysDictInfo sysDictInfo = new SysDictInfo();
        BeanUtils.copyProperties(sysDictDto, sysDictInfo);
        sysDictInfo.setDictStatus(Integer.valueOf(sysDictDto.getDictStatus()));
        sysDictInfoMapper.insertSelective(sysDictInfo);
        return Result.newSuccess();
    }

    @Override
    public Result update(SysDictDto sysDictDto) {
        SysDictInfo sysDictInfo = new SysDictInfo();
        BeanUtils.copyProperties(sysDictDto, sysDictInfo);
        sysDictInfo.setDictStatus(Integer.valueOf(sysDictDto.getDictStatus()));
        sysDictInfoMapper.updateByPrimaryKeySelective(sysDictInfo);
        return Result.newSuccess();
    }

    @Override
    public Result<List<SysDictResultDto>> findByPDict(String pDictNo) {
        List<SysDictInfo> sysDictInfos = sysDictInfoMapper.selectBypDictNo(pDictNo);
        List<SysDictResultDto> sysDictResultDtos = convertDto(sysDictInfos);
        return Result.newSuccess(sysDictResultDtos);
    }

    @Override
    public Result<List<SysDictResultDto>> findByPDictEnable(String pDictNo) {
        List<SysDictInfo> sysDictInfos = sysDictInfoMapper.findByPDictEnable(pDictNo);
        List<SysDictResultDto> sysDictResultDtos = convertDto(sysDictInfos);
        return Result.newSuccess(sysDictResultDtos);
    }

    private List<SysDictResultDto> convertDto(List<SysDictInfo> sysDictInfos) {
        List<SysDictResultDto> sysDictResultDtos = new ArrayList<>();
        for (SysDictInfo sysDictInfo : sysDictInfos) {
            sysDictResultDtos.add(convertSingleDto(sysDictInfo));
        }
        return sysDictResultDtos;
    }

    @Override
    public Result<SysDictResultDto> findById(String id) {
        SysDictInfo sysDictInfo = sysDictInfoMapper.selectByPrimaryKey(Integer.valueOf(id));
        SysDictResultDto sysDictResultDto = convertSingleDto(sysDictInfo);
        return Result.newSuccess(sysDictResultDto);
    }

    private SysDictResultDto convertSingleDto(SysDictInfo sysDictInfo) {
        SysDictResultDto sysDictResultDto = new SysDictResultDto();
        BeanUtils.copyProperties(sysDictInfo, sysDictResultDto);
        sysDictResultDto.setId(String.valueOf(sysDictInfo.getId()));
        sysDictResultDto.setDictStatus(sysDictInfo.getDictStatus().toString());
        sysDictResultDto.setDictStatusName(StatusCharEnum.getNameByCode(sysDictInfo.getDictStatus()));
        return sysDictResultDto;
    }

    @Override
    public Page<SysDictResultDto> findListPage(SysDictCondDto sysDictCondDto) {
        //使用PageHelper类进行分页
        PageHelper.startPage(sysDictCondDto.getPageNum(), sysDictCondDto.getPageSize());
        Page<SysDictInfo> sysDictInfoPage = (Page) findList(sysDictCondDto);
        Page<SysDictResultDto> topicResultDtoPage = new Page<SysDictResultDto>();
        BeanUtils.copyProperties(sysDictInfoPage, topicResultDtoPage);
        topicResultDtoPage.addAll(convertDto(sysDictInfoPage.getResult()));
        return topicResultDtoPage;
    }

    @Override
    public Result<List<SysDictResultDto>> listPDictAll() {
        List<SysDictInfo> sysDictInfos = sysDictInfoMapper.selectForLevel1();
        List<SysDictResultDto> sysDictResultDtos = convertDto(sysDictInfos);
        return Result.newSuccess(sysDictResultDtos);
    }

    @Override
    public void save(SysDictDto sysDictDto) {
        SysDictInfo sysDictInfoOld=sysDictInfoMapper.findByUniq(sysDictDto.getPdictNo(),sysDictDto.getDictNo());
        if(sysDictInfoOld!=null){
            SysDictInfo sysDictInfo =new SysDictInfo();
            BeanUtils.copyProperties(sysDictDto,sysDictInfo);
            sysDictInfo.setId(sysDictInfoOld.getId());
            sysDictInfoMapper.updateByPrimaryKeySelective(sysDictInfo);
        }else {
            SysDictInfo sysDictInfo =new SysDictInfo();
            BeanUtils.copyProperties(sysDictDto,sysDictInfo);
            sysDictInfoMapper.insertSelective(sysDictInfo);
        }
    }

    @Override
    public String findByUniq(String pdictCode, String dictCode) {
        SysDictInfo sysDictInfoOld=sysDictInfoMapper.findByUniq(pdictCode,dictCode);
        if(sysDictInfoOld!=null){
            return sysDictInfoOld.getDictVal();
        }
        return null;
    }

    @Override
    public Result<List<SysDictResultDto>> findByPDictAndDictNo(String pDictNo, String dictNo) {
        List<SysDictInfo> sysDictInfos = sysDictInfoMapper.selectBypDictNo(pDictNo);
        sysDictInfos=sysDictInfos.stream().filter(o->o.getDictNo().equals(dictNo)).collect(Collectors.toList());
        List<SysDictResultDto> sysDictResultDtos = convertDto(sysDictInfos);
        return Result.newSuccess(sysDictResultDtos);
    }

    public List<SysDictInfo> findList(SysDictCondDto sysDictCondDto) {
        return sysDictInfoMapper.selectList(sysDictCondDto);
    }
}
