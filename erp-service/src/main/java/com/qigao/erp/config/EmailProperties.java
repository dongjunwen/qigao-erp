package com.qigao.erp.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.config
 * @ProjectName three-mall
 * @date 2019-11-2019/11/8 16:45
 * @Description:
 */
@Configuration
@Data
public class EmailProperties {
    @Value("${qigao.sysName:起高软件}")
    private String sysName;
    @Value("${spring.mail.host}")
    private String emailHost;
    @Value("${spring.mail.username}")
    private String emailUserName;
    @Value("${spring.mail.password}")
    private String emailPassword;
    @Value("${qigao.registerActiveUrl}")
    private String registerActiveUrl;
    @Value("${qigao.forgetUrl}")
    private String forgetUrl;
    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(emailHost);
        javaMailSender.setUsername(emailUserName);
        javaMailSender.setPassword(emailPassword);
        javaMailSender.setDefaultEncoding("UTF-8");
        return javaMailSender;
    }

}
