package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: SysResourceCondDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-08 18:25
 */
@Data
public class SysResourceCondDto extends PageDto {
    @ApiModelProperty(value = "资源代码")
    private String resourceNo;
    @ApiModelProperty(value = "资源名称")
    private String resourceName;
    @ApiModelProperty(value = "上级资源代码")
    private String preSourceNo;
}
