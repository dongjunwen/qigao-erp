package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: NoticeResultDto
 * Author:   luiz
 * Date:     2019/11/10 15:39
 * Description: 公告结果
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/10 15:39       版本号              描述
 */
@Data
public class NoticeModiDto {

    private Integer id;

    private String noticeDate;

    private String noticeTitle;

    private String noticeContent;

    private String noticePic;

    private String userNo;
}
