package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2020-04-2020/4/13 10:18
 * @Description:
 */
@Data
public class MonthReportDto extends PageDto {
    private String shopNo;
    private String monthNum;
}
