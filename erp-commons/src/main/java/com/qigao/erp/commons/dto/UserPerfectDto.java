package com.qigao.erp.commons.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author luiz
 * @Title: UserRegisterDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-04 10:22
 */
@Data
public class UserPerfectDto {
    @ApiModelProperty(value = "用户号", required = true)
    private String userNo;
    /**
     * 邮箱地址
     */
    private String emailAddr;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 头像
     */
    private String headImgUrl;

    //"微信号"
    private String weichatNo;
    //"FaceBook")
    private String facebookNo;
    //"line")
    private String lineNo;
    //"性别 0:男 1:女 ",required = true)
    private Integer sexType;
    //"年龄",required = false)
    private Integer age;
    //"常住城市",required = false)
    private String activeCity;
    //"国籍",required = false)
    private String country;
    //"语言 0:中文 1:日文 2:英文",required = false)
    private Integer languageType;
    //"是否激活交友功能 0:否 1:是",required = false)
    private Integer friendActive;
    //"身高",required = false)
    private Integer height;
    //"体重",required = false)
    private Integer weight;
    //注册IP
    private String registerIp;
    //"标签",required = false)
    private List<String> tagList;
    //图片
    private PicDto picDto;
}
