package com.qigao.erp.commons.dto;

import lombok.Data;

@Data
public class PicDto {
    private String topicNo;
    /**
     * 原始文件
     */
    private String fileNo;
    /**
     * 压缩后文件
     */
    private String zipFileNo;
    /**
     * 缩小后文件
     */
    private String narrowFileNo;
    private String fileName;
    private String origFileName;
    private int fileSize;
    private String fileType;
    private String picPath;
    private String userNo;
}
