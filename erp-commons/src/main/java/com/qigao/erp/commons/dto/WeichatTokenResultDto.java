package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: WeichatLoginDto
 * Author:   luiz
 * Date:     2019/11/10 14:36
 * Description: 微信TokenDto
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/10 14:36       版本号              描述
 */
@Data
public class WeichatTokenResultDto {
    private String accessToken;
    private String expiresIn;
}
