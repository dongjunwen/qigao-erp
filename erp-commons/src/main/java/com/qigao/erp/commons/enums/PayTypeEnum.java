package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @Author:luiz
 * @Date: 2018/5/28 10:14
 * @Descripton:付款类型 0:在线支付 1:线下支付
 * @Modify :
 **/
public enum PayTypeEnum {
    ONLINE_PAY("0","在线支付"),
    OFFLINE_PAY("1","线下支付"),
    ;
    private String code;
    private String name;

    PayTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final PayTypeEnum parse(String code){
        for(PayTypeEnum statusEnum : PayTypeEnum.values()){
            if(code.equals(statusEnum.getCode())) return statusEnum;
        }
        return null;
    }

    public static String getNameByCode(String status) {
        PayTypeEnum statusEnum = matching(status);
        if (statusEnum != null) return statusEnum.getName();
        return "";
    }

    public static final PayTypeEnum matching(String code) {
        for (PayTypeEnum orderStatusEnum : PayTypeEnum.values()) {
            if (Objects.equals(orderStatusEnum.getCode(), code)) return orderStatusEnum;
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
