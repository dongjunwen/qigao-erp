package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 15:54
 * @Description:
 */
@Data
public class OrderCreateDetailDto {
    @NotNull(message = "商品编号不能为空!")
    @Length(min = 1,max = 32)
    private String itemNo;
    @NotNull(message = "库存编号不能为空!")
    @Length(min = 1,max = 32)
    private String stockNo;
   /* @NotNull(message = "商品名称不能为空!")
    @Length(min = 1,max = 64)
    private String itemName;
    @NotNull(message = "商品图片地址不能为空!")
    @Length(min = 1,max = 256)
    private String itemPicNo;
    @NotNull(message = "商品单价不能为空!")
    private String itemPrice;*/
    @NotNull(message = "商品数量不能为空!")
    @Length(min = 1,max = 16)
    @Min(value = 1,message = "购买数量不合法")
    private String buyNum;
    @ApiModelProperty("优惠金额")
    @DecimalMin(value = "0.00",message = "优惠金额最小数字为0.00")
    private BigDecimal coupAmt=BigDecimal.ZERO;
    @ApiModelProperty("抵扣积分")
    @DecimalMin(value = "0.00",message = "抵扣积分最小数字为0.00")
    private BigDecimal scoreAmt=BigDecimal.ZERO;
}
