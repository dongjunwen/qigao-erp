package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/12 13:59
 * @Description:
 */
@Data
public class WeiChatLoginResultDto {
    @ApiModelProperty("登陆返回的token值")
    private String tokenStr;
    @ApiModelProperty("登陆返回的用户在开放平台的唯一标识符")
    private String unionid;
}
