package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 16:26
 * @Description:
 */
@Data
@ApiModel("每月销售报表")
public class SaleMonthReportDto {
    //销售月份
    @ApiModelProperty("销售月份 yyyy-mm 格式")
    private String saleMonth;
    @ApiModelProperty("销售金额")
    private BigDecimal saleAmt=BigDecimal.ZERO;
    @ApiModelProperty("昨日订单数")
    private BigDecimal yesterdayOrderNum=BigDecimal.ZERO;
    @ApiModelProperty("昨日付款金额")
    private BigDecimal yesterdayPayAmt=BigDecimal.ZERO;
    @ApiModelProperty("昨日付款数量")
    private BigDecimal yesterdayPayNum=BigDecimal.ZERO;
    @ApiModelProperty("昨日支付转换率")
    private BigDecimal yesterdayPayRate=BigDecimal.ZERO;
}
