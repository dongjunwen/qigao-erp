package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 审核动作
 * @date 2019-04-03 16:00
 */
public enum AuditActionEnum {
    PASS("PASS", "通过"),
    REJECT("REJECT", "拒绝"),
    ;

    String code;
    String name;

    AuditActionEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(int status) {
        AuditActionEnum statusEnum = matching(status);
        if (statusEnum != null) return statusEnum.getName();
        return "";
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final AuditActionEnum matching(int code) {
        for (AuditActionEnum statusEnum : AuditActionEnum.values()) {
            if (Objects.equals(statusEnum.getCode(), code)) return statusEnum;
        }
        return null;
    }
}
