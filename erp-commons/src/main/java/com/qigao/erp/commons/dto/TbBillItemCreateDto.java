package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-18 15:18
 * @Description:
 */
@Data
public class TbBillItemCreateDto {
    private Integer userType;
    private String userLevel;
    @ApiModelProperty("费用项")
    private String itemCode;
    @ApiModelProperty("费用项名称")
    private String itemName;
    @ApiModelProperty("分润类型 FIX:固定金额 PERCENT:比例")
    private String itemType;
    @ApiModelProperty("分润值")
    private BigDecimal profitPercent=BigDecimal.ZERO;
    private String createUserNo;
}
