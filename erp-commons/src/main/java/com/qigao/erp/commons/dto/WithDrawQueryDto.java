package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/21 14:13
 * @Description:
 */
@Data
@ApiModel("提现查询条件")
public class WithDrawQueryDto extends PageDto {
    @ApiModelProperty("店铺编号")
    @NotNull(message = "店铺号不能为空")
    private String shopNo;
    @ApiModelProperty("提现状态")
    private String withDrawStatus;
    @ApiModelProperty("开始时间")
    private String startTime;
    @ApiModelProperty("结束时间")
    private String endTime;
    private String ownerNo;
}
