package com.qigao.erp.commons.enums;

/**
 * @Author:luiz
 * @Date: 2018/5/28 10:14
 * @Descripton:交易类型
 * @Modify :
 **/
public enum TradeTypeEnum {
    PAY(0,"付款","-"),
    REFUND(1,"退款","+"),
    WITHDRAW(2,"提现","-"),
    WITHDRAW_BACK(3,"提现回退","+"),
    RECHARGE(4,"充值","+"),
    RECHARGE_BACK(5,"充值回退","-"),
    ACQUIRE_SCORE(6,"获得积分","+"),
    CONSUMER_SCORE(7,"消费积分","-"),
    SETTLE_AMT(8,"费用结算","+"),
    ;

    private int code;
    private String name;
    private String amtDirect;


    TradeTypeEnum(int code, String name,String amtDirect) {
        this.code = code;
        this.name = name;
        this.amtDirect=amtDirect;
    }

    public static final TradeTypeEnum parse(int code){
        for(TradeTypeEnum statusEnum : TradeTypeEnum.values()){
            if(code==statusEnum.getCode()) return statusEnum;
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmtDirect() {
        return amtDirect;
    }
}
