package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 16:00
 */
public enum IfFormulaEnum {
    NO(0, "未转正"),
    YES(1, "已转正"),
    ;

    int code;
    String name;

    IfFormulaEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(int status) {
        IfFormulaEnum statusEnum = matching(status);
        if (statusEnum != null) return statusEnum.getName();
        return "";
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final IfFormulaEnum matching(int code) {
        for (IfFormulaEnum statusEnum : IfFormulaEnum.values()) {
            if (Objects.equals(statusEnum.getCode(), code)) return statusEnum;
        }
        return null;
    }
}
