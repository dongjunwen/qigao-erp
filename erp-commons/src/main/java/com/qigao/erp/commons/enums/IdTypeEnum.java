package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 字典类型枚举
 * @date 2019-04-03 16:00
 */
public enum IdTypeEnum {
    ACCOUNT("A", "账户"),
    COMMENTS("C", "评论"),
    COMPNAY("COM", "公司"),
    ITEM_CAT("IC", "商品分类"),
    FILE("F", "文件"),
    MER("MER", "商户号"),
    ENTER_SEQ("ET", "进件申请业务号"),
    ORDER("O", "订单号"),
    SEQ("S", "流水号"),
    SHOP("SH", "店铺号"),
    SETTLE("SE", "结算"),
    STOCK("ST", "库存"),
    PIC("P", "图片"),
    USER("U", "用户"),
    TOPIC("T", "话题"),
    TRADE("TR", "交易"),
    MSG("M", "站内信"),
    RADOM("R", "随机数"),
    REFUND("RE", "退款信息"),
    REFUND_SEQ("RS", "退款流水信息"),
    GOODS("G", "商品信息"),
    WITHDRAW("W", "提现"),
    ;

    String code;
    String name;

    IdTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final IdTypeEnum matching(String code) {
        for (IdTypeEnum resultCode : IdTypeEnum.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }
}
