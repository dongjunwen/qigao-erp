package com.qigao.erp.commons.dto;

import lombok.Data;

@Data
public class MerInfoDto {
    private Long id;

    private String merNo;

    private String merName;

    private String pmerNo;

    private String merType;

    private String merchantShortname;

    private Integer status;

    private Integer enterStatus;

    private String enterStatusName;

    private String idCardNumber;

    private String idCardName;

    private String idCardCopy;

    private String idCardNational;

    private String idCardValidTime;

    private String accountName;

    private String accountBank;

    private String bankAddressCode;

    private String bankName;

    private String accountNumber;

    private String storeName;

    private String storeAddressCode;

    private String storeStreet;

    private String storeLongitude;

    private String storeLatitude;

    private String storeEntrancePic;

    private String indoorPic;

    private String addressCertification;

    private String servicePhone;

    private String productDesc;

    private String rate;

    private String businessAdditionDesc;

    private String businessAdditionPics;

    private String contact;

    private String contactPhone;

    private String subMchId;

    private String contactEmail;

    private String businessCode;

    private String applymentId;

    private String failMsg;


    private String createTime;

    private String createNo;

    private String modiTime;

    private String modiNo;
}
