package com.qigao.erp.commons.enums;

/**
 * @Author:luiz
 * @Date: 2018/5/28 10:14
 * @Descripton:
 * @Modify : 用户来源 0:微信公众号 1:系统内部
 **/
public enum UserSourceEnum {
    WEICHAIT(0,"微信公众号"),
    SYS_INNER(1,"系统内部"),
    THIRD_AUTH(2,"第三方认证"),
    CUSTOMER(3,"客户");;

    private int code;
    private String name;

    UserSourceEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final UserSourceEnum parse(int code){
        for(UserSourceEnum statusEnum : UserSourceEnum.values()){
            if(code==statusEnum.getCode()) return statusEnum;
        }
        return null;
    }

    public static String getNameByCode(Integer userSource) {
        if(userSource==null)return null;
        UserSourceEnum userSourceEnum=parse(userSource);
        if(userSourceEnum!=null)return userSourceEnum.getName();
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
