package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/1 14:39
 * @Description:
 */
@Data
public class ItemCatModiDto {
    /**
     * 店铺编号
     */
    private String shopNo;
    /**
     * 类目编号
     */
    private String itemCatNo;
    /**
     * 类目名称
     */
    private String catName;
    /**
     * 上级类目编号
     */
    private String pCatNo;
    /**
     * 排序
     */
    private Integer itemSort;

    private String userNo;

}
