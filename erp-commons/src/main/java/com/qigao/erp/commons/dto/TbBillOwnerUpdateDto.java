package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-18 16:04
 * @Description:
 */
@Data
public class TbBillOwnerUpdateDto {
    @ApiModelProperty("费用项编码")
    @NotNull(message = "费用项编码不能为空!")
    private String itemCode;
    @ApiModelProperty("费用项归属列表")
    List<TbBillOwnerDto> tbBillOwnerDtos;
}
