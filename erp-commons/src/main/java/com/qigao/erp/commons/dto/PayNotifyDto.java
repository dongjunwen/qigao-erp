package com.qigao.erp.commons.dto;

import com.qigao.erp.commons.enums.PayStatusEnum;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/11 17:03
 * @Description:
 * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_7&index=8
 */
@Data
public class PayNotifyDto {
    private String appId;
    private String mchId;
    private String nonceStr;
    private String sign;
    private String openid;
    //用户是否关注公众账号，Y-关注，N-未关注
    private String isSubscribe;
    private String tradeType;
    private String bankType;
    //	订单总金额，单位为元
    private BigDecimal totalFee;
    //现金支付金额;
    private String cashFee;
    //微信支付订单号
    private String transactionId;
    //商户订单号
    private String outTradeNo;
    //支付完成时间 格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010
    private String timeEnd;

    private Date closeTime;
    private Date paySuccessTime;

    private String resultCode;
    private String tradeState;
    private String tradeStateDesc;

    private PayStatusEnum tradeStatus;
    private String buylerId;



}
