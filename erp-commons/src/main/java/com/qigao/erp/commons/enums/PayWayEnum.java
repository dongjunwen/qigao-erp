package com.qigao.erp.commons.enums;

/**
@Author:luiz
@Date: 2018/5/25 16:15
@Descripton:支付方式枚举
@Modify :
 **/
public enum PayWayEnum {

    ALIPAY("ALIPAY","支付宝支付"),
    WEICHAT("WEICHAT","微信支付"),
    CASHIER("CASHIER","现金支付"),
    WEICHAT_TRANSFER("WEICHAT_TRANSFER","微信转账"),
    ALIPAY_TRANSFER("ALIPAY_TRANSFER","支付宝转账"),
    BANK_CARD("BANK_CARD","银行卡扣款"),
    ACCOUNT_BALANCE("ACCOUNT_BALANCE","账户余额"),
    ;
    private String payWayCode;
    private String payWayName;

    PayWayEnum(String payWayCode, String payWayName) {
        this.payWayCode = payWayCode;
        this.payWayName = payWayName;
    }

    public static final PayWayEnum parse(String payWayCode){
        for(PayWayEnum payWayEnum : PayWayEnum.values()){
            if(payWayCode.equals(payWayEnum.getPayWayCode())) return payWayEnum;
        }
        return null;
    }

    public String getPayWayCode() {
        return payWayCode;
    }

    public String getPayWayName() {
        return payWayName;
    }
}