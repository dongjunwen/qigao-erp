package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 数据类型枚举
 * @date 2019-04-03 16:00
 */
public enum DataTypeEnum {
    FLOAT("float", "单精度浮点型"),
    BOOL("bool", "布尔型"),
    DOUBLE("double", "双精度浮点型"),
    INT32("int32", "整数型"),
    ;

    String code;
    String name;

    DataTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final DataTypeEnum matching(String code) {
        for (DataTypeEnum resultCode : DataTypeEnum.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }
}
