package com.qigao.erp.commons.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: UserRegisterDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-04 10:22
 */
@Data
public class UserUpdateDto extends UserCreateDto{
    @ApiModelProperty(value = "用户ID", required = true)
    private String id;
    private String operNo;
}
