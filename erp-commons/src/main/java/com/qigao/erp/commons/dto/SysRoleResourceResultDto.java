package com.qigao.erp.commons.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: SysRoleResourceResultDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-09 9:53
 */
@Data
public class SysRoleResourceResultDto {
    @ApiModelProperty(value = "主键Id")
    private String id;
    /**
     * 面包屑导航的父id
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String pId;
    private String name;
    @ApiModelProperty(value = "角色代码")
    private String roleCode;
    @ApiModelProperty(value = "资源代码")
    private String sourceNo;
    @ApiModelProperty(value = "资源名称")
    private String sourceName;
    @ApiModelProperty(value = "资源路径")
    private String reqUrl;
    @ApiModelProperty(value = "资源图标")
    private String sourceIcon;
    @ApiModelProperty(value = "上级编号")
    private String pSourceNo;
    @ApiModelProperty(value = "资源级别")
    private String sourceLevel;
    @ApiModelProperty(value = "是否拥有权限")
    private boolean checked;
}
