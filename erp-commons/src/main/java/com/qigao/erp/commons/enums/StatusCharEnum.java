package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 16:00
 */
public enum StatusCharEnum {
    UNVALID("0", "无效"),
    VALID("1", "有效"),
    ;

    String code;
    String name;

    StatusCharEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(int status) {
        StatusCharEnum statusEnum = matching(status);
        if (statusEnum != null) return statusEnum.getName();
        return "";
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final StatusCharEnum matching(int code) {
        for (StatusCharEnum statusEnum : StatusCharEnum.values()) {
            if (Objects.equals(statusEnum.getCode(), code)) return statusEnum;
        }
        return null;
    }
}
