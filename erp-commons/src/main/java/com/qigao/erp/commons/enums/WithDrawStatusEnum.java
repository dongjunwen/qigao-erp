package com.qigao.erp.commons.enums;

/**
 * @Author:luiz
 * @Date: 2018/5/28 10:14
 * @Descripton:提现状态
 * @Modify :
 **/
public enum WithDrawStatusEnum {
    WITHDRAW_INIT(0,"提现中"),
    WITHDRAW_SUCCESS(1,"提现成功"),
    WITHDRAW_REJECT(2,"拒绝提现"),
    ;

    private int code;
    private String name;

    WithDrawStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final WithDrawStatusEnum parse(int code){
        for(WithDrawStatusEnum statusEnum : WithDrawStatusEnum.values()){
            if(code==statusEnum.getCode()) return statusEnum;
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
