package com.qigao.erp.commons.region;

/**
 * 区，县
 * @author jx on 2018/4/12.
 */

public class Area {
    private String cityCode;
    private String code;
    private String name;
    /**
     * 公布年月
     */
    private String publishYm;

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPublishYm() {
        return publishYm;
    }

    public void setPublishYm(String publishYm) {
        this.publishYm = publishYm;
    }
}

