package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MerAdminCreateDto {
    @ApiModelProperty("商户号")
    private String merNo;
    @ApiModelProperty("商户名称")
    private String merName;
    @ApiModelProperty("用户号")
    private String userNo;
    @ApiModelProperty("用户名称")
    private String userName;
    @ApiModelProperty("是否有效 0:禁用 1:启用")
    private Integer validStatus;
    @ApiModelProperty("操作人")
    private String operNo;
}
