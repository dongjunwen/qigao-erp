package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2020-06-2020-06-12 13:46
 * @Description:
 */
@Data
public class WithDrawExcelResultDto {
    @ApiModelProperty("提现订单号")
    private String withdrawOrderNo;
    @ApiModelProperty("申请时间")
    private String applyTime;
    @ApiModelProperty("账号")
    private String accountNo;
    @ApiModelProperty("账号名称")
    private String accountName;
    @ApiModelProperty("账号归属类型")
    private String ownerType;
    @ApiModelProperty("账号归属")
    private String ownerNo;
    @ApiModelProperty("账号归属人")
    private String ownerName;
    @ApiModelProperty("提现金额")
    private BigDecimal withdrawAmt;
    @ApiModelProperty("提现状态")
    private Integer status;
    @ApiModelProperty("提现状态名称")
    private String statusName;
    @ApiModelProperty("审核人")
    private String auditNo;
    @ApiModelProperty("审核时间")
    private String auditTime;
    @ApiModelProperty("审核备注")
    private String auditMemo;
    private Long id;

    private String compNo;
    @ApiModelProperty("所属公司名称")
    private String compName;
}
