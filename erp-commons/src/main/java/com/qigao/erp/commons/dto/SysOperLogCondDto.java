package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: SysOperLogCondDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-08 17:52
 */
@Data
public class SysOperLogCondDto extends PageDto {
    //操作人
    private String operUserNo;
    //操作开始时间     YYYY-MM-DD
    private String operBeginTime;
    //操作结束时间 YYYY-MM-DD
    private String operEndTime;

}
