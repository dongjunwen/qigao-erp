package com.qigao.erp.commons.enums;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @Author:luiz
 * @Date: 2018/5/28 10:14
 * @Descripton: 销售套餐
 * @Modify :
 **/
public enum SaleProductEnum {
    COMMON_SHOP("COMMON_SHOP","通用店铺",15,new BigDecimal(499)),
    PERSON_SHOP("PERSON_SHOP","独立店铺",15,new BigDecimal(499)),
    SELF_SHOP("SELF_SHOP","定制店铺",15,new BigDecimal(499)),
    ;
    private String saleProductCode;
    private String saleProductName;
    private int monthNum;
    private BigDecimal salePrice;


    SaleProductEnum(String saleProductCode, String saleProductName, int monthNum, BigDecimal salePrice) {
        this.saleProductCode = saleProductCode;
        this.saleProductName = saleProductName;
        this.monthNum = monthNum;
        this.salePrice = salePrice;
    }

    public static final SaleProductEnum parse(String saleProductCode){
        for(SaleProductEnum saleProductEnum : SaleProductEnum.values()){
            if(saleProductCode.equals(saleProductEnum.getSaleProductCode())) return saleProductEnum;
        }
        return null;
    }

    public static String getNameByCode(String saleProductCode) {
        SaleProductEnum saleProductEnum = matching(saleProductCode);
        if (saleProductEnum != null) return saleProductEnum.getSaleProductName();
        return "";
    }

    public static final SaleProductEnum matching(String saleProductCode) {
        for (SaleProductEnum saleProductEnum : SaleProductEnum.values()) {
            if (Objects.equals(saleProductEnum.getSaleProductCode(), saleProductCode)) return saleProductEnum;
        }
        return null;
    }

    public String getSaleProductCode() {
        return saleProductCode;
    }

    public void setSaleProductCode(String saleProductCode) {
        this.saleProductCode = saleProductCode;
    }

    public String getSaleProductName() {
        return saleProductName;
    }

    public void setSaleProductName(String saleProductName) {
        this.saleProductName = saleProductName;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public int getMonthNum() {
        return monthNum;
    }

    public void setMonthNum(int monthNum) {
        this.monthNum = monthNum;
    }
}
