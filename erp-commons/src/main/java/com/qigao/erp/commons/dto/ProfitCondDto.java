package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProfitCondDto extends PageDto {
    @ApiModelProperty("开始日期 2020-01-01")
    private String dateBegin;
    @ApiModelProperty("结束日期 2020-01-02")
    private String dateEnd;
    @ApiModelProperty("用户姓名")
    private String userName;
    private String userNo;
}
