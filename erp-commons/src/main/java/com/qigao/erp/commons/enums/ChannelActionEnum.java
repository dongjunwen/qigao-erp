package com.qigao.erp.commons.enums;

/**
@Author:luiz
@Date: 2018/5/25 16:15
@Descripton:渠道层动作
@Modify :
 **/
public enum ChannelActionEnum {
    PRE_LOGIN("PRE_LOGIN","预登录"),
    CHECK_TOKEN("CHECK_TOKEN","TOKEN检查"),
    CREATE_QRCODE("CREATE_QRCODE","QRCODE生成"),
    CREATE_ORDER("CREATE_ORDER","创建订单"),
    ORDER_QEURY("ORDER_QEURY","订单查询"),
    REFUND("REFUND","退款"),
    REFUND_QUERY("REFUND_QUERY","退款查询"),
    MER_ENTER("MER_ENTER","商户进件")
    ;

    private String code;
    private String name;

    ChannelActionEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final ChannelActionEnum parse(String code){
        for(ChannelActionEnum channelActionEnum : ChannelActionEnum.values()){
            if(code.equals(channelActionEnum.getCode())) return channelActionEnum;
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}