package com.qigao.erp.commons.constant;

/**
 * @author luiz
 * @Title: CommonConstants
 * @ProjectName iot-server
 * @Description: TODO
 * @date 2019-05-23 13:41
 */
public class CommonConstants {
    /**
     * 一个小时毫秒数
     */
    public static final long  ONE_HOUR_MILSECONDS=3600*1000;
    /**
     * 一天毫秒数
     */
    public static final long  ONE_DAY_MILSECONDS=ONE_HOUR_MILSECONDS*24;
    /**
     * 7天
     */
    public static final long SEARCH_DAYS=ONE_DAY_MILSECONDS*7;

    /**
     * 默认显示天数
     */
    public static final int DEFAULT_SHOW_DAYS=7;
    /**
     * 默认显示小时数
     */
    public static final int DEFAULT_SHOW_HOURS=1;
    /**
     * 默认排序方式 0:倒序 1:正序
     */
    public static final int DEFAULT_PAGE_ASC=0;
    /**
     * 默认显示条数
     */
    public static final int DEFAULT_PAGE_SIZE=50;
    /**
     * 最大显示条数
     */
    public static final int MAX_PAGE_SIZE=50;

    /**
     * 分润常用码
     */
    public static final String PROFIT_ITEM="PROFIT_ITEM";
}
