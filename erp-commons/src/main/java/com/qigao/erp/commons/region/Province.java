package com.qigao.erp.commons.region;

import java.util.List;

/**
 * 省份
 * @author jx on 2018/4/12.
 */

public class Province {
    private String code;
    private String name;
    /**
     * 公布年月
     */
    private String publishYm;
    private List<City> cityList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }

    public String getPublishYm() {
        return publishYm;
    }

    public void setPublishYm(String publishYm) {
        this.publishYm = publishYm;
    }
}
