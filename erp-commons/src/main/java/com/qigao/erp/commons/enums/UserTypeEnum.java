package com.qigao.erp.commons.enums;

/**
 * @Author:luiz
 * @Date: 2018/5/28 10:14
 * @Descripton:用户类型  0 合作机构:partner 1 客户:customer 2创业者:OWNER 3 业务员: SALER
 * @Modify :
 **/
public enum UserTypeEnum {
    PARTNER(0,"合作机构"),
    CUSTOMER(1,"客户"),
    OWNER(2,"创业者"),
    SALER(3,"业务员"),
    NOMAD(4,"保荐人"),
    SPONSOR(5,"子公司发起人"),
    ;

    private int code;
    private String name;

    UserTypeEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final UserTypeEnum parse(int code){
        for(UserTypeEnum statusEnum : UserTypeEnum.values()){
            if(code==statusEnum.getCode()) return statusEnum;
        }
        return null;
    }

    public static String getNameByCode(Integer userType) {
        if(userType==null)return null;
        UserTypeEnum userTypeEnum=parse(userType);
        if(userTypeEnum!=null)return userTypeEnum.getName();
        return null;
    }

    public static UserTypeEnum parseByName(String codeName) {
        for(UserTypeEnum userTypeEnum : UserTypeEnum.values()){
            if(codeName.equals(userTypeEnum.name())) return userTypeEnum;
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
