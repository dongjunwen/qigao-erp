package com.qigao.erp.commons.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/4 17:06
 * @Description:
 */
@Data
public class ShopCartCreateDto {
    //商品编号
    private String itemNo;
    //商品编号
    private String stockNo;
    //购买数量
    private BigDecimal buyNum;
    //购买人
    private String buylerId;

}
