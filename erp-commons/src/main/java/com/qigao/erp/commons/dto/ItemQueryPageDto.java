package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/12 17:37
 * @Description:
 */
@Data
public class ItemQueryPageDto extends PageDto {
    private String shopNo;
    private String keyWords;
    private String status;
}
