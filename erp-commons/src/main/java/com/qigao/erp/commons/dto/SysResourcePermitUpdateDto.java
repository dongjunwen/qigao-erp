package com.qigao.erp.commons.dto;

import com.qigao.erp.commons.enums.SourceTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-16 14:24
 * @Description:
 */
@Data
@NoArgsConstructor
@ApiModel(value = "资源授权更新操作实体")
public class SysResourcePermitUpdateDto {
    @ApiModelProperty("来源编号")
    @NotNull(message = "来源编号不能为空!")
    private String sourceCode;
    @ApiModelProperty("来源类型")
    @NotNull(message = "来源类型不能为空!")
    private SourceTypeEnum sourceTypeEnum;
    @ApiModelProperty("资源列表")
    private  List<SysResourcePermitDto> sysResourcePermitDtos;
    private String operUserNo;
}
