package com.qigao.erp.commons.po;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.List;

/**
 * @author luiz
 * @Title: UserPermit
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 18:52
 */
@Data
public class UserPermitResult {
    private String resourceNo;

    private String resourceName;

    private String presourceNo;

    private String resourceType;

    private String reqUrl;

    private String routePath;

    private String resourceIcon;

    private Integer sortOrder;

    private Integer resourceLevel;


    private List<UserPermitResult> childPermits;


    private String name;
    private String path;
    private String component;
    private String meta;
    private String redirect;
    private List<UserPermitResult> children;

    public String getName() {
        return this.resourceName;
    }

    public String getPath() {
        return this.routePath;
    }

    public String getComponent() {
        return this.reqUrl;
    }

    public String getMeta() {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("title",this.resourceName);
        jsonObject.put("icon",this.resourceIcon);
        return jsonObject.toJSONString();
    }

    public String getRedirect() {
        return "noredirect";
    }

    public List<UserPermitResult> getChildren() {
        return this.childPermits;
    }
}
