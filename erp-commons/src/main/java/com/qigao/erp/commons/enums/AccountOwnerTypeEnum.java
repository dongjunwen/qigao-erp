package com.qigao.erp.commons.enums;

/**
@Author:luiz
@Date: 2018/5/25 16:15
@Descripton: 账户归属类型
@Modify :
* MER:商家 SHOP:店铺 PER:个人 SALER:销售员
 **/
public enum AccountOwnerTypeEnum {
    MER("MER","商家"),
    SHOP("SHOP","店铺"),
    USER("USER","个人"),
    SALER("SALER","销售员"),
    COMP("COMP","公司"),
    ;
    private String code;
    private String name;


    AccountOwnerTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final AccountOwnerTypeEnum parse(String code){
        for(AccountOwnerTypeEnum webSourceEnum : AccountOwnerTypeEnum.values()){
            if(code.equals(webSourceEnum.getCode())) return webSourceEnum;
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

}