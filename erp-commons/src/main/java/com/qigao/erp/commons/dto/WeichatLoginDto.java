package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: WeichatLoginDto
 * Author:   luiz
 * Date:     2019/11/10 14:36
 * Description: 微信登录Dto
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/10 14:36       版本号              描述
 */
@Data
public class WeichatLoginDto {
    private String weichatCode;
    //商户号
    private String merNo;
    //请求来源
    private String webSource;
}
