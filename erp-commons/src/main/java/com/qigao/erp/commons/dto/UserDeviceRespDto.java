package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: UserDeviceRespDto
 * @ProjectName iot-server
 * @Description: 用户设备状态
 * @date 2019-05-23 10:05
 */
@Data
@ApiModel("用户设备返回信息")
public class UserDeviceRespDto {
    @ApiModelProperty("设备编号")
    private String deviceId;
    @ApiModelProperty("设备物理编号")
    private String iotId;
    @ApiModelProperty("原始设备名称")
    private String deviceName;
    @ApiModelProperty("设备别名")
    private String deviceNickName;
    @ApiModelProperty("设备状态")
    private String deviceStatus;
    @ApiModelProperty("设备状态名称")
    private String deviceStatusName;
    @ApiModelProperty("设备添加时间")
    private String addTime;
    @ApiModelProperty("设备激活时间")
    private String activeTime;
    @ApiModelProperty("设备最后上线时间")
    private String lastOnlineTime;
    @ApiModelProperty("绑定状态")
    private String bindStatus;
    @ApiModelProperty("绑定状态名称")
    private String bindStatusName;
}
