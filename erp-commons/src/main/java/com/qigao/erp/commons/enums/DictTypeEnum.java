package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 字典类型枚举
 * @date 2019-04-03 16:00
 */
public enum DictTypeEnum {
    SELF_SETTING("SELF_SETTING", "自定义设置"),
    SHIPPING_AMT("SHIPPING_AMT", "运费"),
    ORDER_OVER_VAL("ORDER_OVER_VAL", "订单超过多少付邮费"),
    WORK_START_TIME("WORK_START_TIME", "营业开始时间"),
    WORK_END_TIME("WORK_END_TIME", "营业结束时间"),
    BUY_TYPE("BUY_TYPE", "购买方式"),
    RELEASE_PERCENT("RELEASE_PERCENT", "重销优惠比例"),
    ;

    String code;
    String name;

    DictTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final DictTypeEnum matching(String code) {
        for (DictTypeEnum resultCode : DictTypeEnum.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }
}
