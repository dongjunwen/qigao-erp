package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 16:00
 */
public enum SettleStatusEnum {
    UNSETTLE(0, "待结算"),
    SETTLE(1, "已结算"),
    ;

    int code;
    String name;

    SettleStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(int status) {
        SettleStatusEnum statusEnum = matching(status);
        if (statusEnum != null) {
            return statusEnum.getName();
        }
        return "";
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final SettleStatusEnum matching(int code) {
        for (SettleStatusEnum statusEnum : SettleStatusEnum.values()) {
            if (Objects.equals(statusEnum.getCode(), code)) return statusEnum;
        }
        return null;
    }
}
