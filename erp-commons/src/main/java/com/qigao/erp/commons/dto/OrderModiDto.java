package com.qigao.erp.commons.dto;

import com.qigao.erp.commons.enums.OrderActionEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 13:35
 * @Description:
 */
@Data
public class OrderModiDto {
    /**
     * 订单动作
     */
    private OrderActionEnum orderAction;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 订单状态
     */
    private int orderStatus;
    /**
     * 实际订单金额
     */
    private BigDecimal actOrderAmt;

    @NotNull(message = "联系人不能为空!")
    @ApiModelProperty("联系人")
    private String recvName;
    @ApiModelProperty("联系人电话")
    @NotNull(message = "联系人电话不能为空!")
    private String recvPhone;
    @ApiModelProperty("省份")
    @NotNull(message = "省份不能为空!")
    private String recvProvince;
    @ApiModelProperty("城市")
    @NotNull(message = "城市不能为空!")
    private String recvCity;
    @ApiModelProperty("区县")
    @NotNull(message = "区县不能为空!")
    private String recvDistrict;
    @ApiModelProperty("地址")
    @NotNull(message = "地址不能为空!")
    private String recvAddress;
    @ApiModelProperty("邮编")
    @NotNull(message = "邮编不能为空!")
    private String recvZip;
    //购买人
    @ApiModelProperty("购买人")
    private String buylerId;
    //购买人
    @ApiModelProperty("购买人姓名")
    private String buylerName;
    //买家备注
    @ApiModelProperty("买家备注")
    private String userMemo;
    @ApiModelProperty("卖家备注")
    private String merMemo;
    @ApiModelProperty("店铺编号")
    private String shopNo;
    /**
     * 操作人
     */
    private String userNo;
}
