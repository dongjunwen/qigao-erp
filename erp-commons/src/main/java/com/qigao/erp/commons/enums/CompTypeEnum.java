package com.qigao.erp.commons.enums;

import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 公司类型 INTERNAL:内部公司 PARTENER:合作机构
 * @date 2019-04-03 16:00
 */
public enum CompTypeEnum {
    INTERNAL("INTERNAL", "内部公司"),
    PARTENER("PARTENER", "合作机构"),
    ;

    String code;
    String name;

    CompTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String compType) {
        if(StringUtils.isEmpty(compType))return null;
        CompTypeEnum compTypeEnum = matching(compType);
        if (compTypeEnum!=null) return compTypeEnum.getName();
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final CompTypeEnum matching(String code) {
        for (CompTypeEnum resultCode : CompTypeEnum.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }
}
