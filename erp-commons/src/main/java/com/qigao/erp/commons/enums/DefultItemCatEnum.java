package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 默认产品分类
 * @date 2019-04-03 16:00
 *
 */
public enum DefultItemCatEnum {
    BAOKUAN("BAOKUAN", "爆款"),
    REMEN("REMEN", "热门"),
    XINPIN("XINPIN", "新品"),
    ;

    String code;
    String name;

    DefultItemCatEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final DefultItemCatEnum matching(String code) {
        for (DefultItemCatEnum resultCode : DefultItemCatEnum.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }
}
