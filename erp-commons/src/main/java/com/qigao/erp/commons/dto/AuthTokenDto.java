package com.qigao.erp.commons.dto;

import com.qigao.erp.commons.enums.AuthTypeEnum;
import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: WeichatLoginDto
 * Author:   luiz
 * Date:     2019/11/10 14:36
 * Description: 授权认证
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/10 14:36       版本号              描述
 */
@Data
public class AuthTokenDto {
    //授权认证码
    private String authCode;
    //商户号
    private String merNo;
    //请求来源
    private String webSource;
    //授权认证类型
    private AuthTypeEnum authTypeEnum;

}
