package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 登录前缀
 * @date 2019-04-03 16:00
 *
 */
public enum LoginTypeEnum {
    ADMIN_TOKEN("T", "后台管理TOKEN前缀"),
    JSAPI_TOKEN("W", "微信小程序TOKEN前缀"),
   AUTH_TOKEN("A", "授权认证"),
    ;

    String code;
    String name;

    LoginTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final LoginTypeEnum matching(String code) {
        for (LoginTypeEnum resultCode : LoginTypeEnum.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }
}
