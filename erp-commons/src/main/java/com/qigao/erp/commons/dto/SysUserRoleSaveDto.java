package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Date:2017/10/19 0019 15:26
 * @Author lu.dong
 * @Description：
 **/
@Data
@ApiModel(value = "用户角色操作实体")
public class SysUserRoleSaveDto implements Serializable {

    private String operUserNo;
    private String userNo;
    List<SysUserRoleDto> sysUserRoleDtoList;

}
