package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("站内信查询条件")
public class UserMsgCondDto extends PageDto {
    @ApiModelProperty(value = "接受者用户编号", required = false)
    private String recvUserNo;
    @ApiModelProperty(value = "发送者用户编号", required = false)
    private String sendUserNo;
}
