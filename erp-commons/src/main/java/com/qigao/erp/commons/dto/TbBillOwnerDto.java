package com.qigao.erp.commons.dto;

import lombok.Data;

@Data
public class TbBillOwnerDto {
    private String ownerType;
    private String ownerTypeName;
    private String itemCode;
    private String itemName;
    private String operUserNo;


}