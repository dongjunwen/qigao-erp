package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/4 15:56
 * @Description:
 */
@Data
public class ItemSkuDto {
    @ApiModelProperty("库存编号")
    private String stockNo;
    @ApiModelProperty("商品标题")
    private String stockTitle;
    @ApiModelProperty("库存类型 0:不限制库存 1:有库存限制")
    private String stockType;
    @ApiModelProperty("产品图片")
    private String picNo;
    @ApiModelProperty("产品缩略图片")
    private String narrowPicNo;
    /**
     * 商品价格
     */
    @NotNull(message = "商品单价不能为空!")
    @Length(min = 1,max = 16)
    @ApiModelProperty("商品单价")
    private String price;
    @ApiModelProperty("成本价格")
    private String costPrice;
    /**
     * 库存数量
     */
    @ApiModelProperty("库存数量")
    private String stockNum;
    @ApiModelProperty("分润模式 数据字典:PROFIT_TYPE ")
    private String profitType;
    @ApiModelProperty("积分比例")
    private String scorePercent;
}
