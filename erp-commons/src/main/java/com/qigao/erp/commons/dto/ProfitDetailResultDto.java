package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProfitDetailResultDto {

    @ApiModelProperty("结算日期")
    private String settleDate;
    @ApiModelProperty("交易金额")
    private BigDecimal tradeAmt;
    @ApiModelProperty("费用项名称")
    private String itemName;
    @ApiModelProperty("分润模式名称")
    private String itemTypeName;
    @ApiModelProperty("额度")
    private BigDecimal profitPercent;
    @ApiModelProperty("分润金额")
    private BigDecimal profitAmt;
    @ApiModelProperty("姓名")
    private String userName;
    @ApiModelProperty("用户类型名称")
    private String userTypeName;
    @ApiModelProperty("用户级别名称")
    private String userLevelName;
    @ApiModelProperty("公司名称")
    private String compName;
    @ApiModelProperty("用户号")
    private String userNo;
    private Long id;
    @ApiModelProperty("公司编号")
    private String compNo;
    @ApiModelProperty("分润模式")
    private String itemType;
    @ApiModelProperty("用户级别")
    private String userLevel;
    @ApiModelProperty("用户类型")
    private Integer userType;
    @ApiModelProperty("费用项")
    private String itemCode;
    @ApiModelProperty("结算编号")
    private String settleNo;
    private Integer version;
    private String createTime;

    private String createNo;

    private String modiTime;

    private String modiNo;

}