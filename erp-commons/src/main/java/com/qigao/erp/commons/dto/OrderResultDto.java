package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 16:26
 * @Description:
 */
@Data
@ApiModel("订单结果")
public class OrderResultDto {
    //订单号
    @ApiModelProperty("订单号")
    private String orderNo;
    //取单码
    @ApiModelProperty("取单码")
    private String orderRecv;
    //订单日期
    @ApiModelProperty("订单日期")
    private String orderDate;
    //实际支付金额
    @ApiModelProperty("实际支付金额")
    private String actOrderAmt;
    //订单状态
    @ApiModelProperty("订单状态")
    private String orderStatus;
    //订单状态名称
    @ApiModelProperty("订单状态名称")
    private String orderStatusName;
    //第一个商品图片
    @ApiModelProperty("图片地址")
    private String picNo;
    //商品总数
    @ApiModelProperty("商品总数")
    private String itemNum;

    @ApiModelProperty("用户备注")
    private String userMemo;
    //订单类型
    @ApiModelProperty("订单类型")
    private String orderType;
    @ApiModelProperty("订单类型名称")
    private String orderTypeName;
    @ApiModelProperty("订单创建时间")
    private String orderTime;
    @ApiModelProperty("订单支付时间")
    private String payTime;
    @ApiModelProperty("付款类型  0:在线支付 1:线下支付 ")
    private Integer payType;
    @ApiModelProperty("取货方式 0:送货上门 1:自提")
    private Integer recvType;
    @ApiModelProperty("订单金额")
    private BigDecimal orderAmt;
    @ApiModelProperty("优惠金额")
    private BigDecimal coupAmt;
    @ApiModelProperty("抵扣积分")
    private BigDecimal scoreAmt;
    @ApiModelProperty("客户获得积分")
    private BigDecimal customerScore;
    @ApiModelProperty("快递费用")
    private BigDecimal transAmt;
    @ApiModelProperty("应收订单金额=订单金额+运费-优惠金额-抵扣积分")
    private BigDecimal recvOrderAmt;
    @ApiModelProperty("已付金额")
    private BigDecimal payAmt;
    @ApiModelProperty("已退金额")
    private BigDecimal refundAmt;
    @ApiModelProperty("订单描述")
    private String orderDesc;
    @ApiModelProperty("订单取消时间 客户主动关闭")
    private String cancelTime;
    @ApiModelProperty("确认收钱时间 商家完成")
    private String confirmMoneyTime;
    @ApiModelProperty("发货时间 ")
    private String sendGoodsTime;
    @ApiModelProperty("用户确认收货时间 客户主动完成")
    private String confirmGoodsTime;
    @ApiModelProperty("订单过期时间 超过这个时间自动关闭")
    private String overTime;
    @ApiModelProperty("订单自动完成时间 发货后7天自动完成")
    private String autoFinishTime;
    @ApiModelProperty("购买人")
    private String buylerId;
    @ApiModelProperty("购买人姓名")
    private String buylerName;
    @ApiModelProperty("公司编号")
    private String compNo;
    @ApiModelProperty("公司名称")
    private String compName;
    @ApiModelProperty("商家备注")
    private String merMemo;
    @ApiModelProperty("订单商品明细")
    private List<OrderDetailResultDto> orderDetailResultDtoList;
    @ApiModelProperty("收件人信息")
    private OrderShippingResultDto orderShippingResultDto;
}
