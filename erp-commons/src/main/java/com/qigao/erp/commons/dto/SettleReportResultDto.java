package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: OrderQueryVo
 * Author:   luiz
 * Date:     2019/11/18 21:40
 * Description: 订单查询vo
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/18 21:40       版本号              描述
 */
@Data
@ApiModel("结算单报表")
public class SettleReportResultDto {
    @ApiModelProperty("结算月份")
    private String settleMonth;
    @ApiModelProperty("待结算金额")
    private BigDecimal unsettleAmt;
    @ApiModelProperty("实际结算金额")
    private BigDecimal actSettleAmt;
    @ApiModelProperty("结算单明细")
    private List<SettleDetailResultDto> settleDetailResultDtoList;
}
