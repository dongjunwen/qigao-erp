package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @Author:luiz
 * @Date: 2018/5/28 10:14
 * @Descripton:订单状态 0:待付款 1:已付款 2:已发货 3:已收货 4:退款中 5:订单关闭
 * @Modify :
 **/
public enum OrderStatusEnum {
    INIT(0,"待付款"),
    HAS_PAY(1,"已付款"),
    HAS_SEND(2,"已发货"),
    HAS_RECV(3,"已收货"),
    REFUND_PROC(4,"退款中"),
    CLOSE(5,"订单关闭"),
    DELETED(6,"订单删除"),
    ;
    private int code;
    private String name;

    OrderStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final OrderStatusEnum parse(int code){
        for(OrderStatusEnum statusEnum : OrderStatusEnum.values()){
            if(code!=statusEnum.getCode()) return statusEnum;
        }
        return null;
    }

    public static String getNameByCode(int status) {
        OrderStatusEnum statusEnum = matching(status);
        if (statusEnum != null) return statusEnum.getName();
        return "";
    }

    public static final OrderStatusEnum matching(int code) {
        for (OrderStatusEnum orderStatusEnum : OrderStatusEnum.values()) {
            if (Objects.equals(orderStatusEnum.getCode(), code)) return orderStatusEnum;
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
