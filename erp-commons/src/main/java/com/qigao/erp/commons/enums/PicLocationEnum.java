package com.qigao.erp.commons.enums;

/**
@Author:luiz
@Date: 2018/5/25 16:15
@Descripton: 图片位置信息
@Modify :
 **/
public enum PicLocationEnum {
    TOP("TOP","顶部预览部分"),
    DETAIL("DETAIL","底部详情部分"),
    ;
    private String code;
    private String name;


    PicLocationEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final PicLocationEnum parse(String code){
        for(PicLocationEnum webSourceEnum : PicLocationEnum.values()){
            if(code.equals(webSourceEnum.getCode())) return webSourceEnum;
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

}