package com.qigao.erp.commons.dto;

import lombok.Data;

@Data
public class ShopRecentDto {
    private String userNo;

    private String userName;

    private String shopNo;

    private String shopName;


}
