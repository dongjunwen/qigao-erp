package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 登录前缀
 * @date 2019-04-03 16:00
 *
 */
public enum ItemTypeEnum {
    FIX("FIX", "固定金额"),
    PERCENT("PERCENT", "按照比例"),
    ;

    String code;
    String name;

    ItemTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String code) {
        ItemTypeEnum itemTypeEnum=matching(code);
        if(itemTypeEnum!=null)return itemTypeEnum.getName();
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final ItemTypeEnum matching(String code) {
        for (ItemTypeEnum resultCode : ItemTypeEnum.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }
}
