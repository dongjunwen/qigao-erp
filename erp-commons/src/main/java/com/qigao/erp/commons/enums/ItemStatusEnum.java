package com.qigao.erp.commons.enums;

import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 16:00
 */
public enum ItemStatusEnum {
    UNVALID("0", "已下架"),
    VALID("1", "上架中"),
    ;

    String code;
    String name;

    ItemStatusEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String status) {
        if(StringUtils.isEmpty(status))return null;
        ItemStatusEnum statusEnum = matching(status);
        if (statusEnum != null) return statusEnum.getName();
        return "";
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final ItemStatusEnum matching(String code) {
        for (ItemStatusEnum statusEnum : ItemStatusEnum.values()) {
            if (Objects.equals(statusEnum.getCode(), code)) return statusEnum;
        }
        return null;
    }
}
