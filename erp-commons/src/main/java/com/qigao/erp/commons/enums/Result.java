package com.qigao.erp.commons.enums;

import java.io.Serializable;

/**
 * @author luiz
 * @Title: Result
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 15:54
 */
public class Result<T> implements Serializable {

    private static final long serialVersionUID = -6853585621710956250L;
    /**
     * 返回代码
     */
    private String retCode;
    /**
     * 返回消息
     */
    private String retMsg;
    /**
     * 返回数据
     */
    private T data;

    /**
     * 是否成功
     */
    private boolean success;

    public static <T> Result<T> newSuccess() {
        return newSuccess(ResultCode.SUCCESS, null);
    }

    public static <T> Result<T> newSuccess(T data) {
        return newSuccess(ResultCode.SUCCESS, data);
    }

    public static <T> Result<T> newSuccess(ResultCode ResultCode, T data) {
        return new Result<>(ResultCode.getCode(), ResultCode.getMessage(), true, data);
    }

    public static <T> Result<T> newError(ResultCode ResultCode) {
        return new Result<>(ResultCode.getCode(), ResultCode.getMessage(), false, null);
    }

    public static <T> Result<T> newError(String code, String msg) {
        return new Result<>(code, msg, false, null);
    }

    public Result<T> setErrorCode(ResultCode ResultCode) {
        this.success = false;
        this.retCode = ResultCode.getCode();
        this.retMsg = ResultCode.getMessage();
        return this;
    }

    public Result<T> setError(String code, String msg) {
        this.success = false;
        this.retCode = code;
        this.retMsg = msg;
        return this;
    }

    public Result(String code, String message, boolean success, T data) {
        this.retCode = code;
        this.retMsg = message;
        this.data = data;
        this.success = success;
    }

    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String retCode) {
        this.retCode = retCode;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "Result{" +
                "retCode='" + retCode + '\'' +
                ", retMsg='" + retMsg + '\'' +
                ", data=" + data +
                ", success=" + success +
                '}';
    }
}
