package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: UserMsgResultDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-08 11:06
 */
@Data
public class UserMsgResultDto {

    private String pmsgNo;

    private String msgNo;

    private String userNo;

    private String sendUserNo;

    private String sendNickName;

    private String sendHeadUrl;

    private String msgContent;

    private String status;

    private String statusName;

    private String createTime;

    private String modiTime;
}
