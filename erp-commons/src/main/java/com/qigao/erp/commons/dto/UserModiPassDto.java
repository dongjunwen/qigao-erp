package com.qigao.erp.commons.dto;


import lombok.Data;

/**
 * @author luiz
 * @Title: UserRegisterDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-04 10:22
 */
@Data
public class UserModiPassDto {
    //"登录号",required =true )
    private String loginNo;
    //"老密码",required = true)
    private String oldPass;
    //"新密码",required = true)
    private String passwordNew1;
    //"确认新密码",required = true)
    private String passwordNew2;
    //随机数
    private String forgetRadomStr;
}
