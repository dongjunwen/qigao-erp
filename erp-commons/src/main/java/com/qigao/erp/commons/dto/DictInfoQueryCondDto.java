package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: DictInfoQueryCondDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-04 14:09
 */
@Data
public class DictInfoQueryCondDto extends PageDto {
    private String dictCode;

    private String dictName;

    private String dictValue;

    private String pdictCode;

    private String dictValueType;

    private String isValid;

    private String extendParams;

    private String dictDesc;
}
