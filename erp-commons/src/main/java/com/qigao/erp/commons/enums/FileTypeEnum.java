package com.qigao.erp.commons.enums;

import org.apache.commons.lang3.StringUtils;

public enum FileTypeEnum {
    /**
     * JPEG
     */
    JPEG("FFD8FF", "jpg",true,"image/jpeg"),

    /**
     * PNG
     */
    PNG("89504E47", "png",true,"image/png"),

    /**
     * GIF
     */
    GIF("47494638", "gif",true,"image/gif"),

    /**
     * TIF
     */
    TIF("49492A00","TIF",true,"image/tif"),
    /**
     * TIFF
     */
    TIFF("49492A00","TIFF",true,"image/tiff"),

    /**
     * Windows bitmap
     */
    BMP("424D","BMP",true,"application/x-bmp"),

    /**
     * CAD
     */
    DWG("41433130","CAD",false),

    /**
     * Adobe photoshop
     */
    PSD("38425053","PSD",false),

    /**
     * Rich Text Format
     */
    RTF("7B5C727466","RTF",false),

    /**
     * XML
     */
    XML("3C3F786D6C","XML",false),

    /**
     * HTML
     */
    HTML("68746D6C3E","HTML",false),

    /**
     * Outlook Express
     */
    DBX("CFAD12FEC5FD746F ","DBX",false),

    /**
     * Outlook
     */
    PST("2142444E","PST",false),

    /**
     * doc;xls;dot;ppt;xla;ppa;pps;pot;msi;sdw;db
     */
    OLE2("0xD0CF11E0A1B11AE1","OLE2",false),

    /**
     * Microsoft Word/Excel
     */
    XLS_DOC("D0CF11E0","XLS_DOC",false),

    /**
     * Microsoft Access
     */
    MDB("5374616E64617264204A","MDB",false),

    /**
     * Word Perfect
     */
    WPB("FF575043","WPB",false),

    /**
     * Postscript
     */
    EPS_PS("252150532D41646F6265","WPB",false),

    /**
     * Adobe Acrobat
     */
    PDF("255044462D312E","PDF",false),

    /**
     * Windows Password
     */
    PWL("E3828596","PWL",false),

    /**
     * ZIP Archive
     */
    ZIP("504B0304","ZIP",false),

    /**
     * ARAR Archive
     */
    RAR("52617221","RAR",false),

    /**
     * WAVE
     */
    WAV("57415645","WAV",false),

    /**
     * AVI
     */
    AVI("41564920","AVI",false),

    /**
     * Real Audio
     */
    RAM("2E7261FD","RAM",false),

    /**
     * Real Media
     */
    RM("2E524D46","RM",false),

    /**
     * Quicktime
     */
    MOV("6D6F6F76","MOV",false),

    /**
     * Windows Media
     */
    ASF("3026B2758E66CF11","ASF",false),

    /**
     * MIDI
     */
    MID("4D546864","MID",false);

    private String value = "";
    private String ext = "";
    private boolean isImage=false;
    private String contentType="multipart/form-data";

    FileTypeEnum(String value) {
        this.value = value;
    }

    FileTypeEnum(String value, String ext) {
        this(value);
        this.ext = ext;
    }

    FileTypeEnum(String value, String ext, boolean isImage) {
        this.value = value;
        this.ext = ext;
        this.isImage = isImage;
    }

    FileTypeEnum(String value, String ext, boolean isImage, String contentType) {
        this.value = value;
        this.ext = ext;
        this.isImage = isImage;
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }

    public String getExt() {
        return ext;
    }

    public String getValue() {
        return value;
    }

    public boolean isImage() {
        return isImage;
    }

    public static String getContentTypeByType(String fileType){
        if(StringUtils.isEmpty(fileType))return "";
        for(FileTypeEnum fileTypeEnum:FileTypeEnum.values()) {
            if(fileTypeEnum.getExt().toUpperCase().equals(fileType.toUpperCase()))return fileTypeEnum.getContentType();
        }
        return "";
    }

    public static boolean ifImage(String fileType){
        if(StringUtils.isEmpty(fileType))return false;
        for(FileTypeEnum fileTypeEnum:FileTypeEnum.values()) {
            if(fileTypeEnum.getExt().toUpperCase().equals(fileType.toUpperCase()))return fileTypeEnum.isImage();
        }
        return false;
    }
}
