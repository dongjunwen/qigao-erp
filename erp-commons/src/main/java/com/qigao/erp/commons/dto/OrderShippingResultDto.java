package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/26 9:55
 * @Description:
 */
@ApiModel("收件人信息")
@Data
public class OrderShippingResultDto {
    @ApiModelProperty("订单号")
    private String orderNo;
    @ApiModelProperty("物流公司")
    private String shippingWay;
    @ApiModelProperty("物流单号")
    private String shippingNo;
    @ApiModelProperty("购买人")
    private String recvUserNo;
    @ApiModelProperty("联系人")
    private String recvName;
    @ApiModelProperty("联系人电话")
    private String recvPhone;
    @ApiModelProperty("购买人的推荐人")
    private String recvInviteUserNo;
    @ApiModelProperty("购买人的推荐人姓名")
    private String recvInviteUserName;
    @ApiModelProperty("省份代码")
    private String provinceCode;
    @ApiModelProperty("原始省份代码")
    private String recvProvinceCode;
    @ApiModelProperty("省份")
    private String recvProvince;
    @ApiModelProperty("城市代码")
    private String cityCode;
    @ApiModelProperty("原始城市代码")
    private String recvCityCode;
    @ApiModelProperty("城市")
    private String recvCity;
    @ApiModelProperty("区县代码")
    private String districtCode;
    @ApiModelProperty("原始区县代码")
    private String recvDistrictCode;
    @ApiModelProperty("区县")
    private String recvDistrict;
    @ApiModelProperty("地址")
    private String recvAddress;
    @ApiModelProperty("邮编")
    private String recvZip;

}
