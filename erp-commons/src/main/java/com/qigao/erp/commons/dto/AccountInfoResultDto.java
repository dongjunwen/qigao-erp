package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountInfoResultDto {

    @ApiModelProperty("账户名称")
    private String accountName;
    @ApiModelProperty("金额")
    private BigDecimal accountAmt;
    @ApiModelProperty("所属公司")
    private String compName;

    @ApiModelProperty("账号")
    private String accountNo;
    @ApiModelProperty("账户类型")
    private String accountType;
    @ApiModelProperty("账户类型名称")
    private String accountTypeName;
    private String ownerType;
    @ApiModelProperty("用户号")
    private String ownerNo;
    @ApiModelProperty("姓名")
    private String ownerName;
    private String compNo;
    private Long id;
    private Integer version;
    private String createTime;
    private String createNo;
    private String modiTime;
    private String modiNo;

}
