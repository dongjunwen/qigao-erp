package com.qigao.erp.commons.dto;

import lombok.Data;

@Data
public class ShopEmpCreateDto {

    private String shopNo;

    private String shopName;

    private String userNo;

    private String userName;

    private Integer validStatus;

    private String operNo;
}
