package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 11:31
 * @Description:
 */
@Data
public class ItemPicDto {
    @ApiModelProperty("图片编号")
    private Integer id;
    @ApiModelProperty("图片所属 TOP:顶部预览 DETAIL:底部详情")
    private String picLocation;
    @ApiModelProperty("缩略图片编号")
    private String narrowPicNo;
    @ApiModelProperty("图片编号")
    private String picNo;
    @NotNull(message = "图片标题不能为空!")
    @Length(min = 1,max = 128)
    private String picTitle;
    @ApiModelProperty("设置为主图 0:否 1:是")
    private Integer isMain;
}
