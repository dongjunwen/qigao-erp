package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 输出格式
 * @date 2019-04-03 16:00
 */
public enum StreamFormatEnum {
    STREAM("STREAM", "数据流格式"),
    JSON("JSON", "JSON格式"),
    ;

    String code;
    String name;

    StreamFormatEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String status) {
        StreamFormatEnum statusEnum = matching(status);
        if (statusEnum != null) {
            return statusEnum.getName();
        }
        return "";
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final StreamFormatEnum matching(String code) {
        for (StreamFormatEnum statusEnum : StreamFormatEnum.values()) {
            if (Objects.equals(statusEnum.getCode(), code)) return statusEnum;
        }
        return null;
    }
}
