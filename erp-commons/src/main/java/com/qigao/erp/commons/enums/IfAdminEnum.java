package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 16:00
 */
public enum IfAdminEnum {
    NO(0, "否"),
    YES(1, "是"),
    ;

    int code;
    String name;

    IfAdminEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(int status) {
        IfAdminEnum statusEnum = matching(status);
        if (statusEnum != null) return statusEnum.getName();
        return "";
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final IfAdminEnum matching(int code) {
        for (IfAdminEnum statusEnum : IfAdminEnum.values()) {
            if (Objects.equals(statusEnum.getCode(), code)) return statusEnum;
        }
        return null;
    }
}
