package com.qigao.erp.commons.dto;

import lombok.Data;

@Data
public class MerAdminDto {
    private Long id;

    private String merNo;

    private String merName;

    private String userNo;

    private String userName;

    private Integer validStatus;

    private String validStatusName;

    private String createTime;

    private String createNo;

    private String modiTime;

    private String modiNo;
}
