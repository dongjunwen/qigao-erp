package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/1 14:07
 * @Description:
 */
@Data
@ApiModel("商品结果")
public class ItemResultDto {

    /**
     * 商品编号
     */
    @ApiModelProperty("商品编号")
    private String itemNo;
    /**
     * 标题
     */
    @ApiModelProperty("商品标题")
    private String itemTitle;
    @ApiModelProperty("商品子标题")
    private String subTitle;
    @ApiModelProperty("一级分类")
    private String catNo1;
    @ApiModelProperty("一级分类名称")
    private String catName1;
    @ApiModelProperty("状态 1:有效 0:无效")
    private String status;
    @ApiModelProperty("状态 1:有效 0:无效")
    private String statusName;
    @ApiModelProperty("商品描述")
    private String itemDes;
    private Integer id;


    /**
     * 商品库存列表
     */
    @ApiModelProperty("商品库存列表")
    private List<ItemSkuResultDto> itemSkuResultDtos;
    /**
     * 顶部预览商品图片列表
     */
    @ApiModelProperty("顶部预览商品图片列表")
    private List<ItemPicResultDto> itemPicTopResultDtoList;
    /**
     * 底部详情商品图片列表
     */
    @ApiModelProperty("底部详情商品图片列表")
    private List<ItemPicResultDto> itemPicDetailResultDtoList;

    @ApiModelProperty("二级分类")
    private String catNo2;
    @ApiModelProperty("二级分类名称")
    private String catName2;
    @ApiModelProperty("三级分类")
    private String catNo3;
    @ApiModelProperty("三级分类名称")
    private String catName3;
    @ApiModelProperty("品牌编号")
    private Integer brandId;
    @ApiModelProperty("商户号")
    private String merNo;
    @ApiModelProperty("商户名称")
    private String merName;
    @ApiModelProperty("店铺号")
    private String shopNo;
    @ApiModelProperty("店铺名称")
    private String shopName;

    private String specifications;

    private String specTemplate;

    private String packingList;

    private String afterService;


    private String stockType;
    @ApiModelProperty("成本价")
    private BigDecimal cost;
    @ApiModelProperty("销售价")
    private BigDecimal price;
    @ApiModelProperty("库存数量")
    private BigDecimal stockNum;

    private String picUrl;

    private String narrowPicUrl;

}
