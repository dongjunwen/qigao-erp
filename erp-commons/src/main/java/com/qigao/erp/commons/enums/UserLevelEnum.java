package com.qigao.erp.commons.enums;

import org.springframework.util.StringUtils;

/**
 * @Author:luiz
 * @Date: 2018/5/28 10:14
 * @Descripton:
 * @Modify : 用户级别
 **/
public enum UserLevelEnum {
    A("A","A类级别"),
    B("B","B类级别"),
    C("C","C类级别"),
    D("D","D类级别"),
    ZERO("0","未转正"),
    ONE("1","已转正");;

    private String code;
    private String name;

    UserLevelEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final UserLevelEnum parse(String code){
        for(UserLevelEnum statusEnum : UserLevelEnum.values()){
            if(statusEnum.getCode().equals(code)) return statusEnum;
        }
        return null;
    }

    public static String getNameByCode(String userLevel) {
        if(StringUtils.isEmpty(userLevel))return null;
        UserLevelEnum  userLevelEnum=parse(userLevel);
        if(userLevelEnum!=null)return userLevelEnum.getName();
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
