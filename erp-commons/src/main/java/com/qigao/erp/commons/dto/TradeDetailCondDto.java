package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName drp-parent
 * @date 2020-07-2020-07-10 16:40
 * @Description:
 */
@Data
public class TradeDetailCondDto extends PageDto {
    @ApiModelProperty("订单开始日期 YYYY-MM-DD")
    private String dateBegin;
    @ApiModelProperty("订单结束日期 YYYY-MM-DD")
    private String dateEnd;
}
