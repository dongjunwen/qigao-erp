package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName drp-parent
 * @date 2020-07-2020-07-10 15:46
 * @Description:
 */
@Data
public class TradeStaticCondDto extends PageDto {
    private Date orderDate;
    @ApiModelProperty("订单开始日期 YYYY-MM-DD")
    private String dateBegin;
    @ApiModelProperty("订单结束日期 YYYY-MM-DD")
    private String dateEnd;
}
