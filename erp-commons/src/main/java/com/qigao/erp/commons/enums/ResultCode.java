package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 16:00
 */
public enum ResultCode {
    COMMON_PARAM_NULL("1001", "参数为空"),
    COMMON_PARAM_INVALID("1002", "参数不合法"),
    COMMON_DATA_EXISTS("1003", "数据已存在"),
    COMMON_DATA_NOT_EXISTS("1004", "数据不存在"),
    COMMON_ORDER_SOURCE_ERROR("1005", "数据来源方不存在"),
    COMMON_QUERY_ERROR("1006", "数据执行SQL查询错误"),
    COMMON_UNVALID_URL("1007", "非法链接"),
    SAVE_DB_ERROR("1008", "数据库存储失败"),
    DB_VERSION_UPDATE_ERROR("1009", "系统繁忙，稍后再试.."),
    SIGN_VALID_ERROR("1010", "签名校验错误"),
    OPER_NOT_ALlOWED("1011", "非法操作"),
    COND_NOT_ALLOWED("1012", "条件不足,不允许操作，请联系管理员!"),

    //基础资料 1100-1199
    USER_PASS_NOT_EQUAL("1100", "两次密码不一致"),
    USER_OLD_PASS_ERROR("1101", "原始密码错误"),
    USER_NOT_EXISTS("1102", "用户不存在"),
    PASS_NOT_STRONG("1103", "密码只能输入字母、数字、特殊符号"),
    USER_HAS_LOCK("1104", "账户被锁定,禁止登录"),
    PASS_OVERDUE("1105", "密码已过期,需要修改"),
    PSS_NEED_MODIFY("1106", "密码需要修改"),
    USER_REGISTER_ERROR("1107", "用户注册异常"),
    USER_HAS_EXISTS("1108", "账号已存在"),
    PASS_CANNOT_CONTAINS_ACCOUNT("1109", "密码不能包含账号"),

    // 操作 1200
    CHANNEL_CONFIG_NOT_EXITS("1200", "渠道配置信息不存在"),
    ITEM_NOT_EXITS("1201", "商品信息不存在,请联系客服添加"),
    STOCK_NOT_EXITS("1202", "库存信息不存在,请联系客服添加"),
    BANKCARD_NOT_SUPPORT("1203", "银行卡不支持"),
    SHOP_NOT_EXITS("1204", "店铺信息不存在"),
    SHOP_HAS_EXITS("1205", "店铺信息已存在"),
    SHOP_QRCODE_FAIL("1206", "获取店铺二维码信息失败"),

    // 提现 2000-2100
    ACCOUNT_AMT_ERROR("2000", "余额不足"),
    ACCOUNT_NOT_EXISTS("2001", "账户不存在"),
    ACCOUNT_SCORE_NOT_ENOUGH("2002", "账户积分不足"),

    SUCCESS("200", "操作成功"),
    FAIL("500", "系统开小差了,请联系系统管理员"),
    CHANNEL_ERROR("501", "渠道异常"),
    USER_NO_LOGGED_IN("510", "用户未登录"),
    FOR_UNAUTHORIZED("511", "用户未授权"),
    USERNAME_OR_PASS_ERR("512", "用户名或密码错误"),
    PRE_LOGIN_ERROR("513", "预登录失败");

    String code;
    String message;

    ResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static final ResultCode matching(String code) {
        for (ResultCode resultCode : ResultCode.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }
}
