package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 费用项
 * @date 2019-04-03 16:00
 * 	销售额	原始的单价*销售数量，不需要扣除优惠的金额
 * 	一级、二级机构	机构都是按照级别来的、机构推荐机构算一级；顶级一定属于某个个人（创业者或业务员）
 * 	一层、二层	非机构的都是按照层级来的；顶级属于某个个人
 * 创业者	推荐一层产品1非重销原始购买额	我所推荐的购买产品1的原始商品金额（去掉重销），不是打折后的金额
 * 创业者	推荐一级A类机构产品2销售额	同上，加个A类、产品2
 * 创业者	推荐一级B类机构产品2销售额	同上，加个B类、产品2
 * 创业者	二级及以下所有机构产品2销售额
 * 创业者	推荐一层A类非机构产品1重销金额	推荐的一层的A类创业者的重销类型的产品1的所有销售额
 * 创业者	推荐一层B类非机构产品1重销金额	推荐的一层的B类创业者的重销类型的产品1的所有销售额
 * 创业者	推荐一层C类非机构产品1重销金额	推荐的一层的C类创业者的重销类型的产品1的所有销售额
 * 业务员	推荐的一级A类机构产品2销售额
 * 业务员	推荐的一级B类机构产品2销售额
 * 业务员	二级及以下A类机构产品2销售额	包含二级
 * 业务员	二级及以下B类机构产品2销售额	包含二级
 * 业务员	推荐的一层客户产品2直接购买额	我所推荐的个人购买产品2的原始商品金额，不是打折后的金额
 * 保荐人、子公司发起人	整个子公司产品1销售额	挂靠在这家公司下产品1的所有销售额
 * 保荐人、子公司发起人	整个子公司产品2销售额	挂靠在这家公司下产品2的所有销售额
 * 合作机构	推荐一级机构产品2销售额	直接介绍的机构，挂靠在所属公司下的产品2的销售额度
 *
 */
public enum BillItemEnum {
    //创业者
    ONE_INVITE_PROD1_NOTRELEASE("ONE_INVITE_PROD1_NOTRELEASE","推荐一层个人产品1非重销原始购买额",BillItemTypeEnum.INVITE_LAYER_TYPE_PROD),
    ONE_INVITE_PARTENERA_PROD2_AMT("ONE_INVITE_PARTENERA_PROD2_AMT", "推荐一级A类机构产品2销售额",BillItemTypeEnum.INVITE_LAYER_TYPE_PROD),
    ONE_INVITE_PARTENERB_PROD2_AMT("ONE_INVITE_PARTENERB_PROD2_AMT", "推荐一级B类机构产品2销售额",BillItemTypeEnum.INVITE_LAYER_TYPE_PROD),
    TWO_PARTENER_PROD2_AMT("TWO_PARTENER_PROD2_AMT", "二级及以下所有机构产品2销售额",BillItemTypeEnum.TOP_INVITE_TYPE),
    ONE_INVITE_USERA_PROD1_RELEASE_AMT("ONE_INVITE_USERA_PROD1_RELEASE_AMT", "推荐一层A类非机构产品1重销金额",BillItemTypeEnum.INVITE_LAYER_TYPE_LEVEL_ORDERTYPE_PROD),
    ONE_INVITE_USERB_PROD1_RELEASE_AMT("ONE_INVITE_USERB_PROD1_RELEASE_AMT", "推荐一层B类非机构产品1重销金额",BillItemTypeEnum.INVITE_LAYER_TYPE_LEVEL_ORDERTYPE_PROD),
    ONE_INVITE_USERC_PROD1_RELEASE_AMT("ONE_INVITE_USERC_PROD1_RELEASE_AMT", "推荐一层C类非机构产品1重销金额",BillItemTypeEnum.INVITE_LAYER_TYPE_LEVEL_ORDERTYPE_PROD),
    //业务员
   // ONE_INVITE_PARTENERA_PROD2_AMT("ONE_INVITE_PARTENERA_PROD2_AMT", "推荐的一级A类机构产品2销售额",BillItemTypeEnum.INVITE_LAYER_TYPE_PROD),
    //ONE_INVITE_PARTENERB_PROD2_AMT("ONE_INVITE_PARTENERB_PROD2_AMT", "推荐的一级B类机构产品2销售额",BillItemTypeEnum.INVITE_LAYER_TYPE_PROD),
    TWO_INVITE_PARTENERA_PROD2_AMT("TWO_INVITE_PARTENERA_PROD2_AMT", "二级及以下A类机构产品2销售额",BillItemTypeEnum.TOP_INVITE_TYPE_LEVEL_PROD),
    TWO_INVITE_PARTENERB_PROD2_AMT("TWO_INVITE_PARTENERB_PROD2_AMT", "二级及以下B类机构产品2销售额",BillItemTypeEnum.TOP_INVITE_TYPE_LEVEL_PROD),
    ONE_INVITE_CUSTOMER_PROD2_AMT("ONE_INVITE_CUSTOMER_PROD2_AMT","推荐的一层客户产品2直接购买额",BillItemTypeEnum.INVITE_LAYER_TYPE_PROD),
    //保荐人、子公司发起人
    SUBCOM_PROD1_AMT("SUBCOM_PROD1_AMT", "整个子公司产品1销售额",BillItemTypeEnum.COMP_PROD),
    SUBCOM_PROD2_AMT("SUBCOM_PROD2_AMT", "整个子公司产品2销售额",BillItemTypeEnum.COMP_PROD),
    //机构
    ONE_INVITE_PARTENER_PROD2_AMT("ONE_INVITE_PARTENER_PROD2_AMT", "推荐一级机构产品2销售额",BillItemTypeEnum.INVITE_LAYER_TYPE_PROD),
    ;

    String code;
    String name;
    BillItemTypeEnum billItemTypeEnum;

    BillItemEnum(String code, String name, BillItemTypeEnum billItemTypeEnum) {
        this.code = code;
        this.name = name;
        this.billItemTypeEnum = billItemTypeEnum;
    }

    public static String getNameByCode(String itemCode) {
        BillItemEnum billItemEnum=matching(itemCode);
        if(billItemEnum!=null)return billItemEnum.getName();
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public BillItemTypeEnum getBillItemTypeEnum() {
        return billItemTypeEnum;
    }

    public static final BillItemEnum matching(String code) {
        for (BillItemEnum resultCode : BillItemEnum.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }


   
}
