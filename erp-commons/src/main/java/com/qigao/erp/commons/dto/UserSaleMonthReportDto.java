package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 16:26
 * @Description:
 */
@Data
@ApiModel("按照用户查询订单汇总")
public class UserSaleMonthReportDto {
    @ApiModelProperty("用户编号")
    private String userNo;
    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("订单总额")
    private String orderAmt;
    @ApiModelProperty("订单总额")
    private String orderNum;
}
