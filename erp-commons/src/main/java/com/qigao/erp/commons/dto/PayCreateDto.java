package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/8 17:15
 * @Description:
 */
@Data
public class PayCreateDto {
    @ApiModelProperty("订单号")
    @NotNull(message = "订单号不能为空!")
    private String orderNo;
    @ApiModelProperty("应付费用")
    private String payAmt;
    @ApiModelProperty("系统来源")
    private String webSource;
    @ApiModelProperty("支付方式")
    private String payWay;
    //付款人编号
    private String payUserNo;
    //付款人OpenId
    private String openId;
    //请求终端IP
    private String reqTermIp;
    //异步通知地址
    private String payNotifyUrl;
}
