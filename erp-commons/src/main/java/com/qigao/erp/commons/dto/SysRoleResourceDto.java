package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Date:2017/12/25 0025 14:46
 * @Author lu.dong
 * @Description：
 **/
@Data
@ApiModel(value = "角色操作实体 SysRoleResourceDto")
public class SysRoleResourceDto {
    @ApiModelProperty(value = "角色代码", required = true)
    private String roleCode;
    @ApiModelProperty(value = "资源代码", required = true)
    private String reSourceNo;
    private String userNo;
}
