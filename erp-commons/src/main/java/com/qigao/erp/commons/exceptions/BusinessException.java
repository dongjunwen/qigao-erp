package com.qigao.erp.commons.exceptions;

import com.qigao.erp.commons.enums.ResultCode;

/**
 * @author luiz
 * @Title: BusinessException
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-04 10:17
 */
public class BusinessException extends RuntimeException {
    private String code;
    private String msg;

    public BusinessException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public BusinessException(String code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    public BusinessException(ResultCode resultCode) {
        super(resultCode.getMessage());
        this.code = resultCode.getCode();
        this.msg = resultCode.getMessage();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
