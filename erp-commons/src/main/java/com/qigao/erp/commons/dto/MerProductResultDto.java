package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/8 16:57
 * @Description:
 */
@Data
public class MerProductResultDto {

    private String merNo;

    private String merName;

    private String webSource;

    private String webSourceName;

    private String payWay;

    private String payWayName;

}
