package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: SysResourceCreateDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-08 17:59
 */
@Data
public class SysResourceModiDto extends SysResourceCreateDto {
    private Integer id;
}
