package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.admin.merchant
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-18 15:24
 * @Description:
 */
@Data
@ApiModel("费用项查询条件")
public class TbBillItemCondDto extends PageDto {
}
