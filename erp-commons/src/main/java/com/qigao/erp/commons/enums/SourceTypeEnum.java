package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 来源类型
 * @date 2019-04-03 16:00
 */
public enum SourceTypeEnum {
    USER("USER", "用户"),
    ROLE("ROLE", "角色"),
    ;

    String code;
    String name;

    SourceTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final SourceTypeEnum matching(String code) {
        for (SourceTypeEnum resultCode : SourceTypeEnum.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }
}
