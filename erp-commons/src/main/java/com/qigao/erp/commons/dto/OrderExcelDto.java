package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 16:26
 * @Description:
 */
@Data
@ApiModel("订单结果")
public class OrderExcelDto {
    //订单号
    @ApiModelProperty("订单号")
    private String orderNo;
    //订单日期
   // @ApiModelProperty("订单日期")
   // private String orderDate;
    @ApiModelProperty("确认收钱时间 商家完成")
    private String confirmMoneyTime;
    @ApiModelProperty("客户名称")
    private String customerName;
    @ApiModelProperty("客户手机号")
    private String recvPhone;
    @ApiModelProperty("客户获得积分")
    private BigDecimal customerScore;
    @ApiModelProperty("订单数量")
    private BigDecimal orderNum;
    @ApiModelProperty("订单金额")
    private BigDecimal orderAmt;
    @ApiModelProperty("优惠金额")
    private BigDecimal coupAmt;
    @ApiModelProperty("抵扣积分")
    private BigDecimal scoreAmt;
    @ApiModelProperty("应收订单金额=订单金额+运费-优惠金额-抵扣积分")
    private BigDecimal recvOrderAmt;
    @ApiModelProperty("购买人姓名")
    private String buylerName;
    @ApiModelProperty("公司名称")
    private String compName;
    @ApiModelProperty("订单状态名称")
    private String orderStatusName;
    private String orderTypeName;
}
