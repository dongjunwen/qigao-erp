package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 16:00
 */
public enum YesNoEnum {
    YES("Y", "无效"),
    NO("N", "有效"),
    ;

    String code;
    String name;

    YesNoEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String status) {
        YesNoEnum statusEnum = matching(status);
        if (statusEnum != null) {
            return statusEnum.getName();
        }
        return "";
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final YesNoEnum matching(String code) {
        for (YesNoEnum statusEnum : YesNoEnum.values()) {
            if (Objects.equals(statusEnum.getCode(), code)) return statusEnum;
        }
        return null;
    }
}
