package com.qigao.erp.commons.po;

import com.qigao.erp.commons.enums.IfAdminEnum;
import lombok.Data;

@Data
public class MerShopInfo {
    /**
     * 商户号
     */
    private String merNo;
    /**
     * 商户名称
     */
    private String merName;
    /**
     * 是否是商户管理员
     */
    private IfAdminEnum ifMerAdmin;
    /**
     * 店铺号
     */
    private String shopNo;
    /**
     * 店铺名称
     */
    private String shopName;

}
