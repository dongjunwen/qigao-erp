package com.qigao.erp.commons.dto;

import com.qigao.erp.commons.enums.OrderTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 13:35
 * @Description:
 */
@Data
public class OrderCreateDto {
    @ApiModelProperty("付款类型 0:在线支付 1:线下支付 ")
    private String payType="0";
   @ApiModelProperty("取货方式 0:送货上门 1:自提 ")
    /* @NotNull(message = "付款类型不能为空!")
    @Min(value = 0,message = "付款类型最小值为0")
    @Max(value = 1,message = "付款类型最大值为1")*/
    private String recvType="0";
    @ApiModelProperty("订单来源 0:单个商品 1:购物车 默认为0")
    private String orderSource="0";//是否来自购物车
    @ApiModelProperty("订单类型")
    private String orderType= OrderTypeEnum.NORMAL.getCode();//订单类型
    @ApiModelProperty("联系人")
    private String recvUserNo;
    @NotNull(message = "联系人不能为空!")
    @ApiModelProperty("联系人")
    private String recvName;
    @ApiModelProperty("联系人电话")
    @NotNull(message = "联系人电话不能为空!")
    private String recvPhone;
    @ApiModelProperty("省份代码")
    private String provinceCode;
    @ApiModelProperty("省份")
    @NotNull(message = "省份不能为空!")
    private String recvProvince;
    @ApiModelProperty("城市代码")
    private String cityCode;
    @ApiModelProperty("城市")
    @NotNull(message = "城市不能为空!")
    private String recvCity;
    @ApiModelProperty("区县代码")
    private String districtCode;
    @ApiModelProperty("区县")
    @NotNull(message = "区县不能为空!")
    private String recvDistrict;
    @ApiModelProperty("地址")
    @NotNull(message = "地址不能为空!")
    private String recvAddress;
    @ApiModelProperty("邮编")
    @NotNull(message = "邮编不能为空!")
    private String recvZip="0000";
    @ApiModelProperty("订单金额")
    private BigDecimal orderAmt;
    @ApiModelProperty("成本金额")
    private BigDecimal costAmt;
    @ApiModelProperty("优惠金额")
    private BigDecimal coupAmt;
    @ApiModelProperty("抵扣积分")
    private BigDecimal scoreAmt;
    @ApiModelProperty("物流公司")
    private String shippingWay;
    @ApiModelProperty("物流单号")
    private String shippingNo;
    @ApiModelProperty("运费")
    private String shippingAmt="0.00";
    //购买人
    @ApiModelProperty("购买人")
    private String buylerId;
    //购买人
    @ApiModelProperty("购买人姓名")
    private String buylerName;
    //买家备注
    @ApiModelProperty("买家备注")
    private String buylerMemo;
    @ApiModelProperty("店铺编号")
    private String shopNo;
    @ApiModelProperty("店铺名称")
    private String shopName;

    @ApiModelProperty("获得积分")
    @DecimalMin(value = "0.00",message = "获得积分最小数字为0.00")
    private BigDecimal customerScore;
    //商户编号
    private String merNo;
    //商户名称
    private String merName;
    //平台名称
    private String sysName;
    //订单明细列表
    private List<OrderCreateDetailDto> orderCreateDetailDtoList;
}
