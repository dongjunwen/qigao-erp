package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.utils
 * @ProjectName three-mall
 * @date 2019-11-2019/11/14 11:26
 * @Description:
 */
@Data
public class AreaResultDto {
    //区域级别
    private String areaLevel;
    //区域代码
    private String areaCode;
    //区域名称
    private String areaName;
}
