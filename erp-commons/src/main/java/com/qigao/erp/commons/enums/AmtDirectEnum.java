package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 16:00
 */
public enum AmtDirectEnum {
    ADD("+", "增加"),
    SUB("-", "减少"),
    ;

    String code;
    String name;

    AmtDirectEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String status) {
        AmtDirectEnum statusEnum = matching(status);
        if (statusEnum != null) {
            return statusEnum.getName();
        }
        return "";
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final AmtDirectEnum matching(String code) {
        for (AmtDirectEnum statusEnum : AmtDirectEnum.values()) {
            if (Objects.equals(statusEnum.getCode(), code)) return statusEnum;
        }
        return null;
    }
}
