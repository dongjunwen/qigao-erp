package com.qigao.erp.commons.enums;

/**
@Author:luiz
@Date: 2018/5/25 16:15
@Descripton: 授权认证类型
@Modify :
 **/
public enum AuthTypeEnum {
    ALI_AUTH("ALI_AUTH","支付宝授权", PayProductEnum.ALIPAY_WAP_WEB),
    WEICHAT_AUTH("WEICHAT_AUTH","微信授权", PayProductEnum.WEICHAT_H5_LOGIN),
    ;
    private String code;
    private String name;
    private PayProductEnum payProductEnum;


    AuthTypeEnum(String code, String name, PayProductEnum payProductEnum) {
        this.code = code;
        this.name = name;
        this.payProductEnum = payProductEnum;
    }

    public static final AuthTypeEnum parse(String code){
        for(AuthTypeEnum webSourceEnum : AuthTypeEnum.values()){
            if(code.equals(webSourceEnum.getCode())) return webSourceEnum;
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public PayProductEnum getPayProductEnum() {
        return payProductEnum;
    }
}