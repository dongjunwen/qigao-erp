package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/19 14:27
 * @Description:
 */
@Data
public class FileResultDto {
    /**
     * 文件编号
     */
    private String fileNo;
    /**
     * 缩略文件编号
     */
    private String narrowFileNo;
    /**
     * 压缩文件编号
     */
    private String zipFileNo;

    /**
     * 文件地址
     */
    private String fileUrl;
    /**
     * 缩略文件地址
     */
    private String narrowFileUrl;

    private String zipFileUrl;

}
