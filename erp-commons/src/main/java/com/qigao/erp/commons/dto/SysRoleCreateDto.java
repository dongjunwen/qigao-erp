package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: SysRoleCreateDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-09 9:38
 */
@Data
public class SysRoleCreateDto {
    @ApiModelProperty(value = "角色代码", required = true)
    private String roleCode;
    @ApiModelProperty(value = "角色名称", required = true)
    private String roleName;
    @ApiModelProperty(value = "状态", required = false)
    private String status;
    @ApiModelProperty(value = "是否超级角色", required = false)
    private String ifAdmin;
    @ApiModelProperty(value = "备注", required = false)
    private String memo;
    private String createUserNo;
}
