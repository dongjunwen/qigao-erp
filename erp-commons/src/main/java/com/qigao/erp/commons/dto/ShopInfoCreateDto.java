package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class ShopInfoCreateDto {
    private Long id;
    private String merNo;

    private String merName;

    private String shopNo;

    private String shopName;

    @ApiModelProperty("省份代码")
    private String provinceCode;
    @ApiModelProperty("省份名称")
    private String recvProvince;
    @ApiModelProperty("城市代码")
    private String cityCode;
    @ApiModelProperty("城市名称")
    private String recvCity;
    @ApiModelProperty("区、县代码")
    private String districtCode;
    @ApiModelProperty("区、县名称")
    private String recvDistrict;
    @ApiModelProperty("邮件地址")
    private String emailAddr;
    private String contactAddr;
    private String contactName;
    private String contactPhone;
    private String weichatNo;
    private String weichatPicNo;
    private String shopQrcode;
    private Integer validStatus;

    private String orderNo;

    private Date validBeginTime;

    private Date validEndTime;
    private Integer monthNum;
    private String operNo;
}
