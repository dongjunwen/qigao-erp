package com.qigao.erp.commons.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Date:2017/12/25 0025 14:46
 * @Author lu.dong
 * @Description：
 **/
@Data
@ApiModel(value = "角色操作实体 SysRoleResultDto")
public class SysRoleResultDto {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;
    /**
     * 面包屑导航的父id
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String pId;
    private String name;
    @ApiModelProperty(value = "角色代码", required = true)
    private String roleCode;
    @ApiModelProperty(value = "角色名称", required = true)
    private String roleName;
    private boolean checked;
}
