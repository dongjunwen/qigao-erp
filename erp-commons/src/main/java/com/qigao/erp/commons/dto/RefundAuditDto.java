package com.qigao.erp.commons.dto;

import com.qigao.erp.commons.enums.AuditActionEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/20 18:08
 * @Description:
 */
@Data
public class RefundAuditDto {
    @ApiModelProperty("退款单号")
    @NotNull(message = "退款单号不能为空")
    private String refundOrderNo;
    @ApiModelProperty("实际退款金额")
    @NotNull(message = "实际退款金额不能为空")
    private String actRefundAmt;
    @ApiModelProperty("退款单动作")
    @NotNull(message = "审核动作不能为空")
    private AuditActionEnum auditActionEnum;
    @ApiModelProperty("审核备注")
    @NotNull(message = "审核备注不能为空")
    @Max(value = 128,message = "审核备注不能超过128")
    private String auditDesc;
    //操作员
    private String userNo;
}
