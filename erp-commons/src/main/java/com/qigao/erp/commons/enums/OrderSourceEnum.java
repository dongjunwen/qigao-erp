package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @Author:luiz
 * @Date: 2018/5/28 10:14
 * @Descripton:订单来源 0:单个商品 1:购物车 默认为0
 * @Modify :
 **/
public enum OrderSourceEnum {
    SINGLE_ITEM("0","单个商品"),
    SHOP_CART("1","购物车"),
    ;
    private String code;
    private String name;

    OrderSourceEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final OrderSourceEnum parse(String code){
        for(OrderSourceEnum statusEnum : OrderSourceEnum.values()){
            if(code.equals(statusEnum.getCode())) return statusEnum;
        }
        return null;
    }

    public static String getNameByCode(String status) {
        OrderSourceEnum statusEnum = matching(status);
        if (statusEnum != null) return statusEnum.getName();
        return "";
    }

    public static final OrderSourceEnum matching(String code) {
        for (OrderSourceEnum orderStatusEnum : OrderSourceEnum.values()) {
            if (Objects.equals(orderStatusEnum.getCode(), code)) return orderStatusEnum;
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
