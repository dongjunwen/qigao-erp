package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2020-04-2020/4/22 14:08
 * @Description:
 */
@Data
public class OpenShopDto {
    @ApiModelProperty("套餐编号")
    private String  packageNo;
    @ApiModelProperty("店铺名称")
    @NotNull(message = "店铺名称不能为空!")
    private String shopName;
    @ApiModelProperty("省份代码")
    private String provinceCode;
    @ApiModelProperty("省份名称")
    private String recvProvince;
    @ApiModelProperty("城市代码")
    private String cityCode;
    @ApiModelProperty("城市名称")
    private String recvCity;
    @ApiModelProperty("区、县代码")
    private String districtCode;
    @ApiModelProperty("区、县名称")
    private String recvDistrict;
    @ApiModelProperty("联系地址")
    private String contactAddr;
    @ApiModelProperty("联系人")
    @NotNull(message = "联系人不能为空!")
    private String contactName;
    @ApiModelProperty("手机号")
    @NotNull(message = "手机号不能为空!")
    private String contactPhone;
    @ApiModelProperty("邮件地址")
    private String emailAddr;
    @ApiModelProperty("微信号")
    private String weichatNo;
    @ApiModelProperty("微信图片")
    private String weichatPicNo;
    //操作人
    private String operNo;
    @ApiModelProperty("所属商户号--用户端")
    private String merNo;
    @ApiModelProperty("所属店铺号--用户端")
    private String shopNo;
    @ApiModelProperty("系统商户号--商户端")
    private String sysMerNo;
    @ApiModelProperty("系统店铺号--商户端")
    private String sysShopNo;
    //生成二维码时使用
    private String accessToken;
}
