package com.qigao.erp.commons.dto;

/**
 * @author luiz
 * @Title: UserDeviceDto
 * @ProjectName iot-server
 * @Description: TODO
 * @date 2019-06-26 13:36
 */
public class UserDeviceDto {
    /**
     * 设备编号
     */
    private String iotId;
    /**
     * 用户编号
     */
    private String userNo;
}
