package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Date:2017/10/19 0019 15:26
 * @Author lu.dong
 * @Description：
 **/
@Data
@ApiModel(value = "用户角色操作实体 SysUserRoleVo")
public class SysUserRoleDto implements Serializable {

    @ApiModelProperty(value = "登录号", required = true)
    private String userNo;
    @ApiModelProperty(value = "角色代码", required = true)
    private String roleCode;
    @ApiModelProperty(value = "角色名称", required = true)
    private String roleName;

}
