package com.qigao.erp.commons.dto;

import lombok.Data;

@Data
public class SysDepartResultDto {
    private Integer id;

    private String departNo;

    private String departName;

    private String parentDepartNo;

    private String compNo;

    private String compName;

    private String merNo;

    private Integer version;

    private String createNo;

    private String createTime;

    private String modiNo;

    private String modiTime;
}
