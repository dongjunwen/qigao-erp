package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserPicCondDto extends PageDto {
    @ApiModelProperty(value = "用户编号")
    private String userNo;
    @ApiModelProperty(value = "开始日期 yyyy-mm-dd格式")
    private String beginDate;
    @ApiModelProperty(value = "结束日期 yyyy-mm-dd格式")
    private String endDate;
}
