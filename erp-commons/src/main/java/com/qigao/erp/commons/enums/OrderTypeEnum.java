package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @Author:luiz
 * @Date: 2018/5/28 10:14
 * @Descripton:订单类型
 * @Modify :
 **/
public enum OrderTypeEnum {
    CUSTOMER_BUY("CUSTOMER_BUY","直接购买"),
    NORMAL("NORMAL","帮别人购买订单"),
    UPLEVEL("UPLEVEL","自己升级订单"),
    RESALE("RESALE","自己重销订单"),
    SCORE("SCORE","积分兑换"),
    OPEN_SHOP("OPEN_SHOP","开店")
    ;
    private String code;
    private String name;


    OrderTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final OrderTypeEnum parse(String code){
        for(OrderTypeEnum statusEnum : OrderTypeEnum.values()){
            if(code.equals(statusEnum.getCode())) return statusEnum;
        }
        return null;
    }

    public static String getNameByCode(String status) {
        OrderTypeEnum statusEnum = matching(status);
        if (statusEnum != null) return statusEnum.getName();
        return "";
    }

    public static final OrderTypeEnum matching(String code) {
        for (OrderTypeEnum orderStatusEnum : OrderTypeEnum.values()) {
            if (Objects.equals(orderStatusEnum.getCode(), code)) return orderStatusEnum;
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
