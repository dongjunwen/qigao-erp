package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 费用项
 * @date 2019-04-03 16:00
 *
 */
public enum BillItemTypeEnum {
    USER("USER", "用户"),
    INVITE("INVITE", "推荐人"),
    INVITE_TYPE_LEVEL("INVITE_TYPE_LEVEL", "推荐人-用户类型-级别"),
    INVITE_LAYER_TYPE_PROD("INVITE_LAYER_TYPE_PROD", "推荐人-层-用户类型-产品类型"),
    INVITE_LAYER_TYPE_LEVEL_ORDERTYPE_PROD("INVITE_LAYER_TYPE_LEVEL_ORDERTYPE_PROD", "推荐人-层-用户类型-级别-订单类型-产品类型"),
    TOP_INVITE_TYPE("TOP_INVITE_TYPE", "顶级推荐人-用户类型"),
    TOP_INVITE_TYPE_PRDD("TOP_INVITE_TYPE_PRDD", "顶级推荐人-用户类型-产品类型"),
    TOP_INVITE_TYPE_LEVEL_PROD("TOP_INVITE_TYPE_LEVEL_PROD", "顶级推荐人-用户类型-级别-产品"),
    COMP_PROD("COMP_PROFIT", "公司-产品类型"),
    ;

    String code;
    String name;

    BillItemTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final BillItemTypeEnum matching(String code) {
        for (BillItemTypeEnum resultCode : BillItemTypeEnum.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }
}
