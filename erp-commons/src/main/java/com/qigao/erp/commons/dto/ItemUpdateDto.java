package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/1 14:07
 * @Description:
 */
@Data
public class ItemUpdateDto {
    /**
     * 商品编号
     */
    @ApiModelProperty("商品编号")
    private String itemNo;
    @ApiModelProperty("商品标题")
    private String itemTitle;
    /**
     * 商品描述
     */
    @Length(min = 1)
    @ApiModelProperty("商品描述")
    private String itemDesc;
    /**
     * 所属分类
     */
    @Length(min = 1,max = 128)
    @ApiModelProperty("所属一级分类")
    private String catNo1;
    /**
     * 图片列表
     */
    private List<ItemPicDto> itemPicDtos;

    /**
     * 商品属性列表
     */
    private List<ItemSkuDto> itemSkuDtos;

    /**
     * 所属分类
     */
    @Length(min = 1,max = 128)
    @ApiModelProperty("所属二级分类")
    private String catNo2;
    /**
     * 所属分类
     */
    @Length(min = 1,max = 128)
    @ApiModelProperty("所属三级分类")
    private String catNo3;

    @ApiModelProperty("产品图片")
    private String picUrl;
    @ApiModelProperty("产品缩略图片")
    private String narrowPicUrl;
    @ApiModelProperty("库存类型 0:不限制库存 1:有库存限制")
    private String stockType="1";
    /**
     * 商品价格
     */
    @Length(min = 1,max = 16)
    @ApiModelProperty("商品单价")
    private String price;

    private String costPrice;
    /**
     * 库存数量
     */
    @ApiModelProperty("库存数量")
    private String stockNum;

    private String profitType;

    @ApiModelProperty("店铺编号")
    private String shopNo;

    //操作人
    private String userNo;
}
