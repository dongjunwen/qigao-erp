package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("结算明细结果")
public class SettleDetailResultDto {

    @ApiModelProperty("结算日期")
    private String settleDate;
    @ApiModelProperty("用户名称")
    private String ownerName;
    @ApiModelProperty("分润金额")
    private BigDecimal profitAmt;
    @ApiModelProperty("分红金额")
    private BigDecimal bonusAmt;
    @ApiModelProperty("待结算金额")
    private BigDecimal unsettleAmt;
    @ApiModelProperty("公司名称")
    private String  compName;

    @ApiModelProperty("实际结算金额")
    private BigDecimal actSettleAmt;
    @ApiModelProperty("结算编号")
    private String settleNo;
    private String compNo;
    @ApiModelProperty("账户名称")
    private String accountName;

    private Integer version;

    private String createTime;

    private String createNo;

    private String modiTime;

    private String modiNo;

    private Long id;
    private String ownerNo;
    private String ownerType;

    private String accountNo;
}