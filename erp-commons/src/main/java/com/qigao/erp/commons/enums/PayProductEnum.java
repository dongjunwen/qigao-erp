package com.qigao.erp.commons.enums;

/**
@Author:luiz
@Date: 2018/5/25 16:15
@Descripton:支付产品列表
@Modify :
 **/
public enum PayProductEnum {

    ALIPAY_PC_WEB("ALIPAY_PC_WEB","支付宝PC网站支付"),
    ALIPAY_WAP_WEB("ALIPAY_WAP_WEB","支付宝手机网站支付"),
    ALIPAY_TIAOMA("ALIPAY_TIAOMA","支付宝条码支付"),
    ALIPAY_SAOMA("ALIPAY_SAOMA","支付宝扫码支付"),
    WEICHAT_SMALL_APP("WEICHAT_SMALL_APP","微信小程序"),
    WEICHAT_SCAN_CODE("WEICHAT_SCAN_CODE","微信扫码支付"),
    WEICHAT_H5("WEICHAT_H5","微信H5支付"),
    WEICHAT_H5_LOGIN("WEICHAT_H5_LOGIN","微信H5授权"),
    WEICHAT_PC_LOGIN("WEICHAT_PC_LOGIN","微信PC授权"),
    WEICHAT_SMALL_ENTER("WEICHAT_SMALL_ENTER","微信小微企业进件"),
    ;
    private String productCode;
    private String productName;

    PayProductEnum(String productCode, String productName) {
        this.productCode = productCode;
        this.productName = productName;
    }

    public static final PayProductEnum parse(String productCode){
        for(PayProductEnum payProductEnum : PayProductEnum.values()){
            if(productCode.equals(payProductEnum.getProductCode())) return payProductEnum;
        }
        return null;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getProductName() {
        return productName;
    }
}