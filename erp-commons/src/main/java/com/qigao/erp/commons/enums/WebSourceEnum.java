package com.qigao.erp.commons.enums;

/**
@Author:luiz
@Date: 2018/5/25 16:15
@Descripton: 网页来源
@Modify :
 **/
public enum WebSourceEnum {
    H5_WEB("H5_WEB","H5网页"),
    PC_WEB("PC_WEB","PC网页"),
    CASHIER("CASHIER","收银台"),
    WEICHAT_JSAPI("WEICHAT_JSAPI","微信小程序"),
    ;
    private String code;
    private String name;

    WebSourceEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final WebSourceEnum parse(String code){
        for(WebSourceEnum webSourceEnum : WebSourceEnum.values()){
            if(code.equals(webSourceEnum.getCode())) return webSourceEnum;
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}