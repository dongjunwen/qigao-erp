package com.qigao.erp.commons.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * @Date:2017/12/25 0025 14:46
 * @Author lu.dong
 * @Description：
 **/
public class SysResourceResultDto {

    private Integer id;

    private String resourceNo;

    private String resourceName;

    private String presourceNo;

    private String resourceType;

    private String reqUrl;

    private String routePath;

    private String resourceIcon;

    private Integer sortOrder;

    private Integer resourceLevel;

    private String ifVisible;


    private List<SysResourceResultDto> children;


    private String sourceId;

    /**
     * 面包屑导航的父id
     */
    //@JsonInclude(JsonInclude.Include.NON_NULL)
    private String pId;
    /**
     * 面包屑导航的父id
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String bpid;
    /**
     * 菜单的父id,缺省时为一级菜单,为-1时在菜单中不显示
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mpid;
    /**
     * 显示名称
     */
    private String name;
    /**
     * 匹配路由,缺省时不做跳转
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String route;
    /**
     * 在名称前显示的图标
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String icon;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResourceNo() {
        return resourceNo;
    }

    public void setResourceNo(String resourceNo) {
        this.resourceNo = resourceNo;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getPresourceNo() {
        return presourceNo;
    }

    public void setPresourceNo(String presourceNo) {
        this.presourceNo = presourceNo;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getReqUrl() {
        return reqUrl;
    }

    public void setReqUrl(String reqUrl) {
        this.reqUrl = reqUrl;
    }

    public String getRoutePath() {
        return routePath;
    }

    public void setRoutePath(String routePath) {
        this.routePath = routePath;
    }

    public String getResourceIcon() {
        return resourceIcon;
    }

    public void setResourceIcon(String resourceIcon) {
        this.resourceIcon = resourceIcon;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Integer getResourceLevel() {
        return resourceLevel;
    }

    public void setResourceLevel(Integer resourceLevel) {
        this.resourceLevel = resourceLevel;
    }

    public String getIfVisible() {
        return ifVisible;
    }

    public void setIfVisible(String ifVisible) {
        this.ifVisible = ifVisible;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getBpid() {
        return bpid;
    }

    public void setBpid(String bpid) {
        this.bpid = bpid;
    }

    public String getMpid() {
        return mpid;
    }

    public void setMpid(String mpid) {
        this.mpid = mpid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<SysResourceResultDto> getChildren() {
        return children;
    }

    public void setChildren(List<SysResourceResultDto> children) {
        this.children = children;
    }
}
