package com.qigao.erp.commons.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-16 14:40
 * @Description:
 */
@Data
public class SysResourcePermitResultDto {
    @ApiModelProperty(value = "主键Id")
    private Integer id;
    @ApiModelProperty(value = "来源编码")
    private String sourceCode;
    @ApiModelProperty(value = "来源类型")
    private String resourceType;
    @ApiModelProperty(value = "资源代码")
    private String resourceNo;
    @ApiModelProperty(value = "资源名称")
    private String resourceName;
    @ApiModelProperty(value = "资源路径")
    private String reqUrl;
    @ApiModelProperty(value = "资源图标")
    private String resourceIcon;
    @ApiModelProperty(value = "上级编号")
    private String presourceNo;
    @ApiModelProperty(value = "路由")
    private String routePath;
    @ApiModelProperty(value = "排序")
    private Integer sortOrder;
    @ApiModelProperty(value = "资源级别")
    private Integer resourceLevel;
    @ApiModelProperty(value = "是否拥有权限")
    private boolean checked;

    private List<SysResourcePermitResultDto> children;

    /**
     * 面包屑导航的父id
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String pId;
    private String name;
}
