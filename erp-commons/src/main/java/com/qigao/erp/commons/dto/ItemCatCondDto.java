package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/11 10:35
 * @Description:商品分类查询参数
 */
@Data
public class ItemCatCondDto extends PageDto {
    @ApiModelProperty("分类名称")
    private String catName;
}
