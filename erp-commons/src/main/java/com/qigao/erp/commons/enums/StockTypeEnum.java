package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: 数据类型枚举
 * @date 2019-04-03 16:00
 */
public enum StockTypeEnum {
    STOCK_FREE("0", "无库存限制"),
    STCOK_LIMIT("1", "库存限制"),
    ;

    String code;
    String name;

    StockTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final StockTypeEnum matching(String code) {
        for (StockTypeEnum resultCode : StockTypeEnum.values()) {
            if (Objects.equals(resultCode.getCode(), code)) return resultCode;
        }
        return null;
    }
}
