package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 11:31
 * @Description:
 */
@Data
@ApiModel("商品图片结果")
public class ItemPicResultDto {
    private Integer id;
    @ApiModelProperty("商品编号")
    private String itemNo;
    @ApiModelProperty("图片所属 TOP:顶部预览 DETAIL:底部详情")
    private String picLocation;
    @ApiModelProperty("产品图片地址")
    private String picNo;
    @ApiModelProperty("产品缩略地址")
    private String narrowPicNo;
    @ApiModelProperty("图片地址")
    private String picUrl;
    @ApiModelProperty("缩略图地址")
    private String narrowPicUrl;
    @ApiModelProperty("是否顶部展示")
    private Integer isMain;
    @ApiModelProperty("图片标题描述 可以放在图片上面，作为文字展示")
    private String picTitle;

}
