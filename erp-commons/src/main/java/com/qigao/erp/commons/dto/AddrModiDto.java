package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: AddrResultDto
 * Author:   luiz
 * Date:     2019/11/10 16:48
 * Description: 收件人地址
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/10 16:48       版本号              描述
 */
@Data
public class AddrModiDto {
    private Integer id;
    private String userNo;

    private String recvName;

    private String recvPhone;

    private String recvMobile;

    private String provinceCode;

    private String recvProvince;

    private String cityCode;

    private String recvCity;

    private String districtCode;

    private String recvDistrict;

    private String recvAddress;

    private String recvZip;

    private String ifDefault;
}
