package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: OrderQueryVo
 * Author:   luiz
 * Date:     2019/11/18 21:40
 * Description: 订单查询vo
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/18 21:40       版本号              描述
 */
@Data
@ApiModel("账户流水查询条件")
public class AccountLogCondDto extends PageDto {
    @ApiModelProperty("开始日期")
    private String startDate;
    @ApiModelProperty("结束日期")
    private String endDate;
    private String userNo;
}
