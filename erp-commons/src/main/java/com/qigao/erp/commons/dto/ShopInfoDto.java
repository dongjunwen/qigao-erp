package com.qigao.erp.commons.dto;

import lombok.Data;

@Data
public class ShopInfoDto {
    private Long id;

    private String merNo;

    private String merName;

    private String shopNo;

    private String shopName;

    private String provinceCode;

    private String recvProvince;

    private String cityCode;

    private String recvCity;

    private String districtCode;

    private String recvDistrict;

    private String contactAddr;

    private String contactName;

    private String contactPhone;

    private String weichatNo;

    private String weichatPicNo;

    private String shopQrcode;

    private Integer validStatus;

    private String validStatusName;

    private String orderNo;

    private String validBeginTime;

    private String validEndTime;

    private String createTime;

    private String createNo;

    private String modiTime;

    private String modiNo;
}
