package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.web.vo.app
 * @ProjectName three-mall
 * @date 2019-11-2019/11/21 14:13
 * @Description:
 */
@Data
@ApiModel("退款单查询条件")
public class RefundQueryDto extends PageDto {
    @ApiModelProperty("退款单状态")
    private String refundStatus;
    private String shopNo;
}
