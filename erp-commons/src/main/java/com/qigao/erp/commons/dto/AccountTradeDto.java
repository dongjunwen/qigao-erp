package com.qigao.erp.commons.dto;

import com.qigao.erp.commons.enums.TradeTypeEnum;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2020-04-2020/4/10 10:57
 * @Description:
 */
@Data
public class AccountTradeDto {
    private String userNo;

    private String userName;

    private String fromAccountNo;

    private String fromAccountName;

    private String toAccountNo;

    private String toAccountName;

    private String operAction;

    private String operName;

    private BigDecimal operAmt;

    private String merNo;

    private String merName;

    private String sourceOrderNo;

    private String sourceOrderType;

    private TradeTypeEnum sourceTradeType;//来源交易类型
}
