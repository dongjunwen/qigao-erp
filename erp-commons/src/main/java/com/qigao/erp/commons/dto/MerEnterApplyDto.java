package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MerEnterApplyDto {
    @ApiModelProperty("商户简称")
    private String merchantShortname;
    @ApiModelProperty("身份证号码")
    private String idCardNumber;
    @ApiModelProperty("身份证姓名")
    private String idCardName;
    @ApiModelProperty("身份证人像面照片")
    private String idCardCopy;
    @ApiModelProperty("身份证国徽面照片")
    private String idCardNational;
    @ApiModelProperty("身份证有效期限")
    private String idCardValidTime;
    @ApiModelProperty("开户名称")
    private String accountName;
    @ApiModelProperty("开户银行")
    private String accountBank;
    @ApiModelProperty("开户银行省市编码")
    private String bankAddressCode;
    @ApiModelProperty("开户银行全称（含支行）")
    private String bankName;
    @ApiModelProperty("银行账号")
    private String accountNumber;
    @ApiModelProperty("门店名称")
    private String storeName;
    @ApiModelProperty("门店省市编码")
    @NotNull(message = "门店省市编码不能为空!")
    private String storeAddressCode;
    @ApiModelProperty("门店街道名称")
    @NotNull(message = "门店街道名称不能为空!")
    private String storeStreet;
    @ApiModelProperty("门店经度")
    private String storeLongitude;
    @ApiModelProperty("门店纬度")
    private String storeLatitude;
    @ApiModelProperty("门店门口照片")
    @NotNull(message = "门店门口照片不能为空!")
    private String storeEntrancePic;
    @ApiModelProperty("店内环境照片")
    @NotNull(message = "店内环境照片不能为空!")
    private String indoorPic;
    @ApiModelProperty("经营场地证明")
    private String addressCertification;
    @ApiModelProperty("客服电话")
    @NotNull(message = "客服电话不能为空!")
    private String servicePhone;
    @ApiModelProperty("售卖商品/提供服务描述")
    private String productDesc;
    @ApiModelProperty("费率")
    private String rate;
    @ApiModelProperty("补充说明")
    private String businessAdditionDesc;
    @ApiModelProperty("补充材料")
    private String businessAdditionPics;
    @ApiModelProperty("超级管理员姓名")
    @NotNull(message = "超级管理员姓名不能为空!")
    private String contact;
    @ApiModelProperty("手机号码")
    @NotNull(message = "手机号码不能为空!")
    private String contactPhone;
    @ApiModelProperty("联系邮箱")
    private String contactEmail;
    //上级商户号
    private String pMerNo;

    private String operNo;
}
