package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SysCompanyResultDto {
    private Integer id;

    private String parentCompNo;

    private String compNo;

    private String compName;

    private String compType;

    private String compTypeName;

    private String compLevel;

    private String contactName;

    private String tel;

    private String emailAddr;

    private Integer status;

    @ApiModelProperty(value = "省份", required = false)
    private String provinceCode;
    @ApiModelProperty(value = "省份名称", required = false)
    private String provinceName;
    @ApiModelProperty(value = "城市代码", required = false)
    private String cityCode;
    @ApiModelProperty(value = "城市名称", required = false)
    private String cityName;
    @ApiModelProperty(value = "区县代码", required = false)
    private String districtCode;
    @ApiModelProperty(value = "区县名称", required = false)
    private String districtName;
    @ApiModelProperty(value = "详细地址", required = false)
    private String contactAddr;

    private String recvZip;

    private String resv1;

    private String resv2;

    private String merNo;

    private Integer version;

    private String createNo;

    private String createTime;

    private String modiNo;

    private String modiTime;

    private List<SysCompanyResultDto> children;
}
