package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/11 17:03
 * @Description:
 * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_7&index=8
 */
@Data
public class RefundNotifyDto {
    private String appId;
    private String mchId;
    private String nonceStr;
    //微信订单号
    private String transactionId;
    //商户订单号
    private String outTradeNo;
    //微信退款单号
    private String refundId;
    //商户退款单号
    private String outRefundNo;
    //	订单总金额，单位为分
    private String totalFee;
    //	申请退款金额，单位为分
    private String refundFee;
    //SUCCESS-退款成功
    //CHANGE-退款异常
    //REFUNDCLOSE—退款关闭
    private String refundStatus;
    ///退款入账账户
    private String refundRecvAccount;
    //退款资金来源
    //REFUND_SOURCE_RECHARGE_FUNDS 可用余额退款/基本账户
    //REFUND_SOURCE_UNSETTLED_FUNDS 未结算资金退款
    private String refundAccount;
    //退款发起来源    //API接口    //VENDOR_PLATFORM商户平台
    private String refundRequestSource;

    //退款成功时间
    private String successTime;

    private String returnCode;
    private String returnMsg;



}
