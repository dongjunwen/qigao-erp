package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("分红结果")
public class BonusDetailResultDto {
    @ApiModelProperty("结算日期")
    private String settleDate;
    @ApiModelProperty("姓名")
    private String userName;
    @ApiModelProperty("用户级别名称")
    private String userLevelName;
    @ApiModelProperty("级别数")
    private BigDecimal levelNum;
    @ApiModelProperty("总级别数")
    private BigDecimal totalLevelNum;
    @ApiModelProperty("销售订单数")
    private BigDecimal orderNum;
    @ApiModelProperty("总订单数")
    private BigDecimal totalOrderNum;
    @ApiModelProperty("公司名称")
    private String compName;
    @ApiModelProperty("公司当期差额利润")
    private BigDecimal compProfitAmt;
    @ApiModelProperty("销售应得分红=公司当期差额利润*0.5*销售订单数/总订单数")
    private BigDecimal saleBonus;
    @ApiModelProperty("本级别应得分红=公司当期利润*0.5*级别数/总级别数")
    private BigDecimal levelBonus;
    @ApiModelProperty("当期应得总分红=销售应得分红+本级别应得分红")
    private BigDecimal profitAmt;

    @ApiModelProperty("用户类型名称")
    private String userTypeName;
    private Long id;
    @ApiModelProperty("结算编号")
    private String settleNo;
    @ApiModelProperty("公司编号")
    private String compNo;
    @ApiModelProperty("用户号")
    private String userNo;
    @ApiModelProperty("用户类型")
    private Integer userType;
    @ApiModelProperty("用户级别")
    private String userLevel;
    @ApiModelProperty("公司当期销售额")
    private BigDecimal compSaleAmt;
    @ApiModelProperty("公司当期商品成本")
    private BigDecimal compItemCost;
    @ApiModelProperty("公司当期分润金额")
    private BigDecimal compProfitCost;

    private Integer version;

    private String createTime;

    private String createNo;

    private String modiTime;

    private String modiNo;

}