package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: NoticeQueryDto
 * Author:   luiz
 * Date:     2019/11/10 15:53
 * Description: 公告查询信息
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/10 15:53       版本号              描述
 */
@Data
public class NoticeQueryDto  extends PageDto {
    //生效状态
    private int noticeStatus;
}
