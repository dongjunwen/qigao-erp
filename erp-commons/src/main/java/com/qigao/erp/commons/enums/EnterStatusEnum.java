package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 16:00
 * 进件状态 0:进件中 1:已进件 2:进件失败
 */
public enum EnterStatusEnum {
    PROCESS(0, "进件中"),
    SUCCESS(1, "已进件"),
    FAIL(2, "进件失败"),
    ;

    int code;
    String name;

    EnterStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(int status) {
        EnterStatusEnum statusEnum = matching(status);
        if (statusEnum != null) {
            return statusEnum.getName();
        }
        return "";
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final EnterStatusEnum matching(int code) {
        for (EnterStatusEnum statusEnum : EnterStatusEnum.values()) {
            if (Objects.equals(statusEnum.getCode(), code)) return statusEnum;
        }
        return null;
    }
}
