package com.qigao.erp.commons.dto;


/**
 * @Author:luiz
 * @Date: 2018/1/31 14:34
 * @Descripton:
 * @Modify :
 **/

public class PageDto {
    //第N页
    private int pageNum=1;
    //每页显示行数
    private int pageSize=10;
    //搜索的索引下标
    private int pageIndex=0;

    public int getPageIndex() {
        return pageNum-1;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "PageDto{" +
                "pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                ", pageIndex=" + pageIndex +
                '}';
    }
}
