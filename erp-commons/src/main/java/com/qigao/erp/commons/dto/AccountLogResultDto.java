package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-28 11:09
 * @Description:
 */
@Data
@ApiModel("账户资金流水结果")
public class AccountLogResultDto {
    private String createTime;
    @ApiModelProperty("账户名称")
    private String accountName;
    @ApiModelProperty("操作名称")
    private String operName;
    @ApiModelProperty("操作金额")
    private BigDecimal operAmt;
    @ApiModelProperty("公司名称")
    private String compName;

    private BigDecimal beforeAmt;
    @ApiModelProperty("资金方向")
    private String amtDirect;

    private BigDecimal afterAmt;

    private String operAction;

    private Long id;

    private String logNo;

    private String merNo;

    private String merName;

    private String sourceOrderNo;

    private String sourceOrderType;

    private String accountNo;

    private String userNo;

    private String userName;

    private String compNo;

    private String modiTime;
}
