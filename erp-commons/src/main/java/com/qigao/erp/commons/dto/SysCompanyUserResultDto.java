package com.qigao.erp.commons.dto;

import lombok.Data;

@Data
public class SysCompanyUserResultDto {
    private Integer id;

    private String compNo;

    private String compName;

    private String userNo;

    private String roleCode;

    private String roleName;

    private String createNo;

    private String createTime;

    private String modiNo;

    private String modiTime;

}
