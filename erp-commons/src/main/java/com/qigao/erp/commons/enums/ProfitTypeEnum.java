package com.qigao.erp.commons.enums;

import org.springframework.util.StringUtils;

/**
 * @Author:luiz
 * @Date: 2018/5/28 10:14
 * @Descripton:
 * @Modify : 分润类型
 **/
public enum ProfitTypeEnum {
    PROD1("PROD1","产品1"),
    PROD2("PROD2","产品2"),
    SCORE("SCORE","积分兑换商品"),
    ;
    private String code;
    private String name;

    ProfitTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final ProfitTypeEnum parse(String code){
        for(ProfitTypeEnum statusEnum : ProfitTypeEnum.values()){
            if(code.equals(statusEnum.getCode())) return statusEnum;
        }
        return null;
    }

    public static final String getNameByCode(String code){
        if(StringUtils.isEmpty(code))return null;
        ProfitTypeEnum profitTypeEnum=parse(code);
        if (profitTypeEnum!=null)return profitTypeEnum.getName();
        return null;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
