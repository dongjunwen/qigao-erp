package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @Date:2017/12/25 0025 14:46
 * @Author lu.dong
 * @Description：
 **/
@Data
@ApiModel(value = "角色操作实体 SysRoleResourceUpdateDto")
public class SysRoleResourceUpdateDto {
    private String roleCode;
    private List<SysRoleResourceDto> sysRoleResourceDtoList;
}
