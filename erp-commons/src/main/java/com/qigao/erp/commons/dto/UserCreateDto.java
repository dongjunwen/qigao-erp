package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Getter
@Setter
@ToString
@ApiModel(value = "用户实体 UserCreateDto")
public class UserCreateDto {
    @NotEmpty(message = "登录号不能为空")
    @ApiModelProperty(value = "登录号", required = true)
    private String userNo;
    @ApiModelProperty(value = "姓名", required = true)
    private String userName;
    @ApiModelProperty(value = "昵称", required = false)
    private String nickName;
    @ApiModelProperty(value = "手机号", required = false)
    private String phoneNum;
    @ApiModelProperty(value = "联系方式", required = false)
    private String tel;
    @ApiModelProperty(value = "登录密码", required = false)
    private String loginPass="123456";
    @ApiModelProperty(value = "邮箱地址", required = false)
    private String emailAddr;
    @ApiModelProperty(value = "所属公司编号", required = false)
    private String compNo;
    @ApiModelProperty(value = "所属公司名称", required = false)
    private String compName;
    //用户来源 0:微信公众号 1:系统内部 2:客户
    private Integer userSource;
    @ApiModelProperty(value = "是否管理员 0:否 1:是", required = false)
    private Integer ifAdmin;
    @ApiModelProperty(value = "被推荐人", required = false)
    private String recommendNo;
    @ApiModelProperty(value = "省份编号", required = false)
    private String provinceCode;
    @ApiModelProperty(value = "省份名称", required = false)
    private String provinceName;
    @ApiModelProperty(value = "城市编号", required = false)
    private String cityCode;
    @ApiModelProperty(value = "城市名称", required = false)
    private String cityName;
    @ApiModelProperty(value = "区县编号", required = false)
    private String districtCode;
    @ApiModelProperty(value = "区县名称", required = false)
    private String districtName;
    @ApiModelProperty(value = "联系地址", required = false)
    private String contactAddr;
    @ApiModelProperty(value = "用户类型", required = false)
    private Integer userType;
    @ApiModelProperty(value = "用户级别", required = false)
    private String userLevel;
    @ApiModelProperty(value = "邀请码", required = false)
    private String inviteCode;
    @ApiModelProperty(value = "邀请人", required = false)
    private String inviteUserNo;
    @ApiModelProperty(value = "是否转正 0:不是 1:是 ", required = false)
    private Integer ifFormal;
    @ApiModelProperty(value = "入职日期", required = false)
    private Date entryDate;
    @ApiModelProperty(value = "转正日期", required = false)
    private Date formalDate;
    @ApiModelProperty(value = "用户状态")
    private Integer status;
    @ApiModelProperty(value = "性别 性别 0：未知、1：男、2：女", required = false)
    private String gender;
    @ApiModelProperty(value = "'头像地址'", required = false)
    private String headUrl;
    @ApiModelProperty(value = "'国家'", required = false)
    private String country;
    @ApiModelProperty(value = "'使用语言 '", required = false)
    private String language;

    private String operNo;
}
