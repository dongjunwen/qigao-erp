package com.qigao.erp.commons.enums;

/**
 * @author luiz
 * @Title: FrontResultCode
 * @ProjectName account
 * @Description: TODO
 * @date 2019-03-28 14:11
 */
public enum TransactionTypeEnum {

    ORDER_CREATE("ORDER_CREATE","订单创建"),
    ORDER_MODI("ORDER_MODI","订单修改"),
    PAY_NOTIFY("PAY_NOTIFY","支付通知"),
    APPLY_REFUND("APPLY_REFUND","退款申请"),
    AUDIT_REFUND("AUDIT_REFUND","退款审核"),
    REFUND_NOTIFY("REFUND_NOTIFY","退款通知"),
    APPLY_WITHDRAW("APPLY_WITHDRAW","提现申请"),
    AUDIT_WITHDRAW("AUDIT_WITHDRAW","提现审核"),
    RETISTER_USER("RETISTER_USER","用户注册"),
    PROFIT_CREATE("PROFIT_CREATE","分润单创建"),
    SETTLE_CREATE("SETTLE_CREATE","结算单创建"),
    AUDIT_SETTLE("AUDIT_SETTLE","结算单审核"),
    BONUS_CREATE("BONUS_CREATE","分红单创建"),
    ;

    String code;
    String message;

    TransactionTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
