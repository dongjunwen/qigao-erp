package com.qigao.erp.commons.enums;

/**
@Author:luiz
@Date: 2018/5/25 16:15
@Descripton:渠道层动作
@Modify :
 **/
public enum OrderActionEnum {
    ORDER_CANCEL("ORDER_CANCEL","订单取消"),
    CONFIRM_RECV("CONFIRM_RECV","确认收货"),
    CONFIRM_MONEY("CONFIRM_MONEY","确认收钱"),
    CONFIRM_SEND("CONFIRM_SEND","确认发货"),
    ORDER_DELETE("ORDER_DELETE","删除订单"),
    ;

    private String code;
    private String name;

    OrderActionEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final OrderActionEnum parse(String code){
        for(OrderActionEnum channelActionEnum : OrderActionEnum.values()){
            if(code.equals(channelActionEnum.getCode())) return channelActionEnum;
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}