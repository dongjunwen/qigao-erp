package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @Author luiz
 * @Date 2019/11/17 0017 21:35
 * @Version 1.0
 * @Description
 **/
@Data
public class GoPayResultDto {
    private String timeStamp;
    private String packageStr;
    private String prepareId;
    private String appId;
    private String signVal;
    private String signType;
    private String onceStr;
}
