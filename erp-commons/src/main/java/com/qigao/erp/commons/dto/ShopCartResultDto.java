package com.qigao.erp.commons.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/4 17:06
 * @Description:购物车结果
 */
@Data
public class ShopCartResultDto {
    //商品编号
    private String itemNo;
    /**
     * 标题
     */
    private String itemTitle;
    /**
     * 图片地址
     */
    private String picUrl;
    //购买数量
    private BigDecimal buyNum;
    //加入时的价格
    private BigDecimal price;
    //当前商品实际价格
    private BigDecimal currentPrice;
    //购买人
    private String buylerId;

}
