package com.qigao.erp.commons.enums;

import java.util.Objects;

/**
 * @author luiz
 * @Title: ResultCode
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 16:00
 */
public enum StatusEnum {
    UNVALID(0, "无效"),
    VALID(1, "有效"),
    FORBID(2, "禁用"),
    ;

    int code;
    String name;

    StatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(int status) {
        StatusEnum statusEnum = matching(status);
        if (statusEnum != null) {
            return statusEnum.getName();
        }
        return "";
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static final StatusEnum matching(int code) {
        for (StatusEnum statusEnum : StatusEnum.values()) {
            if (Objects.equals(statusEnum.getCode(), code)) return statusEnum;
        }
        return null;
    }
}
