package com.qigao.erp.commons.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-18 15:25
 * @Description:
 */
@Data
public class TbBillItemResultDto {

    private String userTypeName;

    private String userLevelName;

    private String itemCode;

    private String itemName;

    private String itemTypeName;

    private BigDecimal profitPercent;

    private Long id;

    private Integer userType;

    private String userLevel;

    private String itemType;

    private Integer version;

    private String createTime;

    private String createNo;

    private String modiTime;

    private String modiNo;
}
