package com.qigao.erp.commons.dto;

import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-16 14:10
 * @Description:
 */
@Data
public class SysDepartCondDto extends PageDto {
    private String departName;
}
