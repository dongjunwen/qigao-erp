package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2020-04-2020/4/22 17:55
 * @Description:
 */
@Data
public class IntentCustomerApplyDto {
    @ApiModelProperty("客户名称")
    private String customerName;
    @ApiModelProperty("手机号")
    private String customerPhone;
    @ApiModelProperty("需求内容")
    private String noticeContent;
}
