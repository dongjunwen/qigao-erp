package com.qigao.erp.commons.utils;

/**
 * @Author:luiz
 * @Date: 2018/3/6 16:30
 * @Descripton:
 * @Modify :
 **/

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 日期工具类
 */
public class DateUtil {

    private static final Logger logger= LoggerFactory.getLogger(DateUtil.class);
    // 默认日期格式
    public static final String DATE_DEFAULT_FORMAT = "yyyy-MM-dd";
    public static final String DATE_DEFAULT_FORMAT_STR = "yyyyMMdd";
    public static final String MONTH_DEFAULT_FORMAT = "yyyy-MM";

    // 默认时间格式
    public static final String DATETIME_DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String TIME_DEFAULT_FORMAT = "HH:mm:ss";

    /**
     * 日期格式化yyyy-MM-dd
     *
     * @param date
     * @return
     */
    public static Date formatDate(String date, String format) {
        try {
            if(StringUtils.isEmpty(date)) return null;
            return new SimpleDateFormat(format).parse(date);
        } catch (ParseException e) {
            logger.warn("日期格式化发生异常:{}",e);
        }
        return null;
    }


    public static Date formatDayStart(Date date) {
        if(StringUtils.isEmpty(date)) return null;
        SimpleDateFormat sdf=new SimpleDateFormat(DATE_DEFAULT_FORMAT);
        try {
            return sdf.parse(sdf.format(date));
        } catch (ParseException e) {
            logger.warn("日期格式化发生异常:{}",e);
        }
        return null;
    }

    /**
     * 日期格式化yyyy-MM-dd
     *
     * @param date
     * @return
     */
    public static String getDateFormat(Date date) {
        if(date!=null){
            return new SimpleDateFormat(DATE_DEFAULT_FORMAT).format(date);
        }
        return null;
    }

    /**
     * 日期格式化yyyy-MM-dd HH:mm:ss
     *
     * @param date
     * @return
     */
    public static String getDateTimeFormat(Date date) {
        if(date!=null){
            return new SimpleDateFormat(DATETIME_DEFAULT_FORMAT).format(date);
        }
       return null;
    }

    /**
     * 时间格式化
     *
     * @param date
     * @return HH:mm:ss
     */
    public static String getTimeFormat(Date date) {
        if(date!=null){
            return new SimpleDateFormat(TIME_DEFAULT_FORMAT).format(date);
        }
        return null;
    }

    /**
     * 日期格式化
     *
     * @param date
     * @param formatStr 格式类型
     * @return
     */
    public static String getDateFormat(Date date, String formatStr) {
        if (!StringUtils.isEmpty(formatStr)) {
            return new SimpleDateFormat(formatStr).format(date);
        }
        return null;
    }

    /**
     * 日期格式化
     *
     * @param date
     * @return
     */
    public static Date pareDate(String date) {
        try {
            if(StringUtils.isEmpty(date)) return null;
            return new SimpleDateFormat(DATE_DEFAULT_FORMAT).parse(date);
        } catch (ParseException e) {
            logger.warn("日期格式化发生异常:{}",e);
        }
        return null;
    }

    public static Date pareDate(String date, String timeFormat) {
        try {
            if(StringUtils.isEmpty(date)) return null;
            return new SimpleDateFormat(timeFormat).parse(date);
        } catch (ParseException e) {
            logger.warn("日期格式化发生异常:{}",e);
        }
        return null;
    }

    /**
     * 时间格式化
     *
     * @param date
     * @return
     */
    public static Date getDateTimeFormat(String date) {
        try {
            if(StringUtils.isEmpty(date)) return null;
            return new SimpleDateFormat(DATETIME_DEFAULT_FORMAT).parse(date);
        } catch (ParseException e) {
            logger.warn("日期格式化发生异常:{}",e);
        }
        return null;
    }


    /**
     * 获取当前日期星期一日期
     *
     * @return date
     */
    public static Date getFirstDayOfWeek() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setFirstDayOfWeek(Calendar.MONDAY);
        gregorianCalendar.setTime(new Date());
        gregorianCalendar.set(Calendar.DAY_OF_WEEK, gregorianCalendar.getFirstDayOfWeek()); // Monday
        return gregorianCalendar.getTime();
    }

    /**
     * 获取当前日期星期日日期
     *
     * @return date
     */
    public static Date getLastDayOfWeek() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setFirstDayOfWeek(Calendar.MONDAY);
        gregorianCalendar.setTime(new Date());
        gregorianCalendar.set(Calendar.DAY_OF_WEEK, gregorianCalendar.getFirstDayOfWeek() + 6); // Monday
        return gregorianCalendar.getTime();
    }

    /**
     * 获取日期星期一日期
     *
     * @param date 指定日期
     * @return date
     */
    public static Date getFirstDayOfWeek(Date date) {
        if (date == null) {
            return null;
        }
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setFirstDayOfWeek(Calendar.MONDAY);
        gregorianCalendar.setTime(date);
        gregorianCalendar.set(Calendar.DAY_OF_WEEK, gregorianCalendar.getFirstDayOfWeek()); // Monday
        return gregorianCalendar.getTime();
    }

    /**
     * 获取日期星期一日期
     *
     * @param date 指定日期
     * @return date
     */
    public static Date getLastDayOfWeek(Date date) {
        if (date == null) {
            return null;
        }
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setFirstDayOfWeek(Calendar.MONDAY);
        gregorianCalendar.setTime(date);
        gregorianCalendar.set(Calendar.DAY_OF_WEEK, gregorianCalendar.getFirstDayOfWeek() + 6); // Monday
        return gregorianCalendar.getTime();
    }

    /**
     * 获取当前月的第一天
     *
     * @return date
     */
    public static Date getFirstDayOfMonth() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(new Date());
        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 1);
        return gregorianCalendar.getTime();
    }

    /**
     * 获取当前月的最后一天
     *
     * @return
     */
    public static Date getLastDayOfMonth() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(new Date());
        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 1);
        gregorianCalendar.add(Calendar.MONTH, 1);
        gregorianCalendar.add(Calendar.DAY_OF_MONTH, -1);
        return gregorianCalendar.getTime();
    }

    /**
     * 获取指定月的第一天
     *
     * @param date
     * @return
     */
    public static Date getFirstDayOfMonth(Date date) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 1);
        return gregorianCalendar.getTime();
    }

    /**
     * 获取指定月的第一天开始时间
     *
     * @param date
     * @return
     */
    public static Date getFirstDayTimeOfMonth(Date date) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 1);
        gregorianCalendar.set(Calendar.HOUR_OF_DAY,0);
        gregorianCalendar.set(Calendar.MINUTE,0);
        gregorianCalendar.set(Calendar.SECOND,0);
        gregorianCalendar.set(Calendar.MILLISECOND,0);
        return gregorianCalendar.getTime();
    }


    /**
     * 获取指定月的最后一天
     *
     * @param date
     * @return
     */
    public static Date getLastDayOfMonth(Date date) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 1);
        gregorianCalendar.add(Calendar.MONTH, 1);
        gregorianCalendar.add(Calendar.DAY_OF_MONTH, -1);
        return gregorianCalendar.getTime();
    }

    /**
     * 获取指定月的最后一天结束时间
     *
     * @param date
     * @return
     */
    public static Date getLastDayTimeOfMonth(Date date) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 1);
        gregorianCalendar.add(Calendar.MONTH, 1);
        gregorianCalendar.add(Calendar.DAY_OF_MONTH, -1);
        gregorianCalendar.set(Calendar.HOUR,23);
        gregorianCalendar.set(Calendar.MINUTE,59);
        gregorianCalendar.set(Calendar.SECOND,59);
        gregorianCalendar.set(Calendar.MILLISECOND,0);
        return gregorianCalendar.getTime();
    }


    /**
     * 获取日期前一天
     *
     * @param date
     * @return
     */
    public static Date getDayBefore(Date date) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        int day = gregorianCalendar.get(Calendar.DATE);
        gregorianCalendar.set(Calendar.DATE, day - 1);
        return gregorianCalendar.getTime();
    }

    /**
     * 获取前一天开始时间
     * @param date
     * @return
     */
    public static Date getDayBeforeBegin(Date date) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        int day = gregorianCalendar.get(Calendar.DATE);
        gregorianCalendar.set(Calendar.DATE, day - 1);
        gregorianCalendar.set(Calendar.HOUR_OF_DAY,0);
        gregorianCalendar.set(Calendar.MINUTE,0);
        gregorianCalendar.set(Calendar.SECOND,0);
        gregorianCalendar.set(Calendar.MILLISECOND,0);
        return gregorianCalendar.getTime();
    }
    public static String getDayBeforeBeginStr(Date date) {
        return getDateTimeFormat(getDayBeforeBegin(date));
    }
    public static String getDayBeforeEndStr(Date date) {
        return getDateTimeFormat(getDayBeforeEnd(date));
    }

    public static String getDayBeginStr(Date date) {
        return getDateTimeFormat(getDayBegin(date));
    }

    public static String getDayEndStr(Date date) {
        return getDateTimeFormat(getDayEnd(date));
    }

    public static Date getDayBegin(Date date) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        int day = gregorianCalendar.get(Calendar.DATE);
        gregorianCalendar.set(Calendar.DATE, day);
        gregorianCalendar.set(Calendar.HOUR_OF_DAY,0);
        gregorianCalendar.set(Calendar.MINUTE,0);
        gregorianCalendar.set(Calendar.SECOND,0);
        gregorianCalendar.set(Calendar.MILLISECOND,0);
        return gregorianCalendar.getTime();
    }
    public static Date getDayEnd(Date date) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        int day = gregorianCalendar.get(Calendar.DATE);
        gregorianCalendar.set(Calendar.DATE, day );
        gregorianCalendar.set(Calendar.HOUR_OF_DAY,23);
        gregorianCalendar.set(Calendar.MINUTE,59);
        gregorianCalendar.set(Calendar.SECOND,59);
        return gregorianCalendar.getTime();
    }

    /**
     * 获取前一天结束时间
     * @param date
     * @return
     */
    public static Date getDayBeforeEnd(Date date) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        int day = gregorianCalendar.get(Calendar.DATE);
        gregorianCalendar.set(Calendar.DATE, day - 1);
        gregorianCalendar.set(Calendar.HOUR_OF_DAY,23);
        gregorianCalendar.set(Calendar.MINUTE,59);
        gregorianCalendar.set(Calendar.SECOND,59);
        return gregorianCalendar.getTime();
    }
    /**
     * 获取日期后一天
     *
     * @param date
     * @return
     */
    public static Date getDayAfter(Date date) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        int day = gregorianCalendar.get(Calendar.DATE);
        gregorianCalendar.set(Calendar.DATE, day + 1);
        return gregorianCalendar.getTime();
    }

    /**
     * 获取当前年
     *
     * @return
     */
    public static int getNowYear() {
        Calendar d = Calendar.getInstance();
        return d.get(Calendar.YEAR);
    }

    /**
     * 获取当前月份
     *
     * @return
     */
    public static int getNowMonth() {
        Calendar d = Calendar.getInstance();
        return d.get(Calendar.MONTH) + 1;
    }

    /**
     * 对时间加秒
     *
     * @param date
     * @param seconds
     * @return
     */
    public static Date addSeconds(Date date, int seconds) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        gregorianCalendar.add(Calendar.SECOND, seconds);
        return gregorianCalendar.getTime();
    }

    /**
     * 对时间加分钟
     *
     * @param date
     * @param minutes
     * @return
     */
    public static Date addMinute(Date date, int minutes) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        gregorianCalendar.add(Calendar.MINUTE, minutes);
        return gregorianCalendar.getTime();
    }


    /**
     * 对日期加天数
     *
     * @param date
     * @param days
     * @return
     */
    public static Date addDay(Date date, int days) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        gregorianCalendar.add(Calendar.DAY_OF_YEAR, days);
        return gregorianCalendar.getTime();
    }

    public static Date addMonth(Date date, int monthNum) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        gregorianCalendar.add(Calendar.MONTH, monthNum);
        return gregorianCalendar.getTime();
    }

    /**
     * 获取当月天数
     *
     * @return
     */
    public static int getNowMonthDay() {
        Calendar d = Calendar.getInstance();
        return d.getActualMaximum(Calendar.DATE);
    }


    /**
     * 获取提前多少个月
     *
     * @param monty
     * @return
     */
    public static Date getFirstMonth(int monty) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -monty);
        return c.getTime();
    }

    public static String getNowDateStr() {
        return new SimpleDateFormat(DATE_DEFAULT_FORMAT).format(new Date());
    }

    public static Integer addYear(Date date, int yearNum) {
        Calendar c = new GregorianCalendar();
        c.setTime(date);
        c.add(Calendar.YEAR, yearNum);
        return c.get(Calendar.YEAR);
    }

    public static String formatDateTime(Long valeLong) {
        return new SimpleDateFormat(DATETIME_DEFAULT_FORMAT).format(new Date(valeLong));
    }

    public static void main(String[] args) {
      //  System.err.println(DateUtil.getNowMonthStr());
       // System.err.println(DateUtil.getDateTimeFormat(DateUtil.getDayBeforeEnd(new Date())));
        System.err.println(ifMonthFirtDay(DateUtil.getDateTimeFormat("2020-08-01 15:00:00")));
        System.err.println(ifMonthFirtDay(DateUtil.getDateTimeFormat("2020-08-02 15:00:00")));
       /* Date settleDate=DateUtil.getDateTimeFormat("2020-08-26 08:00:00");
        String beginDate= DateUtil.getDayBeginStr(settleDate);
        String endDate= DateUtil.getDayEndStr(settleDate);
        System.err.println(beginDate);
        System.err.println(endDate);*/
       /* settleDate=DateUtil.addMonth(settleDate,-1);
        //上个月开始第一天开始时间
        Date lastMonthStartDate=DateUtil.getFirstDayTimeOfMonth(settleDate);
        //上个月最后一天结束时间
        Date lastMonthEndDate=DateUtil.getLastDayTimeOfMonth(settleDate);
        System.err.println(DateUtil.getDateTimeFormat(lastMonthStartDate));
        System.err.println(DateUtil.getDateTimeFormat(lastMonthEndDate));*/
    }


    public static String getNowMonthStr() {
        return new SimpleDateFormat(MONTH_DEFAULT_FORMAT).format(new Date());
    }

    public static boolean ifMonthFirtDay(Date actDate) {
        Calendar actCale = Calendar.getInstance();
        actCale.setTime(actDate);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        // 获取前月的第一天
        Calendar firstCare = Calendar.getInstance();
        firstCare.setTime(actDate);
        firstCare.add(Calendar.MONTH, 0);
        firstCare.set(Calendar.DAY_OF_MONTH, 1);
        String monthFirstDay = format.format(firstCare.getTime());
        String actDay = format.format(actCale.getTime());
        System.err.println(monthFirstDay+"----->"+actDay);
        if(monthFirstDay.equals(actDay)){
            return true;
        }
        return false;
    }
}
