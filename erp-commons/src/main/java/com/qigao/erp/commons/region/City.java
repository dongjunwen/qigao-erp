package com.qigao.erp.commons.region;

import java.util.List;

/**
 * 地级市
 * @author jx on 2018/4/12.
 */

public class City {
    /**
     * 省份
     */
    private String provinceCode;
    private String code;
    private String name;
    /**
     * 公布年月
     */
    private String publishYm;
    private List<Area> areaList;

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Area> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<Area> areaList) {
        this.areaList = areaList;
    }

    public String getPublishYm() {
        return publishYm;
    }

    public void setPublishYm(String publishYm) {
        this.publishYm = publishYm;
    }
}
