package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/4 15:56
 * @Description:
 */
@Data
public class ItemSkuResultDto {
    @ApiModelProperty("库存编号")
    private String stockNo;
    @ApiModelProperty("商品标题")
    private String stockTitle;
    @ApiModelProperty("库存类型 0:不限制库存 1:有库存限制")
    private String stockType;
    @ApiModelProperty("产品图片地址")
    private String picNo;
    @ApiModelProperty("产品缩略地址")
    private String narrowPicNo;
    @ApiModelProperty("产品图片")
    private String picUrl;
    @ApiModelProperty("产品缩略图片")
    private String narrowPicUrl;
    /**
     * 商品价格
     */
    @NotNull(message = "商品单价不能为空!")
    @Length(min = 1,max = 16)
    @ApiModelProperty("商品单价")
    private String price;
    /**
     * 库存数量
     */
    @ApiModelProperty("库存数量")
    private String stockNum;

    private Integer id;

    private String itemNo;
    @ApiModelProperty("成本价")
    private BigDecimal costPrice;

    @ApiModelProperty("成本价")
    private BigDecimal cost;
    private String indexes;

    private String ownSpec;
    @ApiModelProperty("分润类型")
    private String profitType;
    @ApiModelProperty("分润类型名称")
    private String profitTypeName;

    @ApiModelProperty("积分兑换比例")
    private BigDecimal scorePercent;
    private String merNo;

    private String shopNo;
}
