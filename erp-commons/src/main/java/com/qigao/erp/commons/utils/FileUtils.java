package com.qigao.erp.commons.utils;

import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.IOUtils;

import java.io.*;

/**
 * @Author:luiz
 * @Date: 2018/4/13 13:59
 * @Descripton:
 * @Modify :
 **/
public class FileUtils {
    /**
     * 存储到指定目录
     * @param file
     * @param filePath
     * @param fileName
     * @throws Exception
     */
    public static void uploadFile(byte[] file, String filePath, String fileName) throws Exception {
        File targetFile = new File(filePath);
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }
        FileOutputStream out = new FileOutputStream(filePath+fileName);
        out.write(file);
        out.flush();
        out.close();
    }

    public static byte[] readFile(String savePath, String saveFileName) throws IOException {
        byte[] resultData=null;
        FileInputStream inputStream=new FileInputStream(new File(savePath+saveFileName));
        resultData=IOUtils.toByteArray(inputStream);
        return resultData;
    }


    public static void zipImage(String sourceFilePath,String targetFilePath){
        File fromPic=new File(sourceFilePath);
        File toPic=new File(targetFilePath);
        try {
            //图片尺寸不变，压缩图片文件大小outputQuality实现,参数1为最高质量
             Thumbnails.of(fromPic).scale(1f).outputQuality(0.25f).toFile(toPic);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void narrowImage(String sourceFilePath,String targetFilePath){
        File fromPic=new File(sourceFilePath);
        File toPic=new File(targetFilePath);
        try {
            //高度 400  宽度 500
            Thumbnails.of(fromPic).size(400,500).toFile(toPic);//变为400 500,遵循原图比
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * A方法追加文件：使用RandomAccessFile
     */
    public static void appendFileByRandom(String fileName, String content) {
        try {
            // 打开一个随机访问文件流，按读写方式
            RandomAccessFile randomFile = new RandomAccessFile(fileName, "rw");
            // 文件长度，字节数
            long fileLength = randomFile.length();
            //将写文件指针移到文件尾。在该位置发生下一个读取或写入操作。
            randomFile.seek(fileLength);
            //按字节序列将该字符串写入该文件。
            randomFile.writeBytes(content);
            //关闭此随机访问文件流并释放与该流关联的所有系统资源。
            randomFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * B方法追加文件：使用FileWriter
     */
    public static void appendFile(String fileName, String content) {
        try {
            //打开一个写文件器，构造函数中的第二个参数true表示以追加形式写文件,如果为 true，则将字节写入文件末尾处，而不是写入文件开始处
            FileWriter writer = new FileWriter(fileName, true);
            writer.write(content);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
