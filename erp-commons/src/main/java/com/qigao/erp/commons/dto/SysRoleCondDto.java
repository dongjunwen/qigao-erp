package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @Date:2017/10/23 0023 14:39
 * @Author lu.dong
 * @Description：
 **/
@Data
@ApiModel(value = "角色操作实体 SysRoleCondDto")
public class SysRoleCondDto extends PageDto {
    @ApiModelProperty(value = "角色代码")
    private String roleCode;
    @ApiModelProperty(value = "角色名称")
    private String roleName;
}
