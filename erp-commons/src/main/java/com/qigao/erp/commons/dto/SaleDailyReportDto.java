package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/5 16:26
 * @Description:
 */
@Data
@ApiModel("每日销售报表")
public class SaleDailyReportDto {
    //销售日期
    @ApiModelProperty("销售日期 yyyy-mm-dd格式")
    private String saleDate;
    @ApiModelProperty("销售金额")
    private String saleAmt;
    /**
     * 每日销售明细
     */
    private List<OrderResultDto> orderResultDtoList;
}
