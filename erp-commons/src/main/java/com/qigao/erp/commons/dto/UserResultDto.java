package com.qigao.erp.commons.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: UserRegisterDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-04 10:22
 */
@Data
public class UserResultDto {
    @ApiModelProperty("手机号")
    private String phoneNum;
    @ApiModelProperty("姓名")
    private String userName;
    @ApiModelProperty("用户类型")
    private String userTypeName;
    @ApiModelProperty("用户级别")
    private String userLevelName;
    @ApiModelProperty("是否转正")
    private String ifFormalName;
    @ApiModelProperty("推荐姓名")
    private String inviteUserName;
    @ApiModelProperty("推荐人手机号")
    private String inviteUserNo;
    @ApiModelProperty("公司名称")
    private String compName;

    private Integer id;
    @ApiModelProperty("登录账号")
    private String userNo;

    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("电话")
    private String tel;
    @ApiModelProperty("邮件")
    private String emailAddr;

   // @ApiModelProperty("登录密码")
   // private String loginPass;
    @ApiModelProperty("状态")
    private Integer status;
    @ApiModelProperty("状态名称")
    private String statusName;
    @ApiModelProperty("是否管理员")
    private Integer ifAdmin;
    @ApiModelProperty("是否管理员")
    private String ifAdminName;
    @ApiModelProperty("推荐码")
    private String inviteCode;

    @ApiModelProperty(value = "省份编号", required = false)
    private String provinceCode;
    @ApiModelProperty(value = "省份名称", required = false)
    private String provinceName;
    @ApiModelProperty(value = "城市编号", required = false)
    private String cityCode;
    @ApiModelProperty(value = "城市名称", required = false)
    private String cityName;
    @ApiModelProperty(value = "区县编号", required = false)
    private String districtCode;
    @ApiModelProperty(value = "区县名称", required = false)
    private String districtName;
    @ApiModelProperty(value = "联系地址", required = false)
    private String contactAddr;

    private Integer gender;


    private String headUrl;
    @ApiModelProperty("公司编号")
    private String compNo;

    private String loginPass;

    private Integer userSource;

    private Integer userType;



    private String userLevel;



    private Integer ifFormal;



    private String entryDate;

    private String formalDate;

    private String country;

    private String language;

    private Integer version;

    private String createNo;

    private String createTime;

    private String modiNo;

    private String modiTime;

}
