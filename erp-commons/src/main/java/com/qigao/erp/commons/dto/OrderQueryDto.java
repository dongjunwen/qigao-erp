package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/19 10:42
 * @Description:
 */
@Data
public class OrderQueryDto extends PageDto {
    @ApiModelProperty("店铺编号")
    private String shopNo;
    @ApiModelProperty("公司编号")
    private String compNo;
    @ApiModelProperty("订单开始日期 YYYY-MM-DD")
    private String dateBegin;
    @ApiModelProperty("订单结束日期 YYYY-MM-DD")
    private String dateEnd;
    /**
     * 订单状态
     */
    private Integer orderStatus;
    /**
     * 用户
     */
    private String userNo;
    @ApiModelProperty("收货用户号")
    private String recvUserNo;
    @ApiModelProperty("订单号")
    private String orderNo;
    @ApiModelProperty("订单开始日期 YYYY-MM-DD")
    private String orderDateBegin;
    @ApiModelProperty("订单结束日期 YYYY-MM-DD")
    private String orderDateEnd;
    @ApiModelProperty("收款日期开始 YYYY-MM-DD")
    private String confirmDateBegin;
    @ApiModelProperty("收款日期结束 YYYY-MM-DD")
    private String confirmDateEnd;
    @ApiModelProperty("收货手机号/客户手机号")
    private String recvPhone;
    @ApiModelProperty("下单人/销售员")
    private String buylerName;
}
