package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/19 17:20
 * @Description:
 */
@Data
@ApiModel("订单商品明细")
public class OrderDetailResultDto {
    @ApiModelProperty("订单号")
    private String orderNo;
    @ApiModelProperty("商品编号")
    private String itemNo;
    @ApiModelProperty("商品名称")
    private String itemName;
    @ApiModelProperty("库存编号")
    private String stockNo;
    @ApiModelProperty("库存名称（可作为规格展示）")
    private String stockName;
    @ApiModelProperty("缩略图片路径")
    private String itemNarrowPic;
    @ApiModelProperty("图片路径")
    private String itemPicUrl;
    @ApiModelProperty("单价")
    private BigDecimal price;
    @ApiModelProperty("数量")
    private BigDecimal num;
    @ApiModelProperty("金额")
    private BigDecimal amt;
    @ApiModelProperty("成本价格")
    private BigDecimal costPrice;
    @ApiModelProperty("成本金额")
    private BigDecimal costAmt;
    @ApiModelProperty("优惠金额")
    private BigDecimal coupAmt;
    @ApiModelProperty("抵扣积分")
    private BigDecimal scoreAmt;
    @ApiModelProperty("客户获得积分")
    private BigDecimal customerScore;
    @ApiModelProperty("实际订单金额")
    private BigDecimal actOrderAmt;
    @ApiModelProperty("分润类型 PROD1:产品1 PROD2:产品2")
    private String profitType;
    @ApiModelProperty("分润类型 PROD1:产品1 PROD2:产品2")
    private String profitTypeName;
    @ApiModelProperty("特有规格属性在spu属性模板中的对应下标组合")
    private String indexes;
    @ApiModelProperty("sku的特有规格参数")
    private String ownSpec;

}
