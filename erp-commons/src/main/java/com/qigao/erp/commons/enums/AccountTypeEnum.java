package com.qigao.erp.commons.enums;

/**
@Author:luiz
@Date: 2018/5/25 16:15
@Descripton: 账户类型
@Modify :
 * 1.可用现金总账户
 * 2.平台手续费账户
 * 3.支付宝账户
 * 4.微信账户
 * 5.
 * 6.
 **/
public enum AccountTypeEnum {
    CASH_ACCOUNT("CASH_ACCOUNT","可用余额账户"),
    SCORE_ACCOUNT("SCORE_ACCOUNT","可用积分账户"),
    FEE_ACCOUNT("FEE_ACCOUNT","手续费账户"),
    ALI_ACCOUNT("ALI_ACCOUNT","支付宝账户"),
    WEICHAT_ACCOUNT("WEICHAT_ACCOUNT","微信账户"),
    ;
    private String code;
    private String name;


    AccountTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final AccountTypeEnum parse(String code){
        for(AccountTypeEnum webSourceEnum : AccountTypeEnum.values()){
            if(code.equals(webSourceEnum.getCode())) return webSourceEnum;
        }
        return null;
    }

    public static String getNameByCode(String accountType) {
        AccountTypeEnum accountTypeEnum=parse(accountType);
        if(accountTypeEnum!=null) return accountTypeEnum.getName();
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

}