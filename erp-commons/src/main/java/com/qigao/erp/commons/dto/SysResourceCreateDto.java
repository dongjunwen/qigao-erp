package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: SysResourceCreateDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-08 17:59
 */
@Data
public class SysResourceCreateDto {
    @ApiModelProperty(value = "资源代码", required = true)
    private String resourceNo;
    @ApiModelProperty(value = "资源名称", required = false)
    private String resourceName;
    @ApiModelProperty(value = "上级资源代码", required = false)
    private String presourceNo;
    @ApiModelProperty(value = "资源类型", required = false)
    private String resourceType;
    @ApiModelProperty(value = "请求路径", required = false)
    private String reqUrl;
    @ApiModelProperty(value = "路由路径", required = false)
    private String routePath;
    @ApiModelProperty(value = "资源图标", required = false)
    private String resourceIcon;
    @ApiModelProperty(value = "排序", required = false)
    private Integer sortOrder;
    @ApiModelProperty(value = "是否显示 Y:显示 N:不显示", required = false)
    private String ifVisible;
    @ApiModelProperty(value = "级别", required = false)
    private Integer resourceLevel;
    @ApiModelProperty(value = "备注", required = false)
    private String memo;
    private String userNo;
}
