package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: OrderQueryVo
 * Author:   luiz
 * Date:     2019/11/18 21:40
 * Description: 订单查询vo
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/18 21:40       版本号              描述
 */
@Data
@ApiModel("结算单显示结果")
public class SettleTotalResultDto  {
    @ApiModelProperty("结算编号")
    private String settleNo;
    @ApiModelProperty("结算日期")
    private String settleDate;
    @ApiModelProperty("结算状态名称")
    private String settleStatusName;
    @ApiModelProperty("待结算金额")
    private BigDecimal unsettleAmt;
    @ApiModelProperty("实际结算金额")
    private BigDecimal actSettleAmt;
    @ApiModelProperty("审核人")
    private String auditName;
    @ApiModelProperty("审核日期")
    private String auditTime;
    @ApiModelProperty("审核备注")
    private String auditMemo;
    @ApiModelProperty("所属公司")
    private String compName;
    @ApiModelProperty("主键ID")
    private Long id;
    private String compNo;
    private String createTime;
    private String modiTime;
}
