package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/20 18:08
 * @Description:
 */
@Data
public class RefundApplyDto {
    @ApiModelProperty("订单号")
    @NotNull(message = "订单号不能为空")
    private String orderNo;
    @ApiModelProperty("服务类型")
    @NotNull(message = "服务类型不能为空")
    private String serviceType;
    @ApiModelProperty("服务类型描述")
    @NotNull(message = "服务类型描述不能为空")
    private String serviceTypeDesc;
    @ApiModelProperty("货物状态")
    @NotNull(message = "货物状态不能为空")
    private String goodsStatus;
    @ApiModelProperty("货物状态描述")
    @NotNull(message = "货物状态描述不能为空")
    private String goodsStatusDesc;
    @ApiModelProperty("售后原因")
    private String sellReason;
    @ApiModelProperty("售后原因描述")
    private String sellReasonDesc;
    @ApiModelProperty("退款金额")
    private String refundAmt;
    @ApiModelProperty("退款原因")
    private String refundReason;
    @ApiModelProperty("退货图片")
    private List<String> picList;

    private String userNo;
}
