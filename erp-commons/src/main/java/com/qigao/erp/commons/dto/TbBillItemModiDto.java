package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-18 15:19
 * @Description:
 */
@Data
public class TbBillItemModiDto extends TbBillItemCreateDto {
    @ApiModelProperty(value = "主键ID", required = true)
    private Long id;
    private String modiUserNo;
}
