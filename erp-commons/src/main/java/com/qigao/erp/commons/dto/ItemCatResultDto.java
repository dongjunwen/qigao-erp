package com.qigao.erp.commons.dto;

import lombok.Data;

import java.util.List;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/1 14:39
 * @Description:
 */
@Data
public class ItemCatResultDto {
    /**
     * 分类编号
     */
    private String catNo;
    /**
     * 类目名称
     */
    private String catName;
    /**
     * 上级类目编号
     */
    private String pcatNo;
    /**
     * 类目级别
     */
    private Integer itemLevel;
    /**
     * 排序
     */
    private Integer itemSort;
    /**
     *
     */
    private List<ItemCatResultDto> children;

}
