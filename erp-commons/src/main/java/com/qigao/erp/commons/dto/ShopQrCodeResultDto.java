package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ShopQrCodeResultDto {
    @ApiModelProperty("店铺号")
    private String shopNo;
    @ApiModelProperty("店铺名称")
    private String shopName;
    @ApiModelProperty("店铺二维码")
    private String shopQrcode;
    @ApiModelProperty("省份代码")
    private String provinceCode;
    @ApiModelProperty("省份名称")
    private String recvProvince;
    @ApiModelProperty("城市代码")
    private String cityCode;
    @ApiModelProperty("城市名称")
    private String recvCity;
    @ApiModelProperty("区、县代码")
    private String districtCode;
    @ApiModelProperty("区、县名称")
    private String recvDistrict;
    @ApiModelProperty("联系地址")
    private String contactAddr;
    @ApiModelProperty("联系人")
    private String contactName;
    @ApiModelProperty("联系电话")
    private String contactPhone;
    @ApiModelProperty("微信号")
    private String weichatNo;
}
