package com.qigao.erp.commons.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2019-11-2019/11/21 14:04
 * @Description:
 */
@Data
public class RefundOrderResultDto {
    private String refundOrderNo;

    private String applyRefundUser;

    private String applyRefundTime;

    private String orderNo;

    private BigDecimal refundAmt;

    private String refundReason;

    private String serviceType;

    private String goodsStatus;

    private String serviceTypeDesc;

    private String sellReason;

    private String goodsStatusDesc;

    private BigDecimal actRefundAmt;

    private String sellReasonDesc;

    private String shopNo;

    private String operNo;

    private String operDesc;

    private String operTime;

    private Integer operStatus;

    private String  operStatusDesc;

    private String refundFinishTime;
}
