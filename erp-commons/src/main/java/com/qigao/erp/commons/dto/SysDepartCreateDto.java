package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Data
public class SysDepartCreateDto {
    @NotEmpty(message = "部门代码不能为空")
    @Length(min = 1, max = 32, message = "部门代码长度不能超过32")
    @ApiModelProperty(value = "部门代码", required = true)
    private String departNo;
    @NotEmpty(message = "部门名称不能为空")
    @Length(min = 1, max = 64, message = "部门名称长度不能超过32")
    @ApiModelProperty(value = "部门名称", required = true)
    private String departName;
    @ApiModelProperty(value = "上级部门编号", required = false)
    private String parentDepartNo;
    @ApiModelProperty(value = "所属公司", required = false)
    private String compNo;
    private String createUserNo;
}
