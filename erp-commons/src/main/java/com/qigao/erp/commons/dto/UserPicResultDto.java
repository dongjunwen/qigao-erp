package com.qigao.erp.commons.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author luiz
 * @Title: UserPicResultDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-15 11:05
 */
@Data
public class UserPicResultDto implements Serializable {

    private static final long serialVersionUID = -6969625072294732110L;
    private String picNo;

    private String userNo;

    private String photoType;

    private String photoTypeName;

    private String picType;

    private String picName;

    private String picSize;

    private String picDesc;

    private String resv1;

    private String resv2;

    private String resv3;

    private String resv4;

    private String createTime;

    private String modiTime;

    private String imgRealPath;

    /**
     * 原始文件
     */
    private String sourceFilePath;
    /**
     * 压缩后文件
     */
    private String zipFilePath;
    /**
     * 缩小后文件
     */
    private String narrowFilePath;
}
