package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: UserMsgCreateDto
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-08 10:49
 */
@Data
public class UserMsgCreateDto {
    //上级消息编号
    private String pMsgNo;
    @ApiModelProperty("接受者用户编号")
    private String recvUserNo;
    @ApiModelProperty("站内信标题")
    private String msgTitle;
    @ApiModelProperty("站内信内容")
    private String msgContent;
    @ApiModelProperty("发送者用户编号")
    private String sendUserNo;
}
