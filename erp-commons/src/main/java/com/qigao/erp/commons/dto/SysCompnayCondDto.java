package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysCompnayCondDto extends PageDto {
    @ApiModelProperty(value = "公司名称", required = true)
    private String compName;
    @ApiModelProperty(value = "公司类型", required = false)
    private String compType;
}
