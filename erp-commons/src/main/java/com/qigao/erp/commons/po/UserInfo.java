package com.qigao.erp.commons.po;

import com.qigao.erp.commons.dto.SysUserRoleResultDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author luiz
 * @Title: UserInfo
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 18:50
 */
@Data
public class UserInfo {
    private String userNo;

    private String userName;

    private String nickName;

    private Integer gender;

    private String tel;

    private String emailAddr;

    private String headUrl;

    private String compNo;

    private String compName;

    private String loginPass;

    private Integer status;

    private String statusName;

    private Integer userSource;

    private String userSourceName;

    private Integer userType;
    @ApiModelProperty("用户类型 0 合作机构:partner 1 客户:customer 2创业者:OWNER 3 业务员:SALER")
    private String userTypeName;

    private String userLevel;

    private Integer ifAdmin;

    private String ifAdminName;

    private String inviteCode;

    private String inviteUserNo;

    private String country;

    private String provinceCode;

    private String provinceName;

    private String cityCode;

    private String cityName;

    private String districtCode;

    private String districtName;

    private String contactAddr;

    private String language;

    private String resv1;

    private String resv2;

    private String resv3;

    private String resv4;

    private String resv5;

    private String resv6;

    private String merNo;

    private String createNo;

    private String createTime;

    private String modiNo;

    private String modiTime;

    @ApiModelProperty("用户角色")
    private List<SysUserRoleResultDto> sysUserRoleResultDtos;

    private List<String> userGroups;

    private List<UserPermit> userPermits;

    private List<MerShopInfo> merShopInfos;

}
