package com.qigao.erp.commons.dto;

import lombok.Data;

@Data
public class UserCondDto extends PageDto {
    //姓名
    private String userName;
    //推荐人
    private String inviteUserName;
    private String createNo;

}
