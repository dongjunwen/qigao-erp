package com.qigao.erp.commons.po;

import lombok.Data;

/**
 * @author luiz
 * @Title: UserPermit
 * @ProjectName iot
 * @Description: TODO
 * @date 2019-04-03 18:52
 */
@Data
public class UserPermit {
    private String userNo;
    private String groupNo;
    private String roleCode;
    private String preSourceNo;
    private String resourceType;
    private String resourceNo;
    private String reqUrl;
    private String resourceName;
    private String resourceIcon;
    private Integer resourceLevel;
}
