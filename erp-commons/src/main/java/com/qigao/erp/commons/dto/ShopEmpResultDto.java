package com.qigao.erp.commons.dto;

import lombok.Data;

@Data
public class ShopEmpResultDto {

    private Long id;

    private String shopNo;

    private String shopName;

    private String userNo;

    private String userName;

    private Integer validStatus;

    private String validStatusName;

}
