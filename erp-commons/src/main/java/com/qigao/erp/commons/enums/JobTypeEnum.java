package com.qigao.erp.commons.enums;

/**
@Author:luiz
@Date: 2018/5/25 16:15
@Descripton: 任务类型
@Modify :
 **/
public enum JobTypeEnum {
    SETTLE_JOB("SETTLE_JOB","结算JOB"),
    ;
    private String code;
    private String name;


    JobTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static final JobTypeEnum parse(String code){
        for(JobTypeEnum webSourceEnum : JobTypeEnum.values()){
            if(code.equals(webSourceEnum.getCode())) return webSourceEnum;
        }
        return null;
    }

    public static String getNameByCode(String accountType) {
        JobTypeEnum accountTypeEnum=parse(accountType);
        if(accountTypeEnum!=null) return accountTypeEnum.getName();
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

}