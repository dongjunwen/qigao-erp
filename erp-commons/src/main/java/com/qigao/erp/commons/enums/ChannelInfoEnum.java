package com.qigao.erp.commons.enums;

/**
@Author:luiz
@Date: 2018/5/25 16:15
@Descripton:渠道信息表
@Modify :
 **/
public enum ChannelInfoEnum {

    ALIPAY("ALIPAY","支付宝支付"),
    WEICHAT("WEICHAT","微信支付"),
    ;
    private String channelCode;
    private String channelName;

    ChannelInfoEnum(String channelCode, String channelName) {
        this.channelCode = channelCode;
        this.channelName = channelName;
    }

    public static final ChannelInfoEnum parse(String channelCode){
        for(ChannelInfoEnum channelInfoEnum : ChannelInfoEnum.values()){
            if(channelCode.equals(channelInfoEnum.getChannelCode())) return channelInfoEnum;
        }
        return null;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public String getChannelName() {
        return channelName;
    }
}