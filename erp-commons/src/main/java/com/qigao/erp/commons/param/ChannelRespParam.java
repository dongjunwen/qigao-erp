package com.qigao.erp.commons.param;

import com.qigao.erp.commons.enums.ChannelCodeEnum;
import com.qigao.erp.commons.enums.StreamFormatEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author:luiz
 * @Date: 2018/5/28 11:21
 * @Descripton:渠道层统一响应出参
 * @Modify :
 **/
@Getter
@Setter
@ToString
public class ChannelRespParam {
    private String respCode;
    private String respMsg;
    //返回格式
    private StreamFormatEnum formatEnum= StreamFormatEnum.JSON;
    //响应内容
    private String respContent;

    public ChannelRespParam(){
    }
    public ChannelRespParam(String respCode,String respMsg){
        this.respCode=respCode;
        this.respMsg=respMsg;
    }
    public ChannelRespParam(String respCode,String respContent,String respMsg){
        this.respCode=respCode;
        this.respMsg=respMsg;
        this.respContent=respContent;
    }
    public static ChannelRespParam error(String code,String msg){
        return new ChannelRespParam(code,msg);
    }
    public static ChannelRespParam error(String msg){
        return error(ChannelCodeEnum.FAIL.getCode(),msg);
    }
    public static ChannelRespParam error(){
        return error(ChannelCodeEnum.FAIL.getCode(),ChannelCodeEnum.FAIL.getName());
    }

    public static ChannelRespParam success(String code,String respContent,String msg){
        return new ChannelRespParam(code,respContent,msg);
    }

    public static ChannelRespParam success(String respContent,String msg){
        return new ChannelRespParam(ChannelCodeEnum.SUCCESS.getCode(),respContent,msg);
    }

    public static ChannelRespParam success(){
        return error(ChannelCodeEnum.SUCCESS.getCode(),ChannelCodeEnum.SUCCESS.getName());
    }
}
