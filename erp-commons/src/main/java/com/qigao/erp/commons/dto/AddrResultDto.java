package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Copyright (C), 2019-2019, 三人行工作室
 * FileName: AddrResultDto
 * Author:   luiz
 * Date:     2019/11/10 16:48
 * Description: 收件人地址
 * History:
 * <author>          <time>          <version>          <desc>
 * luiz            2019/11/10 16:48       版本号              描述
 */
@Data
public class AddrResultDto {
    private Integer id;
    private String userNo;
    @ApiModelProperty("姓名")
    private String recvName;
    private String recvPhone;
    @ApiModelProperty("手机号")
    private String recvMobile;
    @ApiModelProperty("省份代码")
    private String provinceCode;
    @ApiModelProperty("原始省份代码")
    private String recvProvinceCode;
    @ApiModelProperty("省份")
    private String recvProvince;
    @ApiModelProperty("城市代码")
    private String cityCode;
    @ApiModelProperty("原始城市代码")
    private String recvCityCode;
    @ApiModelProperty("城市")
    private String recvCity;
    @ApiModelProperty("区县代码")
    private String districtCode;
    @ApiModelProperty("原始区县代码")
    private String recvDistrictCode;
    @ApiModelProperty("区县")
    private String recvDistrict;

    private String recvAddress;

    private String recvZip;

    private String ifDefault;
}
