package com.qigao.erp.commons.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("费用项结果")
public class TbBillOwnerResultDto {
    @ApiModelProperty("用户类型")
    private Integer userType;
    @ApiModelProperty("用户级别")
    private String userLevel;
    @ApiModelProperty("费用项")
    private String itemCode;
    @ApiModelProperty("费用项名称")
    private String itemName;
    @ApiModelProperty("分润方式")
    private String itemType;
    @ApiModelProperty("分润值")
    private BigDecimal profitPercent;
    @ApiModelProperty("是否拥有")
    private boolean checked;

}