package com.qigao.erp.commons.dto;

import com.qigao.erp.commons.enums.SourceTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName drp-parent
 * @date 2020-06-2020-06-16 14:18
 * @Description:
 */
@Data
public class SysResourcePermitDto {
    @ApiModelProperty(value = "来源编号", required = true)
    private String sourceCode;
    @ApiModelProperty(value = "来源类型", required = true)
    private SourceTypeEnum sourceTypeEnum;
    @ApiModelProperty(value = "资源代码", required = true)
    private String resourceNo;
    private String operUserNo;
}
