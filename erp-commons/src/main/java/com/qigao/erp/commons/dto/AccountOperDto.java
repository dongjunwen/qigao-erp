package com.qigao.erp.commons.dto;

import com.qigao.erp.commons.enums.TradeTypeEnum;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luiz
 * @Title: com.qigao.erp.commons.dto
 * @ProjectName three-mall
 * @date 2020-04-2020/4/10 10:57
 * @Description:
 */
@Data
public class AccountOperDto {
    private String userNo;//用户号
    private BigDecimal tradeAmt;//交易金额
    private String merNo;//商户号
    private TradeTypeEnum sourceTradeType;//来源交易类型
    private String sourceOrderNo;//来源订单号
    private String accoutNo;
}
