/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2020/9/7 10:54:42                            */
/*==============================================================*/


drop table if exists sys_company;

drop table if exists sys_depart;

drop table if exists sys_dict_info;

drop table if exists sys_oper_log;

drop table if exists sys_resource;

drop table if exists sys_resource_permit;

drop table if exists sys_role;

drop index uniq_index on sys_user;

drop table if exists sys_user;

drop table if exists sys_user_role;

drop table if exists tb_cache_info;

drop table if exists tb_channel_info;

drop index Index_01 on tb_file_info;

drop table if exists tb_file_info;

drop table if exists tb_region_info;

/*==============================================================*/
/* Table: sys_company                                           */
/*==============================================================*/
create table sys_company
(
   id                   int(10) not null auto_increment comment '主键ID',
   comp_no              varchar(32) comment '公司编号',
   comp_name            varchar(64) comment '公司名称',
   comp_type            varchar(64) default 'INTERNAL' comment '公司类型 INTERNAL:内部公司 PARTENER:合作机构',
   comp_level           varchar(16) comment '公司级别 A,B,C,D',
   parent_comp_no       varchar(32) comment '上级公司编号',
   contact_name         varchar(64) comment '联系人',
   tel                  varchar(16) comment '联系电话',
   email_addr           varchar(32) comment '邮箱地址',
   status               int default 1 comment '状态 0:无效 1:有效',
   province_code        varchar(32) comment '省份代码',
   province_name        varchar(64) comment '省份',
   city_code            varchar(32) comment '城市代码',
   city_name            varchar(64) comment '城市',
   district_code        varchar(32) comment '区、县代码',
   district_name        varchar(64) comment '区、县名称',
   contact_addr         varchar(128) comment '联系地址',
   resv1                varchar(512) comment '保留域1',
   resv2                varchar(128) comment '保留域2',
   mer_no               varchar(64) comment '商户号(卖家编号)',
   version              int default 0 comment '版本号',
   create_no            varchar(32) comment '创建人',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   modi_no              varchar(32) comment '修改人',
   modi_time            datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_company comment '公司信息表';

/*==============================================================*/
/* Table: sys_depart                                            */
/*==============================================================*/
create table sys_depart
(
   id                   int(10) not null auto_increment comment '主键ID',
   depart_no            varchar(32) comment '公司编号',
   depart_name          varchar(64) comment '公司名称',
   parent_depart_no     varchar(64) default 'INTERNAL' comment '上级部门编号',
   comp_no              varchar(32) comment '公司编号',
   mer_no               varchar(64) comment '商户号(卖家编号)',
   version              int default 0 comment '版本号',
   create_no            varchar(32) comment '创建人',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   modi_no              varchar(32) comment '修改人',
   modi_time            datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_depart comment '部门信息表';

/*==============================================================*/
/* Table: sys_dict_info                                         */
/*==============================================================*/
create table sys_dict_info
(
   id                   int not null auto_increment comment '自增ID',
   dict_no              varchar(32) comment '字典编号',
   dict_name            varchar(64) comment '字典名称',
   dict_val             varchar(128) comment '字典值',
   dict_desc            varchar(128) comment '字典描述',
   pdict_no             varchar(32) comment '上级字典编号',
   dict_status          int(1) default 1 comment '字典状态 0:未生效 1:生效 ',
   create_no            varchar(32) comment '创建人',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   modi_no              varchar(32) comment '修改人',
   modi_time            datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_dict_info comment '字典表 ';

/*==============================================================*/
/* Table: sys_oper_log                                          */
/*==============================================================*/
create table sys_oper_log
(
   id                   int(11) not null auto_increment comment '自增主键',
   oper_user_no         VARCHAR(32) comment '操作人',
   oper_user_name       VARCHAR(64) comment '操作人姓名',
   oper_time            datetime comment '操作时间',
   oper_where           VARCHAR(64) comment '地方',
   oper_action          VARCHAR(64) comment '动作',
   oper_content         VARCHAR(4000) comment '操作内容',
   primary key (id)
);

alter table sys_oper_log comment '操作日志表 sys_oper_log';

/*==============================================================*/
/* Table: sys_resource                                          */
/*==============================================================*/
create table sys_resource
(
   id                   int(11) not null auto_increment comment '自增主键ID',
   resource_no          VARCHAR(32) comment '资源编号',
   resource_name        VARCHAR(64) comment '资源名称',
   presource_no         VARCHAR(32) comment '上级资源编号',
   resource_type        VARCHAR(16) default 'MENU' comment '资源类型 MENU:菜单 BUTTON:按钮',
   req_url              VARCHAR(128) comment '请求路径',
   route_path           VARCHAR(128) comment '路由路径',
   resource_icon        VARCHAR(128) comment '图标',
   sort_order           int(11) comment '排序规则',
   resource_level       int(11) comment '级别',
   if_visible           CHAR(1) default 'Y' comment '是否显示 Y:显示 N:不显示',
   memo                 VARCHAR(256) comment '备注',
   create_no            VARCHAR(32) comment '创建人',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   modi_no              VARCHAR(32) comment '修改人',
   modi_time            datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_resource comment '资源表';

/*==============================================================*/
/* Table: sys_resource_permit                                   */
/*==============================================================*/
create table sys_resource_permit
(
   id                   int(11) not null auto_increment comment '自增主键ID',
   source_code          VARCHAR(32) comment '来源编号 ',
   source_type          VARCHAR(16) default 'ROLE' comment '来源类型 ROLE:角色 USER:用户',
   resource_no          VARCHAR(32) comment '资源编号',
   create_no            varchar(32) comment '创建人',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   modi_no              varchar(32) comment '修改人',
   modi_time            datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_resource_permit comment '资源权限表';

/*==============================================================*/
/* Table: sys_role                                              */
/*==============================================================*/
create table sys_role
(
   id                   int(11) not null auto_increment comment '自增主键ID',
   role_code            VARCHAR(32) comment '角色编号',
   role_name            VARCHAR(64) comment '角色名称',
   parent_role_code     VARCHAR(32) comment '上级角色编号',
   status               CHAR(1) default 'Y' comment '状态 Y:有效 N:无效',
   memo                 VARCHAR(256) comment '备注',
   create_no            varchar(32) comment '创建人',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   modi_no              varchar(32) comment '修改人',
   modi_time            datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_role comment '角色表';

/*==============================================================*/
/* Table: sys_user                                              */
/*==============================================================*/
create table sys_user
(
   id                   int(10) not null auto_increment comment '主键ID',
   user_no              varchar(32) comment '用户编号',
   user_name            varchar(64) comment '用户名称',
   nick_name            varchar(64) comment '昵称',
   gender               int default 0 comment '性别 0：未知、1：男、2：女',
   tel                  varchar(16) comment '联系电话',
   phone_num            varchar(32) comment '手机号',
   email_addr           varchar(32) comment '邮箱地址',
   head_url             varchar(256) comment '头像地址',
   comp_no              varchar(32) comment '单位编号',
   comp_name            varchar(128) comment '单位名称',
   login_pass           varchar(512) comment '登录密码',
   status               int default 1 comment '状态 0:无效 1:有效',
   user_source          int default 1 comment '用户来源 0:微信公众号 1:系统内部',
   user_type            int default 1 comment '用户类型  partner:合作机构 customer:客户 EMPLOYEE:内部员工',
   if_admin             int default 0 comment '是否管理员 0:不是 1:是 ',
   user_level           varchar(16) comment '用户级别 A,B,C,D',
   invite_code          varchar(64) comment '个人推荐码',
   invite_user_no       varchar(32) comment '被推荐人',
   if_formal            int default 0 comment '是否转正 0:不是 1:是 ',
   entry_date           date comment '入职日期',
   formal_date          date comment '转正日期',
   country              varchar(32) comment '国家',
   province_code        varchar(32) comment '省份代码',
   province_name        varchar(64) comment '省份',
   city_code            varchar(32) comment '城市代码',
   city_name            varchar(64) comment '城市',
   district_code        varchar(32) comment '区、县代码',
   district_name        varchar(64) comment '区、县名称',
   contact_addr         varchar(128) comment '联系地址',
   language             varchar(32) comment '使用语言 ',
   resv1                varchar(512) comment '保留域1',
   resv2                varchar(512) comment '保留域2',
   resv3                varchar(512) comment '保留域3',
   resv4                varchar(512) comment '保留域4',
   resv5                varchar(512) comment '保留域5',
   resv6                varchar(512) comment '保留域6',
   mer_no               varchar(64) comment '商户号(卖家编号)',
   version              int default 0 comment '版本号',
   create_no            varchar(32) comment '创建人',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   modi_no              varchar(32) comment '修改人',
   modi_time            datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_user comment '用户表';

/*==============================================================*/
/* Index: uniq_index                                            */
/*==============================================================*/
create unique index uniq_index on sys_user
(
   user_no
);

/*==============================================================*/
/* Table: sys_user_role                                         */
/*==============================================================*/
create table sys_user_role
(
   id                   int(11) not null auto_increment comment '自增主键ID',
   user_no              VARCHAR(32) comment '用户编号',
   role_code            VARCHAR(32) comment '角色编号',
   role_name            VARCHAR(64) comment '角色名称',
   create_no            varchar(32) comment '创建人',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   modi_no              varchar(32) comment '修改人',
   modi_time            datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_user_role comment '用户角色表';

/*==============================================================*/
/* Table: tb_cache_info                                         */
/*==============================================================*/
create table tb_cache_info
(
   id                   int not null auto_increment comment '自增ID',
   cache_key            varchar(128) comment '缓存KEY',
   cache_val            text comment '缓存值',
   start_time           datetime comment '开始时间',
   end_time             datetime comment '束时间',
   mer_no               varchar(64) comment '商户号(卖家编号)',
   version              int default 0 comment '版本号',
   primary key (id)
);

alter table tb_cache_info comment '缓存表';

/*==============================================================*/
/* Table: tb_channel_info                                       */
/*==============================================================*/
create table tb_channel_info
(
   id                   int not null auto_increment comment '主键id',
   channel_partener_no  varchar(64) comment '渠道合作者编号',
   channel_partener_name varchar(120) comment '渠道合作者名称',
   channel_code         varchar(64) comment '渠道编号',
   channel_name         varchar(64) comment '渠道名称',
   sign_type            varchar(16) comment '加密方式',
   pub_key              varchar(4096) comment '公钥',
   priv_key             varchar(4096) comment '私钥',
   md5_key              varchar(64) comment 'Md5秘钥',
   resv_key             varchar(4096) comment '保留key',
   resv1                varchar(1024) comment '保留域1',
   resv2                varchar(1024) comment '保留域2',
   channel_fee          decimal(16,4) default 0 comment '渠道方收取手续费',
   status               char(1) default 'Y' comment '状态 Y:有效 N:无效',
   version              int default 0 comment '版本号',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   create_no            varchar(64) comment '创建人',
   modi_time            datetime default CURRENT_TIMESTAMP comment '修改时间',
   modi_no              varchar(64) comment '修改人',
   primary key (id)
);

alter table tb_channel_info comment '渠道信息表';

/*==============================================================*/
/* Table: tb_file_info                                          */
/*==============================================================*/
create table tb_file_info
(
   id                   int not null auto_increment comment '自增ID',
   file_no              varchar(32) comment '文件编号',
   narrow_file_no       varchar(32) comment '缩略图编号',
   zip_file_no          varchar(32) comment '压缩图片编号',
   file_type            varchar(16) comment '文件类型',
   file_name            varchar(512) comment '文件名称',
   file_size            int(11) comment '文件大小',
   file_url             varchar(256) comment '文件地址',
   file_desc            varchar(256) comment '文件描述',
   resv1                varchar(256) comment '保留域1',
   resv2                varchar(256) comment '保留域2',
   resv3                varchar(256) comment '保留域3',
   resv4                varchar(256) comment '保留域4',
   version              int default 0 comment '版本号',
   create_no            varchar(32) comment '创建人',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   modi_no              varchar(32) comment '修改人',
   modi_time            datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table tb_file_info comment '文件信息表，所有文件存储位置，方便展示及后续迁移。';

/*==============================================================*/
/* Index: Index_01                                              */
/*==============================================================*/
create unique index Index_01 on tb_file_info
(
   file_no
);

/*==============================================================*/
/* Table: tb_region_info                                        */
/*==============================================================*/
create table tb_region_info
(
   id                   int not null auto_increment comment '主键id',
   parea_code           varchar(32) comment '上级编码',
   area_code            varchar(32) comment '区域编码',
   area_name            varchar(64) comment '区域名称',
   recv_zip             varchar(6) comment '邮政编码',
   area_level           int default 1 comment '区域级别',
   status               char(1) default 'Y' comment '是否生效 Y:有效N:无效',
   publish_ym           varchar(12) comment '公布年月 类似:201909',
   version              int default 0 comment '版本号',
   create_no            varchar(32) comment '创建人',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   modi_no              varchar(32) comment '修改人',
   modi_time            datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table tb_region_info comment '区域信息表';

