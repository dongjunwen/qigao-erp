package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.jdbc.model.TbCacheInfo;

public interface AbstractTbCacheInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbCacheInfo record);

    int insertSelective(TbCacheInfo record);

    TbCacheInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbCacheInfo record);

    int updateByPrimaryKeyWithBLOBs(TbCacheInfo record);

    int updateByPrimaryKey(TbCacheInfo record);
}