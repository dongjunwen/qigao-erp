package com.qigao.erp.jdbc.model;

import java.util.Date;

public class TbCacheInfo {
    private Integer id;

    private String cacheKey;

    private Date startTime;

    private Date endTime;

    private String merNo;

    private Integer version;

    private String cacheVal;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey == null ? null : cacheKey.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getMerNo() {
        return merNo;
    }

    public void setMerNo(String merNo) {
        this.merNo = merNo == null ? null : merNo.trim();
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getCacheVal() {
        return cacheVal;
    }

    public void setCacheVal(String cacheVal) {
        this.cacheVal = cacheVal == null ? null : cacheVal.trim();
    }
}