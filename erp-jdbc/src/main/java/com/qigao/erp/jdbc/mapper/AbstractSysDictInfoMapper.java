package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.jdbc.model.SysDictInfo;

public interface AbstractSysDictInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysDictInfo record);

    int insertSelective(SysDictInfo record);

    SysDictInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysDictInfo record);

    int updateByPrimaryKey(SysDictInfo record);
}