package com.qigao.erp.jdbc.mapper;


import com.qigao.erp.commons.dto.SysRoleCondDto;
import com.qigao.erp.jdbc.model.SysRole;

import java.util.List;

public interface SysRoleMapper extends AbstractSysRoleMapper {
    SysRole selectByRoleCode(String roleCode);

    List<SysRole> selectAll();

    List<SysRole> selectList(SysRoleCondDto sysRoleCondDto);

    List<SysRole> selectRoleLike(String condStr);

}