package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.jdbc.model.SysResourcePermit;

public interface AbstractSysResourcePermitMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysResourcePermit record);

    int insertSelective(SysResourcePermit record);

    SysResourcePermit selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysResourcePermit record);

    int updateByPrimaryKey(SysResourcePermit record);
}