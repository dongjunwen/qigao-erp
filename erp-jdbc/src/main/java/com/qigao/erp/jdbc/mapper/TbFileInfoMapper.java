package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.jdbc.model.TbFileInfo;

public interface TbFileInfoMapper extends AbstractTbFileInfoMapper{

    TbFileInfo findByFileNo(String fileNo);
}