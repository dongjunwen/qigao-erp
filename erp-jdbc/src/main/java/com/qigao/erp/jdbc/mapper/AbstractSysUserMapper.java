package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.jdbc.model.SysUser;

public interface AbstractSysUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);
}