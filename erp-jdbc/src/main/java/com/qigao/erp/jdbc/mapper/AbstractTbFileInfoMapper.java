package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.jdbc.model.TbFileInfo;

public interface AbstractTbFileInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbFileInfo record);

    int insertSelective(TbFileInfo record);

    TbFileInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbFileInfo record);

    int updateByPrimaryKey(TbFileInfo record);
}