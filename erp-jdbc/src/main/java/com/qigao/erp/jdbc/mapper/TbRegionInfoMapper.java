package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.jdbc.model.TbRegionInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbRegionInfoMapper extends AbstractTbRegionInfoMapper{

    TbRegionInfo selectByIndex(@Param("areaCode") String areaCode, @Param("publishYm") String publishYm, @Param("areaLevel")int areaLevel);

    List<TbRegionInfo> selectByYmAndLevel( @Param("publishYm") String publishYm, @Param("areaLevel")int areaLevel);

    List<TbRegionInfo> selectChildByYmAndAreaCode(@Param("publishYm") String publishYm,@Param("areaCode") String areaCode);
}