package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.commons.dto.SysCompnayCondDto;
import com.qigao.erp.jdbc.model.SysCompany;

import java.util.List;

public interface SysCompanyMapper extends AbstractSysCompanyMapper{

    SysCompany selectByCompanyName(String compName);

    List<SysCompany> selectByCond(String condStr);

    SysCompany selectByCompNo(String compCode);

    List<SysCompany> selectList(SysCompnayCondDto sysCompanyCondDto);

    List<SysCompany> selectAll();
}