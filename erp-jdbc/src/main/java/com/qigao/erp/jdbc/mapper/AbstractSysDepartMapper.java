package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.jdbc.model.SysDepart;

public interface AbstractSysDepartMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysDepart record);

    int insertSelective(SysDepart record);

    SysDepart selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysDepart record);

    int updateByPrimaryKey(SysDepart record);
}