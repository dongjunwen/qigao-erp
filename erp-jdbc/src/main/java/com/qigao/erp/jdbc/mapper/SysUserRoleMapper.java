package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.jdbc.model.SysUserRole;

import java.util.List;

public interface SysUserRoleMapper extends AbstractSysUserRoleMapper {

    void deleteInBatch(List<SysUserRole> sysUserRoles);

    List<SysUserRole> selectByUserNo(String userNo);

    SysUserRole selectByIndex(String userNo, String roleCode);

    void deleteByUserNo(String userNo);

    List<SysUserRole> selectAll();
}