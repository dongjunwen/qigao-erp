package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.jdbc.model.TbRegionInfo;

public interface AbstractTbRegionInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbRegionInfo record);

    int insertSelective(TbRegionInfo record);

    TbRegionInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbRegionInfo record);

    int updateByPrimaryKey(TbRegionInfo record);
}