package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.commons.dto.SysOperLogCondDto;
import com.qigao.erp.jdbc.model.SysOperLog;

import java.util.List;

public interface SysOperLogMapper extends AbstractSysOperLogMapper {
    List<SysOperLog> selectList(SysOperLogCondDto sysOperLogCondDto);

}