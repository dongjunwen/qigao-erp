package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.commons.dto.SysDepartCondDto;
import com.qigao.erp.jdbc.model.SysDepart;

import java.util.List;

public interface SysDepartMapper extends AbstractSysDepartMapper{

    List<SysDepart> selectByCond(String condStr);

    SysDepart selectByDepartNo(String compCode);

    Object selectList(SysDepartCondDto sysDepartCondDto);

    List<SysDepart> selectAll();
}