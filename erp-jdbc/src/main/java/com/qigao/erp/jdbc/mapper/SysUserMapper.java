package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.commons.dto.UserCondDto;
import com.qigao.erp.commons.dto.UserRegisterDto;
import com.qigao.erp.jdbc.model.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author luiz
 * @Title: SysUserMapper
 * @ProjectName tokyo
 * @Description: TODO
 * @date 2019-04-04 10:52
 */
public interface SysUserMapper extends AbstractSysUserMapper{
    SysUser selectByUserNo(String userNo);

    List<SysUser> selectByCond(UserRegisterDto userRegisterDto);

    SysUser selectByIndex(@Param("uniqIndexNo") String loginNo);

    List<SysUser> selectList(UserCondDto userCondDto);

    void updateByUserNo(SysUser sysUser);

    List<SysUser> selectAll();

    SysUser selectByActiveCode(String activeCode);

    void updateBatch(List<SysUser> sysUsers);

    SysUser selectByEmailAddr(String emailAddr);

    SysUser selectByPhoneNum(String loginNo);

    SysUser selectInviteUserByNo(String recvUserNo);
}
