package com.qigao.erp.jdbc.model;

import java.util.Date;

public class SysOperLog {
    private Integer id;

    private String operUserNo;

    private String operUserName;

    private Date operTime;

    private String operWhere;

    private String operAction;

    private String operContent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOperUserNo() {
        return operUserNo;
    }

    public void setOperUserNo(String operUserNo) {
        this.operUserNo = operUserNo == null ? null : operUserNo.trim();
    }

    public String getOperUserName() {
        return operUserName;
    }

    public void setOperUserName(String operUserName) {
        this.operUserName = operUserName == null ? null : operUserName.trim();
    }

    public Date getOperTime() {
        return operTime;
    }

    public void setOperTime(Date operTime) {
        this.operTime = operTime;
    }

    public String getOperWhere() {
        return operWhere;
    }

    public void setOperWhere(String operWhere) {
        this.operWhere = operWhere == null ? null : operWhere.trim();
    }

    public String getOperAction() {
        return operAction;
    }

    public void setOperAction(String operAction) {
        this.operAction = operAction == null ? null : operAction.trim();
    }

    public String getOperContent() {
        return operContent;
    }

    public void setOperContent(String operContent) {
        this.operContent = operContent == null ? null : operContent.trim();
    }
}