package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.jdbc.model.SysOperLog;

public interface AbstractSysOperLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysOperLog record);

    int insertSelective(SysOperLog record);

    SysOperLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysOperLog record);

    int updateByPrimaryKey(SysOperLog record);
}