package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.commons.dto.PageDto;
import com.qigao.erp.jdbc.model.TbCacheInfo;

import java.util.List;

public interface TbCacheInfoMapper extends AbstractTbCacheInfoMapper {
    TbCacheInfo selctByIndex(String cacheKey);

    void deleteByCacheKey(String cacheKey);

    void deleteWhenIsOverTime();

    List<TbCacheInfo> selectPageList(PageDto pageDto);

    void deleteByIds(List<TbCacheInfo> tbCacheInfos);

    TbCacheInfo selectTopOneByMerNo(String merNo);

    void updateWithVersion(TbCacheInfo tbCacheInfo);
}