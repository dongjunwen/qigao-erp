package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.commons.dto.SysDictCondDto;
import com.qigao.erp.jdbc.model.SysDictInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysDictInfoMapper extends AbstractSysDictInfoMapper{


    List<SysDictInfo> findByPDictEnable(String pDictNo);

    List<SysDictInfo> selectBypDictNo(String pDictNo);

    List<SysDictInfo> selectList(SysDictCondDto sysDictCondDto);

    SysDictInfo findByUniq(@Param("pdictNo") String pdictNo, @Param("dictNo") String dictNo);

    List<SysDictInfo> selectAll();

    List<SysDictInfo> selectForLevel1();

}