package com.qigao.erp.jdbc.mapper;

import com.github.pagehelper.Page;
import com.qigao.erp.commons.dto.SysResourceCondDto;
import com.qigao.erp.jdbc.model.SysResource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysResourceMapper extends AbstractSysResourceMapper {
    SysResource selectBySourceNo(String sourceNo);

    List<SysResource> selectByUserNo(String currentUserNo);

    Page<SysResource> selectList(SysResourceCondDto sysResourceCondDto);

    List<SysResource> selectAll();

    List<SysResource> selectByCond(@Param("condStr") String condStr);
}