package com.qigao.erp.jdbc.mapper;

import com.qigao.erp.commons.po.UserPermitResult;
import com.qigao.erp.jdbc.model.SysResourcePermit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysResourcePermitMapper extends AbstractSysResourcePermitMapper {

    void deleteBySourceCode(String sourceNo);

    void insertBatch(List<SysResourcePermit> sysResourcePermits);

    void deleteInBatch(List<SysResourcePermit> sysResourcePermitArrayList);

    List<SysResourcePermit> selectBySourceCode(String sourceNo);

    List<UserPermitResult> selectByLevelOne(String userNo);

    List<UserPermitResult> selectChild(@Param("userNo") String userNo,@Param("resourceNo")String resourceNo);

    List<SysResourcePermit> selectByIndex(@Param("sourceCode") String sourceCode, @Param("sourceType") String sourceType);
}